 jQuery(document).ready(function($) {
     $('.page-edit-claim .form-country').val($('.form-country').data('value'));
     $("body").delegate(".play_music .fa-play", "click", function(){
         if(!$(this).parents('.play_music').hasClass('play_hiden')){
             if($(this).hasClass('play_active')){
                 $(".play_music .fa-play").removeClass('play_active');
                 $(".play_music .fa-play").next().trigger('pause');
                 $(this).removeClass('play_active');
                 $(this).next().trigger('pause');
             }else{
                 $(".play_music .fa-play").removeClass('play_active');
                 $(".play_music .fa-play").next().trigger('pause');
                 $(this).addClass('play_active');
                 $(this).next().trigger('play');
             }
         }

     });
     $('.list-publish').each(function(){
         if($(this).find('.td-composers').text().trim() == ''){
             $(this).find('.btn-pubbtn-choose-file btn-add-contributor5 color-alish-song-color').addClass('disabled');
         }else if($(this).find('.td-lyricists').text().trim() == ''){
             $(this).find('.btn-publish-song-color').addClass('disabled');
         }else{
             $(this).find('.btn-publish-song-color').removeClass('disabled');

         }
     });
     $('.edit-song-pages input').each(function(){

        if($(this).attr('checked') &&  $(this).attr('name') != "option2"){
            $(this).attr('value','1');
        }
     });
     if($('.edit-equal').length >=1){
         $('.edit-equal').change();
     }
     $('.claim-select-song').change(function(){
         var name = $(this).find(':selected').data('name');
         var url = $(this).find(':selected').data('url');
         var id = $(this).find(':selected').data('id');
         $('.claim-name-song').val(name);
         $('.claim-url').val(url);
         $('.claim-id-song').val(id);
     });
     $('.claim-upload-file').on('click',function(){
        $('#id08').hide();
     });
     $('.btn-claim-cancel').click(function(e){
         e.preventDefault();
         $('#idSubmitClaim').hide();
     });
     $('.form-copy-song').on('change', function() {
         $('.id-song-copy').val(this.value);
         $('.list-copy-song').removeAttr('disabled');
         if($(this).val() == 'No Song'){
             $('.list-copy-song').attr('disabled','disabled');
         }
     });
     $("body").delegate(".page-copy-song .fa-pencil", "click", function(e){
         e.preventDefault();
         var id = $(this).data('name2');
         var modal =  $('#idAddContributor');
         modal.show();
         modal.addClass('edit-copy-contributor').attr('data-id',id);
         modal.find('.title-upload-song').text('EDIT CONTRIBUTOR');
         var form = $(this).parents('.contributors');
         var first_name = form.find('input[name="contributors_firstname[]"]').val();
         var last_name = form.find('input[name="contributors_lastname[]"]').val();
         var member_pro = form.find('input[name="contributors_memberpro[]"]').val();
         var member_pro_which_pro = form.find('input[name="contributors_promember[]"]').val();
         var member_pro_cae_number = form.find('input[name="contributors_cae_number[]"]').val();
         var member_society = form.find('input[name="contributors_member_society[]"]').val();
         var member_society_which_pro = form.find('input[name="contributors_mech_society_member[]"]').val();
         var member_society_number = form.find('input[name="contributors_mech_society_number[]"]').val();
         var role = form.find('input[name="contributors_role[]"]').val();
         modal.find('.form-first-name').val(first_name);
         modal.find('.form-last-name').val(last_name);
         if(member_pro == 1){
             modal.find('.member_pro').val(member_pro).attr('checked','checked');
             $('.form-group3').show();
         }
         modal.find('.member_pro_which_pro').val(member_pro_which_pro);
         modal.find('.member_pro_number').val(member_pro_cae_number);
         if(member_society == 1){
             modal.find('.member_society').val(member_society).attr('checked','checked');
             $('.form-group4').show();
         }
         modal.find('.member_society_which_pro').val(member_society_which_pro);
         modal.find('.member_society_number').val(member_society_number);
         modal.find('input[name="role"]').removeAttr('checked','checked');
         modal.find('input[name="role"][value="'+role+'"]').attr('checked','checked');
     });
     $('.page-copy-song .btn-add-contributors').click(function(){
         $('#idAddContributor .title-upload-song').text('ADD CONTRIBUTOR');
         var form = $('#idAddContributor');
         var first_name = form.find('.form-first-name').val('');
         var last_name = form.find('.form-last-name').val('');
         var member_pro = form.find('.member_pro').val('0').removeAttr('checked');
         var member_pro_which_pro = form.find('.member_pro_which_pro').val('');
         var member_pro_cae_number = form.find('.member_pro_number').val('');
         var member_society = form.find('.member_society').val('0').removeAttr('checked');
         var member_society_which_pro = form.find('.member_society_which_pro').val('');
         var member_society_number = form.find('.member_society_number').val('');
         form.find('input[name="role"][value=""]').attr('checked','checked');
         form.removeAttr('data-id').removeClass('edit-copy-contributor');
         $('.form-group4').hide();
         $('.form-group3').hide();
     });

     $('.page-copy-song #btnchange-color212').on('click',function(){


         if($('#idAddContributor').hasClass('edit-copy-contributor')){
             var id = $('.edit-copy-contributor').data('id');
             $('.id-'+id+'').children().remove();
             var form = $(this).parents('#userform');
             var first_name = form.find('.form-first-name').val();
             var last_name = form.find('.form-last-name').val();
             var member_pro = form.find('.member_pro').val();
             var member_pro_which_pro = form.find('.member_pro_which_pro').val();
             var member_pro_cae_number = form.find('.member_pro_number').val();
             var member_society = form.find('.member_society').val();
             var member_society_which_pro = form.find('.member_society_which_pro').val();
             var member_society_number = form.find('.member_society_number').val();
             var role = form.find('.role:checked').val();
             var string ='<span class="contributors-name">'+first_name+' '+last_name+'</span>';
             string+='<input type="hidden" name="contributors_firstname[]" value="'+first_name+'">';
             string+='<input type="hidden" name="contributors_lastname[]" value="'+last_name+'">';
             string+='<input type="hidden" name="contributors_role[]" value="'+role+'">';
             string+='<input type="hidden" name="contributors_memberpro[]" value="'+member_pro+'">';
             string+='<input type="hidden" name="contributors_promember[]" value="'+member_pro_which_pro+'">';
             string+='<input type="hidden" name="contributors_cae_number[]" value="'+member_pro_cae_number+'">';
             string+='<input type="hidden" name="contributors_member_society[]" value="'+member_society+'">';
             string+='<input type="hidden" name="contributors_mech_society_member[]" value="'+member_society_which_pro+'">';
             string+='<input type="hidden" name="contributors_mech_society_number[]" value="'+member_society_number+'">';
             string+='<input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal">';
             string+='<span class="role"><span class="role-mobi">ROLE: </span>'+role+'</span>';
             string+='<span class="ratio-percent" style="display:block"> %</span><input type="text" name="contributors_equal2[]" value="0" class="percent-share2" placeholder="0">';
             string+='<span class="percent-icon1">%</span><i class="fa fa-pencil" data-name2="'+id+'"></i>';
             string+='<i class="fa fa-remove  " data-name="'+id+'" style="font-size:24px; color: red"></i>' ;
                 // ' <div class="border"></div>';
             $('#idAddContributor').hide();
             $('.id-'+id+'').append(string);
             // $('.half-past-percent').show();
             $('.half-past-percent, .title-role, .title-share').show();
             $('#note_contributor').text(' ');
         }else{
             if($('input[name="txtFirstName"]').val() == '' && $('input[name="txtLastName"]').val() == ''){
             }else{
                 var form = $(this).parents('#userform');
                 var first_name = form.find('.form-first-name').val();
                 var last_name = form.find('.form-last-name').val();
                 var member_pro = form.find('.member_pro').val();
                 var member_pro_which_pro = form.find('.member_pro_which_pro').val();
                 var member_pro_cae_number = form.find('.member_pro_number').val();
                 var member_society = form.find('.member_society').val();
                 var member_society_which_pro = form.find('.member_society_which_pro').val();
                 var member_society_number = form.find('.member_society_number').val();
                 var role = form.find('.role:checked').val();
                 var count = $('.contributors').length + 1;
                 var id = $('.contributors').length;
                 var string = '<div class="contributors id-'+id+'">';
                 string+='<span class="contributors-name">'+first_name+' '+last_name+'</span>';
                 string+='<input type="hidden" name="contributors_firstname[]" value="'+first_name+'">';
                 string+='<input type="hidden" name="contributors_lastname[]" value="'+last_name+'">';
                 string+='<input type="hidden" name="contributors_role[]" value="'+role+'">';
                 string+='<input type="hidden" name="contributors_memberpro[]" value="'+member_pro+'">';
                 string+='<input type="hidden" name="contributors_promember[]" value="'+member_pro_which_pro+'">';
                 string+='<input type="hidden" name="contributors_cae_number[]" value="'+member_pro_cae_number+'">';
                 string+='<input type="hidden" name="contributors_member_society[]" value="'+member_society+'">';
                 string+='<input type="hidden" name="contributors_mech_society_member[]" value="'+member_society_which_pro+'">';
                 string+='<input type="hidden" name="contributors_mech_society_number[]" value="'+member_society_number+'">';
                 string+='<input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal">';
                 string+='<span class="role"><span class="role-mobi">ROLE: </span>'+role+'</span>';
                 string+='<span class="ratio-percent" style="display:block"> %</span><input type="text" name="contributors_equal2[]" value="0" class="percent-share2" placeholder="0">';
                 string+='<span class="percent-icon1">%</span><i class="fa fa-pencil" data-name2="'+id+'"></i>';
                 string+='<i class="fa fa-remove  " data-name="'+id+'" style="font-size:24px; color: red"></i> ' +
                     // '<div class="border"></div>' +
                     '</div>';
                 // $('.half-past-percent').before(string);
                 $('.group-contributors').append(string);
                 $('#idAddContributor').hide();
                 $('.half-past-percent, .title-role, .title-share').show();
                 $('#note_contributor').text(' ');
             }
         }
     });
     $('.btn-save-add-song').click(function(e){
        var title =  $('.form-title');
        var comtributor = $('.contributors');
        if(title.length == 0 || comtributor.length == 0){
            e.preventDefault();
            $('#check-song-contributor').show();
        }
        var check_contain = $('.check_contains_sample');
        var check_right = $('.check_right_sample');
        if(check_contain.is(':checked')){
            if(!check_right.is(':checked')){
                e.preventDefault();
                $('#checkright-sample').show();
            }
        }
     });
     // $('.edit-song-pages .btn-save-add-song').click(function(e){
     //     var title =  $('.form-title');
     //     var comtributor = $('.contributors');
     //     if(title.length == 0 || comtributor.length == 0){
     //         e.preventDefault();
     //         $('#check-song-contributor').show();
     //     }
     //     var check_contain = $('.check_contains_sample');
     //     var check_right = $('.check_right_sample');
     //     if(check_contain.is(':checked')){
     //         if(!check_right.is(':checked')){
     //             e.preventDefault();
     //             $('#checkright-sample').show();
     //         }
     //     }
     // });
     $("body").delegate(".add-exist-contributor", "click", function(){
        $('.select-option-form').hide();
     });
     $("body").delegate(".add-new-contributor ", "click", function(){
         $('.select-option-form').show();
     });
     $("body").delegate(".btn-add-contributors", "click", function(){
         $('.select-option-form').show();
         $('.add-new-contributor').trigger('click');
     });
     $("body").delegate(".exis-contributors", "change", function(){
         $('.add-btn-save-contributor').css('background-color','rgb(243, 86, 103)');
        var first_name = $(this).find(':selected').data('first_name');
        var last_name = $(this).find(':selected').data('last_name');
        var role = $(this).find(':selected').data('role');
        var share = $(this).find(':selected').data('share');
        var pro_member_check =  $(this).find(':selected').data('pro_member_check');
        var which_pro =  $(this).find(':selected').data('which_pro');
        var cae_number = $(this).find(':selected').data('cae_number');
        var member_society_check = $(this).find(':selected').data('member_society_check');
        var mech_society_member = $(this).find(':selected').data('mech_society_member');
        var mech_society_number = $(this).find(':selected').data('mech_society_number');
        var string ='';
         if(pro_member_check == 1){
             string += ' <div class="existing-text">';
             string += '<p class="existing-title">Member of a PRO:</p><span class="existing-data">';
             string += 'Yes,'+which_pro+'';
             string +='</span></div><div class="existing-text">';
             string +='<p class="existing-title">CAE Number:</p><span class="existing-data">'+cae_number+'</span></div>'
          }
         if(member_society_check == 1){
             string += ' <div class="existing-text">';
             string += '<p class="existing-title">Member of a mech. society:</p><span class="existing-data">';
             string += 'Yes,'+mech_society_member+'';
             string +='</span></div><div class="existing-text">';
             string +='<p class="existing-title">CAE Number:</p><span class="existing-data">'+mech_society_number+'</span></div>'
         }
         $('.modal-add-contributor input[name="option2"]').removeAttr('checked');
         $('.modal-add-contributor input[name="option2"][value="'+role+'"]').attr('checked','checked');
         if(member_society_check !=1 && pro_member_check != 1){
             string +=""
         }
        var form = $(this).parents('.modal-add-contributor');
         form.find('.form-first-name').val(first_name);
         form.find('.form-last-name').val(last_name);
         form.find('.member_pro').val(pro_member_check).removeAttr('checked');
         form.find('.member_society').val(member_society_check).removeAttr('checked');
         form.find('.form-group4').hide();
         form.find('.form-group3').hide();
        if(pro_member_check == 1){
            form.find('.form-group3').show();
            form.find('.member_pro').val(pro_member_check).attr('checked','checked');
        }
         form.find('.member_pro_number').val(cae_number);
         if(which_pro == ''){
             form.find('.sel2_val').val('Please choose');
             form.find('.member_pro_which_pro').val('Please choose');
         }else{
             form.find('.sel2_val').val(which_pro);
             form.find('.member_pro_which_pro').val(which_pro);
         }
        if(member_society_check == 1){
            form.find('.form-group4').show();
            form.find('.member_society').val(member_society_check).attr('checked','checked');
        }
        if(mech_society_member == ''){
            form.find('.sel3_val').val('Please choose');
            form.find('.member_society_which_pro').val('Please choose');
        }else{
            form.find('.sel3_val').val(mech_society_member);
            form.find('.member_society_which_pro').val(mech_society_member);
        }
         form.find('.member_society_number').val(mech_society_number);
             $('.existing-container').children().remove();
             $('.existing-container').append(string);
     });
     $('.bth-data-submit').click(function(){
         if(!$(this).hasClass('bth-data-disable')){
             $('#idSubmitClaim').show();
             $('.publish-hidden-button').trigger('click');
         }

     });
     $('.bth-data-claim-submit').click(function(){
         if(!$(this).hasClass('bth-data-disable')){
             $('#idSubmitClaim').show().attr('data-id',$(this).data('id'));
         }
     });
     $('.page-home-song .btn-confirm').click(function(){
         window.location.href = 'submit-claim/'+$(this).parents('#idSubmitClaim').data('id');
     });
     $('.concept-songs-data').each(function(){
        var composers = $(this).find('.data-songs-composers');
        var lyricists = $(this).find('.data-songs-lyricists').text().trim();
        if(composers.text().trim() == ''){
            composers.text('-');
            $(this).find('.bth-data-submit').addClass('bth-data-disable');
            $(this).find('.publish-hidden-button').arrt('disabled','disabled');
        }
        if(lyricists.text().trim() == ''){
            composers.text('-');
            $(this).find('.bth-data-submit').addClass('bth-data-disable');
            $(this).find('.publish-hidden-button').arrt('disabled','disabled');
        }
     });
    $('.claim-upload-file').click(function(){
        if($('#file18').val() == ''){
            alert('Please choose a file to upload.');
        }else{
            $('#claim-upload-file,.form-upload-claim').hide();
            $('.form-file-claim').css('display','flex');
        }
    });
    $('.file-remove .fa-remove').click(function(){
        $('#file18').val('');
        $('.form-file-claim').hide();
        $('.form-upload-claim').show();
    });
     $('.form-date').datepicker({
     });
     $('.claim-submit').click(function(){
         $('#idSubmitClaim').show();
     });
     $('.claim-delete').click(function(e){
             e.preventDefault();
             $('#claim-delete').show();

     });
     $('.btn-confirm-detele').click(function(e){
         e.preventDefault();
          window.location.href = $('.claim-delete').attr('href');
     });
     $('.get-info-claim').click(function(){
         var id = $(this).data('id');
         $.get( "get-info-claim/"+id, function( data ) {
             $( ".page-claims .table-submitted" ).after(data);
         });
     });
     $("body").delegate(".remove-claims-info", "click", function(){
         $('#claims-info').remove();
     });
});



