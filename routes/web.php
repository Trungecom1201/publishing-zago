<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('admin','UserController@getLoginAdmin');
Route::get('admin/login','UserController@getLoginAdmin');
Route::post('admin/login','UserController@postLoginAdmin');
Route::get('admin/logout','UserController@getLogoutAdmin');
Route::group(['prefix'=>'admin', 'middleware'=>'adminLogin'], function(){
      Route::group(['prefix'=>'songs'], function(){
      Route::get('list','SongsController@getList');
      Route::get('delete/{id}','SongsController@getDelete');
      Route::get('list-claims','ClaimsController@getListClaims');
       Route::get('delete-claims/{id}','ClaimsController@getDelete');
   });

   Route::group(['prefix'=>'user'], function(){
      Route::get('danhsach','UserController@getDanhSach');
      Route::get('them','UserController@getThem');
      Route::post('them','UserController@postThem');
      Route::get('sua/{id}','UserController@getSua');
      Route::post('sua/{id}','UserController@postSua');
      Route::get('xoa/{id}','UserController@getXoa');
   });
});

Route::get('login',[
'as' => 'login',
'uses' => 'PageController@getLogin'
]);
Route::post('login',[
'as' => 'login',
'uses' => 'PageController@postLogin'
]);
Route::get('logout','PageController@getLogout');
Route::get('register', [
  'as' => 'register', 
  'uses' => 'PageController@getRegister'
]);
Route::post('register', [
  'as' => 'register', 
  'uses' => 'PageController@postRegister'
]);
Route::get('/',[
'as' => 'home',
'uses' => 'PageController@getHomePage'
]);
Route::get('index',[
'as' => 'home',
'uses' => 'PageController@getHomePage'
]);
Route::get('agree',[
'as' => 'agreement',
'uses' => 'PageController@getAgreement'
]);
Route::get('add-song',[
'as' => 'addsong',
'uses' => 'PageController@getAddSong'
]);
Route::get('publish-your-song',[
'as' => 'publish your song',
'uses' => 'PageController@getPublishYourSong'
]);
Route::get('saved-song',[
'as' => 'savedsong',
'uses' => 'PageController@getSavedSong'
]);
Route::get('published-song',[
'as' => 'published_song',
'uses' => 'PageController@getPublishedSong'
]);
Route::get('list-songs',[
'as' => 'listsongs',
'uses' => 'PageController@getListSongs'
]);
Route::get('saved-claims',[
'as' => 'saved_claims',
'uses' => 'PageController@getSavedClaims'
]);
Route::get('finished-publish-songs',[
'as' => 'finished_publish_songs',
'uses' => 'PageController@getFinishedPublishSongs'
]);
Route::get('edit-song2',[
'as' => 'edit_song2',
'uses' => 'PageController@getEditSong2'
]);
Route::get('copy-song',[
    'as' => 'copy_song',
    'uses' => 'PageController@getCopySong'
]);
Route::get('copy-song2',[
'as' => 'copy_song2',
'uses' => 'PageController@getCopySong2'
]);
Route::get('broadcast-claims',[
'as' => 'broadcast_claims',
'uses' => 'PageController@getBroadcastClaims'
]);
Route::get('abcd',[
'as' => 'abcd',
'uses' => 'PageController@getAbCd'
]);
Route::get('claims',[
    'as' => 'claims',
    'uses' => 'PageController@getClaims'
]);
Route::get('poiu',[
'as' => 'poiu',
'uses' => 'PageController@getPoIu'
]);
Route::get('add-claim-empty',[
'as' => 'add_claim_empty',
'uses' => 'PageController@getAddClaimEmpty'
]);
Route::get('edit-claim',[
'as' => 'edit_claim',
'uses' => 'PageController@getEditClaim'
]);
Route::group(['prefix'=>'contributors'], function(){            
      Route::post('add','ContributorsController@postAdd');
      Route::get('edit/{id}','ContributorsController@getSua');
      Route::post('sua/{id}','ContributorsController@postSua');
      Route::get('xoa/{id}','ContributorsController@getXoa');
   });
Route::group(['prefix'=>'songs'], function(){            
      Route::post('add','SongsController@postAdd');
      Route::post('add2','SongsController@postAdd2');
      Route::get('edit/{id}','SongsController@getSua');
      Route::post('sua/{id}','SongsController@postSua');
      Route::get('xoa/{id}','SongsController@getXoa');
   });
Route::get('session_form', function () {
    return view('ajax_session');
});
Route::post('set_session', 'SessionController@createsession');
Route::post('post_session', 'SessionController@postSession');
Route::get('allsession', 'SessionController@getsession');


Route::post('add-file', 'SongsController@postSong2');
Route::post('add-file2', 'SongsController@postSong3');
Route::get('delete-file/{name}', 'SongsController@postDeleteSong');
Route::get('delete-file2/{name}', 'SongsController@postDeleteSong2');
Route::post('save-contributor', 'ContributorsController@postSaveContributor');
Route::get('delete-contributor/{name1}', 'ContributorsController@getDeleteContributor');
Route::get('delete-edit-contributor/{name1}/{id_song}', 'ContributorsController@getEditDeleteContributor');
Route::post('add','SongsController@postAdd');
Route::get('edit-contributor/{name2}', 'ContributorsController@getEditContributor');
Route::get('edit-contributor/{name2}/{idsong}', 'ContributorsController@getEditContributorSong');
Route::post('edit-contributor1', 'ContributorsController@postEditContributor');
Route::post('edit-song-contributor/{id}', 'ContributorsController@postEditSongContributor');
Route::get('exis-contributors/{name}', 'ContributorsController@getExisSong');

Route::post('saved-song/add','SongsController@postAddSong2');

Route::post('list-songs/add', 'SongsController@postCopySong');
Route::post('save-edit-contributor-copysong', 'ContributorsController@postSaveEditContributorCopySong');
Route::post('add-song-copy','SongsController@postAddSongCopy');

Route::get('published-song/{id}','SongsController@getDetails');
Route::get('saved-song/{id}/{place_saved}','SongsController@getEditSongSaved');
Route::get('delete-music/{id_song_saved}', 'SongsController@postDeleteMusicOriginal');
Route::get('delete-music2/{id_song_saved}', 'SongsController@postDeleteMusicInstrumental');
Route::get('delete-contributor2/{idsong}/{idcontributor}', 'SongsController@getDeleteContributor2');
Route::post('save-contributor2', 'SongsController@postSaveContributor2');
Route::get('edit-contributor2/{idsong}/{idcontributor}', 'SongsController@getEditContributor2');
Route::post('save-edit-contributor2', 'SongsController@postSaveEditContributor');

Route::post('save-edit-song','SongsController@postSaveEditSong');
Route::get('delete-song1/{id_song1}', 'SongsController@getDeleteSong1');
Route::get('delete-file1', 'SongsController@getDeleteFileSong1');
Route::get('delete-file2', 'SongsController@getDeleteFileSong2');
Route::post('save-contributor-copysong', 'ContributorsController@postSaveContributorCopySong');
Route::get('delete-contributor-copysong/{name1}', 'ContributorsController@getDeleteContributorCopySong');
Route::get('edit-contributor-copysong/{idcontributor}', 'ContributorsController@getEditContributorCopySong');

Route::post('add-claim', 'ClaimsController@postSaveSessionClaims');

route::get('insert-claim',function(){
    Schema::table('contributors',function($tb){
        $tb->string('id_user')->nullable();
    });
    echo 'ok';
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
route::get('clear-session','PageController@getClearSession');
route::get('clear-contributor','PageController@getClearContributor');
route::get('db-claim',function(){
    Schema::create('claims',function($tb){
        $tb->increments('id');
        $tb->string('song_title')->nullable();
        $tb->string('station_channel')->nullable();
        $tb->string('program')->nullable();
        $tb->date('air_date')->nullable();
        $tb->string('country')->nullable();
        $tb->string('evidence')->nullable();
        $tb->string('id_song')->nullable();
        $tb->string('id_user')->nullable();
        $tb->string('file')->nullable();
        $tb->string('file_url')->nullable();
        $tb->integer('radio_tv')->nullable();
        $tb->integer('publish')->nullable();
        $tb->timestamps();
    });
    echo "kem";
});
route::get('submit-claim/{id}','ClaimsController@getSubmitClaims');
route::get('edit-claim/{id}','ClaimsController@getEditClaims');
route::post('edit-claim/{id}','ClaimsController@postEditClaims');
route::get('detele-claim/{id}','ClaimsController@getDeleteClaim');
route::get('get-info-claim/{id}','ClaimsController@getInfoClaim');