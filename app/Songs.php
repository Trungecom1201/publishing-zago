<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Songs extends Model
{
    protected $fillable = [
    'user_first_name', 'user_last_name', 'user_email', 'user_phone', 'user_member_type', 'song_title', 'status', 'duration','contains_samples','rights_to_use_samples','samples_info','remix','prev_registered_with_pro','which_pro','prs_tunecode','audio_original','file_size','file_url','audio_instrumental','file_size_instrumental','file_url_instrumental','equal_shares',
];
}
/*
function contributors() {

    return $this->hasMany('Contributors','id_song');
}

public function skus()
{
    return hasMany('App/Sku','products_id');
}
*/