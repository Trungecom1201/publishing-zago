<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contributors extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'role', 'share', 'pro_member_check', 'which_pro', 'cae_number', 'member_society_check','mech_society_member','mech_society_number','id_song',
    ];
}
/*
function songs()
    {
        return $this->belongsTo('Songs','id_song','id');
    }
*/