<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BroadcastClaim extends Model
{
    protected $table = "claims";
}
