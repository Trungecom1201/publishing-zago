<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Contributors;
use Illuminate\Support\Facades\Session;
use function PHPSTORM_META\type;
use Auth;
class ContributorsController extends Controller
{
    public function postAdd(Request $request)
    {
        $contributors = new Contributors;
        if ($request->option18 == "New") {
            $contributors->first_name = $request->txtFirstName;
            $contributors->last_name = $request->txtLastName;


            //$contributors->role = $request->option2;
            if ($request->MemberPro == 1) {
                $contributors->pro_member = $request->sel3;
                $contributors->cae_number = $request->txtCAE_Number;
            }
            if ($request->MemberSociety == 1) {
                $contributors->mech_society_member = $request->sel2;
                $contributors->mech_society_number = $request->txtSocietyNumber;
            }
        } else {
            $contributors->first_name = $request->txtFirstName2;
            $contributors->last_name = $request->txtLastName2;
            $member_pro = $request->member_pro;
            $contributors->pro_member = str_replace("Yes, ", "", $member_pro);
            $contributors->cae_number = $request->cae_number;
            $mech_society_member = $request->mech_society_member;
            $contributors->mech_society_member = str_replace("Yes, ", "", $mech_society_member);
            $contributors->mech_society_number = $request->mech_society_number;
        }
        if ($request->option2 == "Both") {
            $contributors->role = "Composer + Lyricist";
        } else {
            $contributors->role = $request->option2;
        }
        $contributors->id_user = Auth::id();
        $contributors->id_song = $request->multi_song;
        $contributors->save();
        return redirect('add-song');
    }

    public function getSua($id)
    {
        $theloai = TheLoai::find($id);
        return view('admin.theloai.sua', ['theloai' => $theloai]);
    }

    public function postSua(Request $request, $id)
    {
        $theloai = TheLoai::find($id);
        $this->validate($request,
            [
                'txtName' => 'required|min:3|max:100|unique:TheLoai,Ten'
            ],
            [
                'txtName.required' => 'Bạn chưa nhập tên thể loại',
                'txtName.unique' => 'Tên thể loại này đã có rồi',
                'txtName.min' => 'Ngắn quá, phải từ 3 ký tự trở lên',
                'txtName.max' => 'Dài quá, tối đa 100 ký tự',
            ]);
        $theloai->Ten = $request->txtName;
        $theloai->TenKhongDau = changeTitle($request->txtName);
        $theloai->save();
        return redirect('admin/theloai/sua/' . $id)->with('thongbao', 'Sửa thành công!');
    }

    public function getXoa($id)
    {
        $theloai = TheLoai::find($id);
        $theloai->delete();
        return redirect('admin/theloai/danhsach')->with('thongbao', 'Xóa thành công!');
    }

    public function postSaveContributor(Request $request)
    {

        if($request->option18 == "New") {
            $first_name = $request->txtFirstName;
            $last_name = $request->txtLastName;
            if ($request->option2 == "Both") {
                $role = "Composer + Lyricist";
            } else {
                $role = $request->option2;
            }
            if ($request->MemberPro == 1) {
                $member_pro = 1;
                $pro_member = $request->sel2_val;
                $cae_number = $request->txtCAE_Number;
            } else {
                $member_pro = 0;
                $pro_member = "";
                $cae_number = "";
            }
            if ($request->MemberSociety == 1) {
                $member_society = 1;
                $mech_society_member = $request->sel3_val;
                $mech_society_number = $request->txtSocietyNumber;
            } else {
                $member_society = 0;
                $mech_society_member = "";
                $mech_society_number = "";
            }
        }else{
            $first_name = $request->txtFirstName;
            $last_name = $request->txtLastName;
            if ($request->option2 == "Both") {
                $role = "Composer + Lyricist";
            } else {
                $role = $request->option2;
            }
            if ($request->MemberPro == 1) {
                $member_pro = 1;
                $pro_member = $request->sel2_val;
                $cae_number = $request->txtCAE_Number;
            } else {
                $member_pro = 0;
                $pro_member = "";
                $cae_number = "";
            }
            if ($request->MemberSociety == 1) {
                $member_society = 1;
                $mech_society_member = $request->sel3_val;
                $mech_society_number = $request->txtSocietyNumber;
            } else {
                $member_society = 0;
                $mech_society_member = "";
                $mech_society_number = "";
            }

            $data = session('song_info');
            if(!empty($data[$request->idSong][0][12])){
                $count = count($data[$request->idSong][0][12]);
                foreach ($data[$request->idSong][0][12] as $key1 => $val1){
                    Session()->push('comtributors', $val1);
                }
            }
        }
        $arr_str = [$first_name, $last_name, $role, $member_pro, $pro_member, $cae_number, $member_society, $mech_society_member, $mech_society_number,0];
            Session()->push('comtributors', $arr_str);
            Session()->save();
        $contributor2221 =  Session()->get('comtributors');


foreach ($contributor2221 as $key1 => $val1) {
    echo '<div class="contributors id-'.$key1.'" >'.'<span id="'.$val1[0]."_".$val1[1].'" hidden>'.'</span>'.
        '<span class="contributors-name">'.$val1[0]." ".$val1[1].'</span>'.
        '<input type="hidden" name="contributors_firstname[]" value="'.$val1[0].'" />'.
        '<input type="hidden" name="contributors_lastname[]" value="'.$val1[1].'"/>'.
        '<input type="hidden" name="contributors_role[]" value="'.$val1[2].'"/>'.
        '<input type="hidden" name="contributors_memberpro[]" value="'.$val1[3].'" />'.
        '<input type="hidden" name="contributors_promember[]" value="'.$val1[4].'"/>'.
        '<input type="hidden" name="contributors_cae_number[]" value="'.$val1[5].'"/>'.
        '<input type="hidden" name="contributors_member_society[]" value="'.$val1[6].'" />'.
        '<input type="hidden" name="contributors_mech_society_member[]" value="'.$val1[7].'"/>'.
        '<input type="hidden" name="contributors_mech_society_number[]" value="'.$val1[8].'"/>'.
        '<input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal"/>'.
        '<span class="role">'.$val1[2].'</span>'.
        '<span class="ratio-percent" hidden>'.'</span>'.
        '<input type="text" name="contributors_equal2[]" class="percent-share2" placeholder="0" style="
       ">'.'<span class="percent-icon1">'."&#37;".'</span>'.
        '<i class="fa fa-pencil" data-name2="'.$key1.'" >'.'</i>'.
        '<i class="fa fa-remove fa-remove2" data-name="'.$key1.'" style="font-size:24px; color: red">'.'</i>'.    '<div class="border">'.'</div>'.
        '</div>';
}
       echo '<div class="half-past-percent">'.'<p class="check-agreement check-contributors">'.'<span class="squaredTwo">'.
  '<input type="checkbox" value="None" id="squaredEight" name="check">'.
  '<label for="squaredEight">'.'</label>'.
'</span>'.'<span class="equal-share equal-share2">'. "Equal share" .'</span>'.'</p>'.'</div>';

    }
    public function getDeleteContributor($name1) {
        foreach (Session()->get('comtributors') as $key => $val) {
            if($key == $name1){
                Session()->pull('comtributors.'.$key); // retrieving pen and removing
                break;
            }
        }
 }
    public function getEditDeleteContributor($name1,$id_song) {

        $data_song = session("song_info");
        $data = $data_song[$id_song][0][12];

        if(!empty($data_contributors)){
            $data_contributors = session("comtributors");
        }

        unset($data[$name1]);
        if(!empty($data)){
            foreach ($data as $key => $val) {
                $new_data = $val;
            }
            session()->push('comtributors',$new_data);
        }else{
            session()->put('contributors_edit',1);
        }




    }

    public function getEditContributor($name2) {

        $contributors = Contributors::where('id_user',Auth::id())->orderBy('id', 'desc')->get();
        foreach (Session()->get('comtributors') as $key => $val) {
            if($key == $name2){
                $check1 = "";
                $check2 = "";
                $check3 = "";
                if($val[2] == "Composer"){
                    $check1 = "checked";
                }
                if($val[2] == "Lyricist"){
                    $check2 = "checked";
                }
                if($val[2] == "Composer + Lyricist"){
                    $check3 = "checked";
                }
                if($val[3] == 1){
                    $check_pro = "checked";
                    $check_pro2 = $val[4];
                    $cae_number2 = $val[5];
                    $show_pro = "style='display:block'";
                }else{
                    $check_pro = "";
                    $check_pro2 = "";
                    $cae_number2 = "";
                    $show_pro = "style='display:none'";
                }
                if($val[6] == 1){
                    $check_society = "checked";
                    $which_society2 = $val[7];
                    $cae_society = $val[8];
                    $show_society = "style='display:block'";
                }else{
                    $check_society = "";
                    $which_society2 = "";
                    $cae_society = "";
                    $show_society = "style='display:none'";
                }

               echo '<div class="modal-content  modal-add-contributor" data-id="'.$key.'">'.'<p class="title-home-warning title-upload-song">'.
                   "EDIT CONTRIBUTOR".
                   '</p>'.'<p class="btn-choose-file contributor-radio1">'.

                   '<input id="new22" name="option24" type="radio" value="New" onclick="edit_contributorFunction2()" checked="" />'
                   .'<label for="new22" class="add-new-contributor">'."New".'</label>'.
                   '<input id="new23" name="option24" type="radio" value="Existing" onclick="edit_interfaceExistingFunction()" />'.
                   '<label for="new23" class="add-exist-contributor">'."Existing".'</label>'.
                   '</p>'.'<div class="form-group form-group60" hidden>'.
                   '<select class="form-control form-control-pro show-member exis-contributors" id="sel1">';
                if(count($contributors) > 0){
                    echo ' <option value="Please choose">Please choose</option>';
                    foreach ($contributors as $key => $data){
                        echo ' <option data-first_name="'.$data->first_name.'" data-last_name="'.$data->last_name.'" data-role="'.$data->role.'" data-share="'.$data->share.'" data-pro_member_check="'.$data->pro_member_check.'" data-which_pro="'.$data->which_pro.'" data-cae_number="'.$data->cae_number.'" data-member_society_check="'.$data->member_society_check.'" data-mech_society_member="'.$data->mech_society_member.'" data-mech_society_number="'.$data->mech_society_number.'" value="'.$key.'">'.$data->first_name.' '.$data->last_name.'</option>';
                    }
                }else{
                    echo ' <option value="Please choose">No contributors </option>';
                }
              echo '</select><div class="existing-container"></div>'.
                   '</div>'.'<div class="member_contributor" hidden>'.
                   '<p>'.'<span class="member_left1 color-member-left">'."MEMBER of a PRO:".'</span>'.'<span class="member_right">'."Yes, Buma".'</span>'.'</p>'.
                '<p>'.'<span class="member_left2 color-member-left">'."CAE Number:".'</span>'.'<span class="member_right">'."189904BM".'</span>'.'</p>'.
                '<p>'.'<span class="member_left3 color-member-left">'."MEMBER of a mech, society:".'</span>'.'<span class="member_right">'."Yes, Stemra".'</span>'.'</p>'.
                '<p>'.'<span class="member_left4 color-member-left">'."CAE Number:".'</span>'.'<span class="member_right">'."2884242RP".'</span>'.'</p>'.
                '</div>'.'<form id="userform11" method="POST">'.'<input type="hidden" name="_token" value="'.csrf_token().'">'.'<input type="hidden" name="id_contributor" value="'.$name2.'">'.'<p class="text-upload-audio">'.'<div class="form-group form-group50 edit_hide">'.
                        '<label class="label-title">'."First name".'<img src="source/image/content/question.jpg">'.'</label>'.

                            '<input type="text" name="txtFirstName" value="'.$val[0].'" class="form-control form-title form-first-name" placeholder="First name">'.

  '</div>'.'</p>'.
  '<p class="text-upload-audio">'.
               '<div class="form-group form-group50 edit_hide">'.
                        '<label class="label-title">'."Last name".'<img src="source/image/content/question.jpg">'.'</label>'.

                            '<input type="text" name="txtLastName" class="form-control form-title form-last-name" value="'.$val[1].'" placeholder="Last name">'.

  '</div>'.
               '</p>'.
                   '<div class="select-option-form"><p class="check-agreement check-agreement2  form-group50 edit_hide">'.'<span class="squaredTwo">'.
  '<input type="checkbox" value="'.$val[3].'" id="squaredSix33" class="which_pro1 member_pro" name="check" onclick="shsel8Function()" '.$check_pro.' >'.
  '<label for="squaredSix33">'.'</label>'.'</span>'.'<span class="color-text-checkbox color-text-checkbox2">'." Member of a PRO".'<img src="source/image/content/question.jpg">'.'</span>'.
'</p>'.'<div class="form-group form-group3 " '.$show_pro.'>'.

  '<label class="label-title label-title-2">'."Which PRO". '<img src="source/image/content/question.jpg">'.'</label>'.
  '<select class="form-control form-control-pro form-sel1 member_pro_which_pro" id="sel1" name="which_pro_option">'.
    '<option>'."Please choose".'</option>';
if($check_pro2 == 'BUMA'){
    echo    '<option selected vaule="BUMA">'."BUMA".'</option>'.'<option vaule="Stemra">'."Stemra".'</option>';
}else{
    echo    '<option selected  vaule="BUMA">'."BUMA".'</option>'.'<option vaule="Stemra">'."Stemra".'</option>';
}

  echo '</select>'.
                   '<input type="hidden" name="sel_val1" class="sel_val1" value="'.$check_pro2.'">'.
'</div>'.
                   '<div class="form-group form-group3" '.$show_society.'>'.
                        '<label class="label-title">'."CAE number".'<img src="source/image/content/question.jpg">'.'</label>'.

                            '<input type="text" name="cae_number3" class="form-control form-title member_pro_number" placeholder="Title" value="'.$cae_number2.'">'.

  '</div>'.'<p class="check-agreement check-agreement2 form-group5 edit_hide">'.'<span class="squaredTwo">'.
  '<input type="checkbox" value="'.$val[6].'" id="squaredSeven44" class="mechanical_society1 member_society" onclick="shsel7Function()" name="check_society2" '.$check_society.'>'.
  '<label for="squaredSeven44">'.'</label>'.
'</span>'.'<span class="color-text-checkbox">'." Member of a mechanical society ".'<img src="source/image/content/question.jpg">'.'</span>'.
'</p>'.
'<div class="form-group form-group4" hidden>'.

  '<label class="label-title label-title-2">'."Which society". '<img src="source/image/content/question.jpg">'.'</label>'.
  '<select class="form-control form-control-pro form-sel2 member_society_which_pro" name="society_option" id="sel1">'.
    '<option>'."Please choose".'</option>';
        if($which_society2 == 'BUMA'){
            echo    '<option selected vaule="BUMA">'."BUMA".'</option>'.'<option vaule="Stemra">'."Stemra".'</option>';
        }else{
            echo    '<option selected  vaule="BUMA">'."BUMA".'</option>'.'<option vaule="Stemra">'."Stemra".'</option>';
        }
  echo '</select>'.'<input type="hidden" name="sel_val2" class="sel_val2" value="'.$which_society2.'">'.
'</div>'.
               '<div class="form-group form-group4" hidden>'.
                        '<label class="label-title">CAE number<img src="source/image/content/question.jpg">'.'</label>'.

                            '<input type="text" name="cae_number4" class="form-control form-title member_society_number" placeholder="Title" value="'.$cae_society.'">'.

            '</div></div>'.
               '<div class="form-group">'.
                        '<label class="label-title label-title-role">'."Role".'<img src="source/image/content/question.jpg">'.'</label>'.
               '</div>'.
'<p class="btn-choose-file contributor-radio1">'.

  '<input id="composer25" name="option2" type="radio" value="Composer" '.$check1.'/>'.
  '<label for="composer25">'."Composer".'</label>'.
  '<input id="lyricist26" name="option2" type="radio" value="Lyricist" '.$check2.'/>'.
  '<label for="lyricist26">'."Lyricist".'</label>'.
  '<input id="both27" name="option2" type="radio" value="Both" '.$check3.'/>'.
  '<label for="both27">Both</label>'.


  '</p>'.


                '<p class="btn-choose-file btn-add-contributor5 color-a">'.
      '<span id="btnchange-color" class="btn btn-upload-audio btn-upload-audio2 btn-add-contributor2 btn-edit-contributor add-btn-save-contributor">'."SAVE".'</span>'.
      '<a onclick="document.getElementById(\'idEditContributor\').style.display=\'none\'">'."Cancel".'</a>'.
                '</p>'.'</form>'.
                   '</div>';
                echo "<script>";
                echo "if ($('input.which_pro1').is(':checked')){jQuery('.form-group3').css('display', 'block');} ";
                echo "if ($('input.mechanical_society1').is(':checked')){jQuery('.form-group4').css('display', 'block');} ";
                echo "</script>";


                echo "<script>";
                echo "$( 'input.which_pro1' ).change(function() {
                    if($('input.which_pro1').is(':checked')){
                        $( 'input.which_pro1' ).attr('value', 1);
                    }else{
                        $( 'input.which_pro1' ).attr('value', 0);
                    }
                    });";
                echo "$( 'input.mechanical_society1' ).change(function() {
                    if($('input.mechanical_society1').is(':checked')){
                        $( 'input.mechanical_society1' ).attr('value', 1);
                    }else{
                        $( 'input.mechanical_society1' ).attr('value', 0);
                    }
                    });";

                echo "</script>";

                echo "<script>";
                echo "$( '.form-sel1' ).change(function() {
                    var optsel4 = $( '.form-sel1 option:selected' ).text();
                       $('.sel_val1').val(optsel4);
                    });";
                echo "$( '.form-sel2' ).change(function() {
                    var optsel5 = $( '.form-sel2 option:selected' ).text();
                       $('.sel_val2').val(optsel5);
                    });";

                echo "</script>";
                break;

            }

        }
    }

    public function postEditContributor(Request $request) {

        $contributor_id = $request->id_contributor;
        $first_name = $request->txtFirstName;
        $last_name = $request->txtLastName;
        if ($request->option2 == "Both") {
            $role = "Composer + Lyricist";
        } else {
            $role = $request->option2;
        }
        if($request->check == 1) {
            $member_pro = 1;
            $which_pro_option = $request->sel_val1;
            $cae_number3 = $request->cae_number3;
        }else{
            $member_pro = 0;
            $which_pro_option = "";
            $cae_number3 = "";
        }
        if($request->check_society2 == 1) {
            $check_society2 = 1;
            $society_option = $request->sel_val2;
            $cae_number4 = $request->cae_number4;
        }else{
            $check_society2 = 0;
            $society_option = "";
            $cae_number4 = "";
        }
        $update_contributor = Session()->get('comtributors');
        if(isset($update_contributor[$contributor_id])){
            $update_contributor[$contributor_id][0] = $first_name;
            $update_contributor[$contributor_id][1] = $last_name;
            $update_contributor[$contributor_id][2] = $role;
            $update_contributor[$contributor_id][3] = $member_pro;
            $update_contributor[$contributor_id][4] = $which_pro_option;
            $update_contributor[$contributor_id][5] = $cae_number3;
            $update_contributor[$contributor_id][6] = $check_society2;
            $update_contributor[$contributor_id][7] = $society_option;
            $update_contributor[$contributor_id][8] = $cae_number4;
            Session()->put("comtributors", $update_contributor);

            echo '<span id="'.$first_name."_".$last_name.'" hidden>'.'</span>'.
                '<span class="contributors-name">'.$first_name." ".$last_name.'</span>'.'<input type="hidden" name="contributors_firstname[]" value="'.$first_name.'">'.
                '<input type="hidden" name="contributors_lastname[]" value="'.$last_name.'">'.
            '<input type="hidden" name="contributors_role[]" value="'.$role.'">'.
            '<input type="hidden" name="contributors_memberpro[]" value="'.$member_pro.'">'.
            '<input type="hidden" name="contributors_promember[]" value="'.$which_pro_option.'">'.
            '<input type="hidden" name="contributors_cae_number[]" value="'.$cae_number3.'">'.
            '<input type="hidden" name="contributors_member_society[]" value="'.$check_society2.'">'.
            '<input type="hidden" name="contributors_mech_society_member[]" value="'.$society_option.'">'.
            '<input type="hidden" name="contributors_mech_society_number[]" value="'.$cae_number4.'">'.
            '<input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal">'.
                '<span class="role">'.$role.'</span>'.
                '<span class="ratio-percent" hidden="">'.'</span>'.
            '<input type="hidden" class="ratio-percent-val">'.
            '<input type="text" name="contributors_equal2[]" class="percent-share2" placeholder="0" value="" style="
       ">'.'<span class="percent-icon1">'."%".'</span>'.
                '<i class="fa fa-pencil" data-name2="'.$contributor_id.'">'.'</i>'.
                '<i class="fa fa-remove fa-remove2" data-name="'.$contributor_id.'" style="font-size:24px; color: red">'.'</i>'.    '<div class="border">'.'</div>';
        }
        }

    public function getExisSong($name) {
         foreach (Session()->get('comtributors') as $key => $val) {

            if ($key == $name) {
if($val[4] == "No choose" || $val[4] == ""){
    $pro1 = "No ";
}else{
    $pro1 = "Yes, ";
}
if($val[7] == "No choose" || $val[7] == ""){
                    $pro2 = "No ";
}else{
                    $pro2 = "Yes, ";
}
                $check1 = "";
                $check2 = "";
                $check3 = "";
                if($val[2] == "Composer"){
                    $check1 = "checked";
                }
                if($val[2] == "Lyricist"){
                    $check2 = "checked";
                }
                if($val[2] == "Composer + Lyricist"){
                    $check3 = "checked";
                }
        echo '<input type="hidden" name="txtFirstName3" value="'.$val[0].'">'.
            '<input type="hidden" name="txtLastName3" value="'.$val[1].'">'.
            '<input type="hidden" name="nbMember_Pro" value="'.$val[3].'">'.
             '<p>'.'<span class="member_left1 color-member-left">'."MEMBER of a PRO:".'</span>'.'<span>'.$pro1.'</span>'.'<input type="text" name="sel2_val3" class="border_contributor member_pro member_pro2" value="'.$val[4].'">'.'</p>'.
        '<p>'.'<span class="member_left2 color-member-left">'."CAE Number:".'</span>'.'<input type="text" name="nbCAE_Number3" class="border_contributor cae_number" value="'.$val[5].'">'.'</p>'.
            '<input type="hidden" name="nbMember_Society" value="'.$val[6].'">'.
        '<p>'.'<span class="member_left3 color-member-left">'."MEMBER of a mech, society:".'</span>'.'<span>'.$pro2.'</span>'.'<input type="text" name="txtSociety_Member3" class="border_contributor mech_society_member member_pro2" value="'.$val[7].'">'.'</p>'.
        '<p>'.'<span class="member_left4 color-member-left">'."CAE Number:".'</span>'.'<input type="text" name="nbSociety_Number3" class="border_contributor mech_society_number" value="'.$val[8].'">'.'</p>'.

            '<div class="form-group">'.
                        '<label class="label-title label-title-role">'."Role".'<img src="source/image/content/question.jpg">'.'</label>'.
                        '</div>'.
    '<p class="btn-choose-file contributor-radio1">'
  .'<input id="composer8" name="option20" type="radio" value="Composer" '.$check1.'>'.
  '<label for="composer8">'."Composer".'</label>'.
  '<input id="lyricist8" name="option20" type="radio" value="Lyricist" '.$check2.'>'.
  '<label for="lyricist8">'."Lyricist".'</label>'.
  '<input id="both8" name="option20" type="radio" value="Both" '.$check3.'>'.
  '<label for="both8">'."Both".'</label>'.


  '</p>';
            }
        }

    }

    public function postSaveContributorCopySong(Request $request)
    {
        if($request->option18 == "New") {
            $first_name = $request->txtFirstName;
            $last_name = $request->txtLastName;
            if ($request->option2 == "Both") {
                $role = "Composer + Lyricist";
            } else {
                $role = $request->option2;
            }
            if ($request->MemberPro == 1) {
                $member_pro = 1;
                $pro_member = $request->sel2_val;
                $cae_number = $request->txtCAE_Number;
            } else {
                $member_pro = 0;
                $pro_member = "";
                $cae_number = "";
            }
            if ($request->MemberSociety == 1) {
                $member_society = 1;
                $mech_society_member = $request->sel3_val;
                $mech_society_number = $request->txtSocietyNumber;
            } else {
                $member_society = 0;
                $mech_society_member = "";
                $mech_society_number = "";
            }
        }else{
            $first_name = $request->txtFirstName3;
            $last_name = $request->txtLastName3;
            if ($request->option20 == "Both") {
                $role = "Composer + Lyricist";
            } else {
                $role = $request->option20;
            }
            $member_pro = $request->nbMember_Pro;
            $pro_member = $request->sel2_val3;
            $cae_number = $request->nbCAE_Number3;
            $member_society = $request->nbMember_Society;
            $mech_society_member = $request->txtSociety_Member3;
            $mech_society_number = $request->nbSociety_Number3;
        }
        $arr_str = [$first_name, $last_name, $role, $member_pro, $pro_member, $cae_number, $member_society, $mech_society_member, $mech_society_number];

            
        if(Session()->get('comtributors') != null){
            Session()->push("comtributors.0", $arr_str);
            Session()->save();
        }else{
            Session()->push("commtributors", $arr_str);
            Session()->save();

        }



$contributors =  Session()->get('comtributors');
foreach ($contributors as $key => $val) {
  foreach ($val as $key1 => $val1) {
    echo '<div class="contributors id-'.$key1.'" >'.'<span id="'.$val1[0]."_".$val1[1].'" hidden>'.'</span>'.
        '<span class="contributors-name">'.$val1[0]." ".$val1[1].'</span>'.
        '<input type="hidden" name="contributors_firstname[]" value="'.$val1[0].'" />'.
        '<input type="hidden" name="contributors_lastname[]" value="'.$val1[1].'"/>'.
        '<input type="hidden" name="contributors_role[]" value="'.$val1[2].'"/>'.
        '<input type="hidden" name="contributors_memberpro[]" value="'.$val1[3].'" />'.
        '<input type="hidden" name="contributors_promember[]" value="'.$val1[4].'"/>'.
        '<input type="hidden" name="contributors_cae_number[]" value="'.$val1[5].'"/>'.
        '<input type="hidden" name="contributors_member_society[]" value="'.$val1[6].'" />'.
        '<input type="hidden" name="contributors_mech_society_member[]" value="'.$val1[7].'"/>'.
        '<input type="hidden" name="contributors_mech_society_number[]" value="'.$val1[8].'"/>'.
        '<input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal"/>'.
        '<span class="role">'.$val1[2].'</span>'.
        '<span class="ratio-percent" hidden>'.'</span>'.
        '<input type="text" name="contributors_equal2[]" class="percent-share2" placeholder="0" style="
       ">'.'<span class="percent-icon1">'."&#37;".'</span>'.
        '<i class="fa fa-pencil" data-name2="'.$key1.'">'.'</i>'.
        '<i class="fa fa-remove fa-remove2" data-name="'.$key1.'" style="font-size:24px; color: red">'.'</i>'.    '<div class="border">'.'</div>'.
        '</div>';
}
}
       echo '<div class="half-past-percent">'.'<p class="check-agreement check-contributors">'.'<span class="squaredTwo">'.
  '<input type="checkbox" value="None" id="squaredEight" name="check">'.
  '<label for="squaredEight">'.'</label>'.
'</span>'.'<span class="equal-share equal-share2">'. "Equal share" .'</span>'.'</p>'.'</div>';

    }

    public function getDeleteContributorCopySong($name1) {
        foreach (Session()->get('comtributors') as $key => $val) {
          foreach ($val as $key1 => $val1) {
            if($key1 == $name1){
                Session()->pull('contributors.0.'.$key1);
                Session()->save();
                 // retrieving pen and removing
                break;

            }
          }
        }
    }

    public function getEditContributorCopySong($name2) {
        foreach (Session()->get('comtributors') as $key1 => $val1) {
  foreach ($val1 as $key => $val) {
            if($key == $name2){
                $check1 = "";
                $check2 = "";
                $check3 = "";
                if($val[2] == "Composer"){
                    $check1 = "checked";
                }
                if($val[2] == "Lyricist"){
                    $check2 = "checked";
                }
                if($val[2] == "Composer + Lyricist"){
                    $check3 = "checked";
                }
                if($val[3] == 1){
                    $check_pro = "checked";
                    $check_pro2 = $val[4];
                    $cae_number2 = $val[5];
                }else{
                    $check_pro = "";
                    $check_pro2 = "";
                    $cae_number2 = "";
                }
                if($val[6] == 1){
                    $check_society = "checked";
                    $which_society2 = $val[7];
                    $cae_society = $val[8];

                }else{
                    $check_society = "";
                    $which_society2 = "";
                    $cae_society = "";
                }
               echo '<div class="modal-content  modal-add-contributor" data-id="'.$key.'">'.'<p class="title-home-warning title-upload-song">'.
                   "EDIT CONTRIBUTOR".
                   '</p>'.'<p class="btn-choose-file contributor-radio1">'.

                   '<input id="new22" name="option24" type="radio" value="New" onclick="edit_contributorFunction2()" checked="" />'
                   .'<label for="new22">'."New".'</label>'.
                   '<input id="new23" name="option24" type="radio" value="Existing" onclick="edit_interfaceExistingFunction()" />'.
                   '<label for="new23">'."Existing".'</label>'.

                   '</p>'.'<div class="form-group form-group60" hidden>'.

                   '<label class="label-title label-title-2">'."Which PRO".'<img src="source/image/content/question.jpg">'.'</label>'.
                '<select class="form-control form-control-pro show-member" id="sel1">'.
                '<option value="">'.'</option>'.

                '</select>'.
                '</div>'
.'<div class="member_contributor" hidden>'.
                '<p>'.'<span class="member_left1 color-member-left">'."MEMBER of a PRO:".'</span>'.'<span class="member_right">'."Yes, Buma".'</span>'.'</p>'.
                '<p>'.'<span class="member_left2 color-member-left">'."CAE Number:".'</span>'.'<span class="member_right">'."189904BM".'</span>'.'</p>'.
                '<p>'.'<span class="member_left3 color-member-left">'."MEMBER of a mech, society:".'</span>'.'<span class="member_right">'."Yes, Stemra".'</span>'.'</p>'.
                '<p>'.'<span class="member_left4 color-member-left">'."CAE Number:".'</span>'.'<span class="member_right">'."2884242RP".'</span>'.'</p>'.
                '</div>'.'<form id="userform11" method="POST">'.'<input type="hidden" name="_token" value="'.csrf_token().'">'.'<input type="hidden" name="id_contributor" value="'.$key.'">'.'<p class="text-upload-audio">'.'<div class="form-group form-group50 edit_hide">'.
                        '<label class="label-title">'."First name".'<img src="source/image/content/question.jpg">'.'</label>'.

                            '<input type="text" name="txtFirstName" value="'.$val[0].'" class="form-control form-title" placeholder="First name">'.

  '</div>'.'</p>'.
  '<p class="text-upload-audio">'.
               '<div class="form-group form-group50 edit_hide">'.
                        '<label class="label-title">'."Last name".'<img src="source/image/content/question.jpg">'.'</label>'.

                            '<input type="text" name="txtLastName" class="form-control form-title" value="'.$val[1].'" placeholder="Last name">'.

  '</div>'.
               '</p>'.
                   '<p class="check-agreement check-agreement2  form-group50 edit_hide">'.'<span class="squaredTwo">'.
  '<input type="checkbox" value="'.$val[3].'" id="squaredSix33" class="which_pro1" name="check" onclick="shsel8Function()" '.$check_pro.' >'.
  '<label for="squaredSix33">'.'</label>'.'</span>'.'<span class="color-text-checkbox color-text-checkbox2">'." Member of a PRO".'<img src="source/image/content/question.jpg">'.'</span>'.
'</p>'.'<div class="form-group form-group300 edit_hide" hidden>'.

  '<label class="label-title label-title-2">'."Which PRO". '<img src="source/image/content/question.jpg">'.'</label>'.
  '<select class="form-control form-control-pro form-sel1" id="sel1" name="which_pro_option">'.
    '<option value="'.$check_pro2.'">'.$check_pro2.'</option>'.'<option>'."Please choose".'</option>'.
            '<option>'."BUMA".'</option>'.'<option>'."Stemra".'</option>'.
  '</select>'.
                   '<input type="hidden" name="sel_val1" class="sel_val1" value="'.$check_pro2.'">'.
'</div>'.
                   '<div class="form-group form-group300 edit_hide" hidden>'.
                        '<label class="label-title">'."CAE number".'<img src="source/image/content/question.jpg">'.'</label>'.

                            '<input type="text" name="cae_number3" class="form-control form-title" placeholder="Title" value="'.$cae_number2.'">'.

  '</div>'.'<p class="check-agreement check-agreement2 form-group5 edit_hide">'.'<span class="squaredTwo">'.
  '<input type="checkbox" value="'.$val[6].'" id="squaredSeven44" class="mechanical_society1" onclick="shsel7Function()" name="check_society2" '.$check_society.'>'.
  '<label for="squaredSeven44">'.'</label>'.
'</span>'.'<span class="color-text-checkbox">'." Member of a mechanical society ".'<img src="source/image/content/question.jpg">'.'</span>'.
'</p>'.
'<div class="form-group form-group4" hidden>'.

  '<label class="label-title label-title-2">'."Which society". '<img src="source/image/content/question.jpg">'.'</label>'.
  '<select class="form-control form-control-pro form-sel2" name="society_option" id="sel1">'.
    '<option value="'.$which_society2.'">'.$which_society2.'</option>'.'<option>'."Please choose".'</option>'.
                   '<option>'."BUMA".'</option>'.'<option>'."Stemra".'</option>'.
  '</select>'.'<input type="hidden" name="sel_val2" class="sel_val2" value="'.$which_society2.'">'.
'</div>'.
               '<div class="form-group form-group4" hidden>'.
                        '<label class="label-title">CAE number<img src="source/image/content/question.jpg">'.'</label>'.

                            '<input type="text" name="cae_number4" class="form-control form-title" placeholder="Title" value="'.$cae_society.'">'.

            '</div>'.
               '<div class="form-group">'.
                        '<label class="label-title label-title-role">'."Role".'<img src="source/image/content/question.jpg">'.'</label>'.
               '</div>'.
'<p class="btn-choose-file contributor-radio1">'.

  '<input id="composer25" name="option28" type="radio" value="Composer" '.$check1.'/>'.
  '<label for="composer25">'."Composer".'</label>'.
  '<input id="lyricist26" name="option28" type="radio" value="Lyricist" '.$check2.'/>'.
  '<label for="lyricist26">'."Lyricist".'</label>'.
  '<input id="both27" name="option28" type="radio" value="Both" '.$check3.'/>'.
  '<label for="both27">Both</label>'.


  '</p>'.


                '<p class="btn-choose-file btn-add-contributor5 color-a">'.
      '<span id="btnchange-color" class="btn btn-upload-audio btn-upload-audio2 btn-add-contributor2 btn-edit-contributor">'."SAVE".'</span>'.
      '<a onclick="document.getElementById(\'idEditContributor\').style.display=\'none\'">'."Cancel".'</a>'.
                '</p>'.'</form>'.
                   '</div>';
                echo "<script>";
                echo "if ($('input.which_pro1').is(':checked')){jQuery('.form-group300').css('display', 'block');} ";
                echo "if ($('input.mechanical_society1').is(':checked')){jQuery('.form-group4').css('display', 'block');} ";
                echo "</script>";


                echo "<script>";
                echo "$( 'input.which_pro1' ).change(function() {
                    if($('input.which_pro1').is(':checked')){
                        $( 'input.which_pro1' ).attr('value', 1);
                    }else{
                        $( 'input.which_pro1' ).attr('value', 0);
                    }
                    });";
                echo "$( 'input.mechanical_society1' ).change(function() {
                    if($('input.mechanical_society1').is(':checked')){
                        $( 'input.mechanical_society1' ).attr('value', 1);
                    }else{
                        $( 'input.mechanical_society1' ).attr('value', 0);
                    }
                    });";

                echo "</script>";

                echo "<script>";
                echo "$( '.form-sel1' ).change(function() {
                    var optsel4 = $( '.form-sel1 option:selected' ).text();
                       $('.sel_val1').val(optsel4);
                    });";
                echo "$( '.form-sel2' ).change(function() {
                    var optsel5 = $( '.form-sel2 option:selected' ).text();
                       $('.sel_val2').val(optsel5);
                    });";

                echo "</script>";

                //dd($val);
                break;

            }
          }

        }
    }

    public function postSaveEditContributorCopySong(Request $request) {
        $contributor_id = $request->id_contributor;
        $first_name = $request->txtFirstName;
        $last_name = $request->txtLastName;
        //$role = $request->option28;
        if ($request->option28 == "Both") {
            $role = "Composer + Lyricist";
        } else {
            $role = $request->option28;
        }
        //$member_pro = $request->check;
        if($request->check == 1) {
            $member_pro = 1;
            $which_pro_option = $request->sel_val1;
            $cae_number3 = $request->cae_number3;
        }else{
            $member_pro = 0;
            $which_pro_option = "";
            $cae_number3 = "";
        }
        if($request->check_society2 == 1) {
            $check_society2 = 1;
            $society_option = $request->sel_val2;
            $cae_number4 = $request->cae_number4;
        }else{
            $check_society2 = 0;
            $society_option = "";
            $cae_number4 = "";
        }

        //$check_society2 = $request->check_society2;


        //dd($contributor_id);
        $update_contributor = Session()->get('comtributors');
//        die($contributor_id);
        if(isset($update_contributor[0][$contributor_id])){
            $update_contributor[0][$contributor_id][0] = $first_name;
            $update_contributor[0][$contributor_id][1] = $last_name;
            $update_contributor[0][$contributor_id][2] = $role;
            $update_contributor[0][$contributor_id][3] = $member_pro;
            $update_contributor[0][$contributor_id][4] = $which_pro_option;
            $update_contributor[0][$contributor_id][5] = $cae_number3;
            $update_contributor[0][$contributor_id][6] = $check_society2;
            $update_contributor[0][$contributor_id][7] = $society_option;
            $update_contributor[0][$contributor_id][8] = $cae_number4;
//            var_dump($update_contributor);
//            die();
            Session()->put('comtributors', $update_contributor);
            Session()->save();
            //$val =  $arr_str;
            //$key = max(array_keys(Session()->get('comtributors')));
            echo '<span id="'.$first_name."_".$last_name.'" hidden>'.'</span>'.
                '<span class="contributors-name">'.$first_name." ".$last_name.'</span>'.'<input type="hidden" name="contributors_firstname[]" value="'.$first_name.'">'.
                '<input type="hidden" name="contributors_lastname[]" value="'.$last_name.'">'.
            '<input type="hidden" name="contributors_role[]" value="'.$role.'">'.
            '<input type="hidden" name="contributors_memberpro[]" value="'.$member_pro.'">'.
            '<input type="hidden" name="contributors_promember[]" value="'.$which_pro_option.'">'.
            '<input type="hidden" name="contributors_cae_number[]" value="'.$cae_number3.'">'.
            '<input type="hidden" name="contributors_member_society[]" value="'.$check_society2.'">'.
            '<input type="hidden" name="contributors_mech_society_member[]" value="'.$society_option.'">'.
            '<input type="hidden" name="contributors_mech_society_number[]" value="'.$cae_number4.'">'.
            '<input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal">'.
                '<span class="role">'.$role.'</span>'.
                '<span class="ratio-percent" hidden="">'.'</span>'.
            '<input type="hidden" class="ratio-percent-val">'.
            '<input type="text" name="contributors_equal2[]" class="percent-share2" placeholder="0" value="" style="
       ">'.'<span class="percent-icon1">'."%".'</span>'.
                '<i class="fa fa-pencil" data-name2="'.$contributor_id.'">'.'</i>'.
                '<i class="fa fa-remove fa-remove2" data-name="'.$contributor_id.'" style="font-size:24px; color: red">'.'</i>'.    '<div class="border">'.'</div>';
            //dd(Session()->get('comtributors55'));
        }
        }
    public function getEditContributorSong($name2,$idsong) {
        $contributors = Contributors::where('id_user',Auth::id())->orderBy('id', 'desc')->get();
        $data_song = session('song_info');
        $data = $data_song[$idsong][0][12];
        $data_comtributor = session('comtributors');
        if(!empty($data_comtributor)){
            $data = $data_comtributor;
        }
        foreach ($data as $key => $val) {

            if($key == $name2){
                $check1 = "";
                $check2 = "";
                $check3 = "";
                if($val[2] == "Composer"){
                    $check1 = "checked";
                }
                if($val[2] == "Lyricist"){
                    $check2 = "checked";
                }
                if($val[2] == "Composer + Lyricist"){
                    $check3 = "checked";
                }
                if($val[3] == 1){
                    $check_pro = "checked";
                    $check_pro2 = $val[4];
                    $cae_number2 = $val[5];
                }else{
                    $check_pro = "";
                    $check_pro2 = "";
                    $cae_number2 = "";
                }
                if($val[6] == 1){
                    $check_society = "checked";
                    $which_society2 = $val[7];
                    $cae_society = $val[8];

                }else{
                    $check_society = "";
                    $which_society2 = "";
                    $cae_society = "";
                }

                echo '<div class="modal-content  modal-add-contributor" data-id="'.$name2.'">'.'<p class="title-home-warning title-upload-song">'.
                    "EDIT CONTRIBUTOR".
                    '</p>'.'<p class="btn-choose-file contributor-radio1">'.

                    '<input id="new22" name="option24" type="radio" value="New" onclick="edit_contributorFunction2()" checked="" />'
                    .'<label for="new22" class="add-new-contributor">'."New".'</label>'.
                    '<input id="new23" name="option24" type="radio" value="Existing" onclick="edit_interfaceExistingFunction()" />'.
                    '<label for="new23" class="add-exist-contributor">'."Existing".'</label>'.

                    '</p>'.'<div class="form-group form-group60" hidden>'.
                    '<select class="form-control form-control-pro show-member exis-contributors" id="sel1">';
                if(count($contributors) > 0){
                    echo ' <option value="Please choose">Please choose</option>';
                    foreach ($contributors as $key => $data){
                        echo ' <option data-first_name="'.$data->first_name.'" data-last_name="'.$data->last_name.'" data-role="'.$data->role.'" data-share="'.$data->share.'" data-pro_member_check="'.$data->pro_member_check.'" data-which_pro="'.$data->which_pro.'" data-cae_number="'.$data->cae_number.'" data-member_society_check="'.$data->member_society_check.'" data-mech_society_member="'.$data->mech_society_member.'" data-mech_society_number="'.$data->mech_society_number.'" value="'.$key.'">'.$data->first_name.' '.$data->last_name.'</option>';
                    }
                }else{
                    echo ' <option value="Please choose">No contributors </option>';
                }

                    echo '</select>'.
                    '</div>'
                    .'<div class="member_contributor" hidden>'.
                    '<p>'.'<span class="member_left1 color-member-left">'."MEMBER of a PRO:".'</span>'.'<span class="member_right">'."Yes, Buma".'</span>'.'</p>'.
                    '<p>'.'<span class="member_left2 color-member-left">'."CAE Number:".'</span>'.'<span class="member_right">'."189904BM".'</span>'.'</p>'.
                    '<p>'.'<span class="member_left3 color-member-left">'."MEMBER of a mech, society:".'</span>'.'<span class="member_right">'."Yes, Stemra".'</span>'.'</p>'.
                    '<p>'.'<span class="member_left4 color-member-left">'."CAE Number:".'</span>'.'<span class="member_right">'."2884242RP".'</span>'.'</p>'.
                    '</div>'.'<form id="userform11" method="POST">'.
                    '<input type="hidden" name="_token" value="'.csrf_token().'">'.
                    '<input type="hidden" name="id_contributor" value="'.$name2.'">'.
                    '<p class="text-upload-audio">'.
                        '<div class="select-option-form"> <div class="form-group form-group50 edit_hide">'.
                    '<label class="label-title">'."First name".'<img src="source/image/content/question.jpg">'.'</label>'.

                    '<input type="text" name="txtFirstName" value="'.$val[0].'" class="form-control form-title form-first-name" placeholder="First name">'.

                    '</div>'.'</p>'.
                    '<p class="text-upload-audio">'.
                    '<div class="form-group form-group50 edit_hide">'.
                    '<label class="label-title">'."Last name".'<img src="source/image/content/question.jpg">'.'</label>'.
                    '<input type="text" name="txtLastName" class="form-control form-title form-last-name" value="'.$val[1].'" placeholder="Last name">'.
                    '</div>'.
                    '</p>'.
                    '<div class="select-option-form"><p class="check-agreement check-agreement2  form-group50 edit_hide">'.'<span class="squaredTwo">'.
                    '<input type="checkbox" value="'.$val[3].'" id="squaredSix33" class="which_pro1 member_pro" name="check" onclick="shsel8Function()" '.$check_pro.' >'.
                    '<label for="squaredSix33">'.'</label>'.'</span>'.'<span class="color-text-checkbox color-text-checkbox2">'." Member of a PRO".'<img src="source/image/content/question.jpg">'.'</span>'.
                    '</p>'.'<div class="form-group form-group3" hidden>'.

                    '<label class="label-title label-title-2">'."Which PRO". '<img src="source/image/content/question.jpg">'.'</label>'.
                    '<select class="form-control form-control-pro form-sel1 member_pro_which_pro" id="sel1" name="which_pro_option">'.
                    '<option>'."Please choose".'</option>';
                if($check_pro2 == 'BUMA'){
                    echo    '<option selected>'."BUMA".'</option>'.'<option>'."Stemra".'</option>';
                }else{
                    echo    '<option>'."BUMA".'</option>'.'<option selected>'."Stemra".'</option>';
                }
                echo '</select>'.
                    '<input type="hidden" name="sel_val1" class="sel_val1" value="'.$check_pro2.'">'.
                    '</div>'.
                    '<div class="form-group form-group3" hidden>'.
                    '<label class="label-title">'."CAE number".'<img src="source/image/content/question.jpg">'.'</label>'.

                    '<input type="text" name="cae_number3" class="form-control form-title  member_pro_number" placeholder="Title" value="'.$cae_number2.'">'.

                    '</div>'.'<p class="check-agreement check-agreement2 form-group5 edit_hide">'.'<span class="squaredTwo">'.
                    '<input type="checkbox" value="'.$val[6].'" id="squaredSeven44" class="mechanical_society1 member_society" onclick="shsel7Function()" name="check_society2" '.$check_society.'>'.
                    '<label for="squaredSeven44">'.'</label>'.
                    '</span>'.'<span class="color-text-checkbox">'." Member of a mechanical society ".'<img src="source/image/content/question.jpg">'.'</span>'.
                    '</p>'.
                    '<div class="form-group form-group4" hidden>'.

                    '<label class="label-title label-title-2">'."Which society". '<img src="source/image/content/question.jpg">'.'</label>'.
                    '<select class="form-control form-control-pro form-sel2 member_society_which_pro" name="society_option" id="sel1">'.
                    '<option>'."Please choose".'</option>';
                if($which_society2 == 'BUMA'){
                    echo '<option selected>'."BUMA".'</option>'.'<option>'."Stemra".'</option>';
                }else{
                    echo '<option>'."BUMA".'</option>'.'<option selected>'."Stemra".'</option>';
                }
                echo  '</select>'.'<input type="hidden" name="sel_val2" class="sel_val2" value="'.$which_society2.'">'.
                    '</div>'.
                    '<div class="form-group form-group4" hidden>'.
                    '<label class="label-title">CAE number<img src="source/image/content/question.jpg">'.'</label>'.
                    '<input type="text" name="cae_number4" class="form-control form-title member_society_number" placeholder="Title" value="'.$cae_society.'">'.
                    '</div></div></div>'.
                    '<div class="form-group">'.
                    '<label class="label-title label-title-role">'."Role".'<img src="source/image/content/question.jpg">'.'</label>'.
                    '</div>'.
                    '<p class="btn-choose-file contributor-radio1">'.
                    '<input id="composer25" name="option2" type="radio" value="Composer" '.$check1.'/>'.
                    '<label for="composer25">'."Composer".'</label>'.
                    '<input id="lyricist26" name="option2" type="radio" value="Lyricist" '.$check2.'/>'.
                    '<label for="lyricist26">'."Lyricist".'</label>'.
                    '<input id="both27" name="option2" type="radio" value="Both" '.$check3.'/>'.
                    '<label for="both27">Both</label>'.
                    '</p>'.
                    '<p class="btn-choose-file btn-add-contributor5 color-a">'.
                    '<span id="btnchange-color" class="btn btn-upload-audio btn-upload-audio2 btn-add-contributor2 btn-edit-contributor">'."SAVE".'</span>'.
                    '<a onclick="document.getElementById(\'idEditContributor\').style.display=\'none\'">'."Cancel".'</a>'.
                    '</p>'.'</form>'.
                    '</div>';
                echo "<script>";
                echo "if ($('input.which_pro1').is(':checked')){jQuery('.form-group3').css('display', 'block');} ";
                echo "if ($('input.mechanical_society1').is(':checked')){jQuery('.form-group4').css('display', 'block');} ";
                echo "</script>";
                echo "<script>";
                echo "$( 'input.which_pro1' ).change(function() {
                    if($('input.which_pro1').is(':checked')){
                        $( 'input.which_pro1' ).attr('value', 1);
                    }else{
                        $( 'input.which_pro1' ).attr('value', 0);
                    }
                    });";
                echo "$( 'input.mechanical_society1' ).change(function() {
                    if($('input.mechanical_society1').is(':checked')){
                        $( 'input.mechanical_society1' ).attr('value', 1);
                    }else{
                        $( 'input.mechanical_society1' ).attr('value', 0);
                    }
                    });";
                echo "</script>";
                echo "<script>";
                echo "$( '.form-sel1' ).change(function() {
                    var optsel4 = $( '.form-sel1 option:selected' ).text();
                       $('.sel_val1').val(optsel4);
                    });";
                echo "$( '.form-sel2' ).change(function() {
                    var optsel5 = $( '.form-sel2 option:selected' ).text();
                       $('.sel_val2').val(optsel5);
                    });";
                echo "</script>";
                break;

            }

        }
    }
    public function postEditSongContributor(Request $request,$id) {

        $contributor_id = $request->id_contributor;
        $first_name = $request->txtFirstName;
        $last_name = $request->txtLastName;
        if ($request->option2 == "Both") {
            $role = "Composer + Lyricist";
        } else {
            $role = $request->option2;
        }
        if($request->check == 1) {
            $member_pro = 1;
            $which_pro_option = $request->sel_val1;
            $cae_number3 = $request->cae_number3;
        }else{
            $member_pro = 0;
            $which_pro_option = "";
            $cae_number3 = "";
        }
        if($request->check_society2 == 1) {
            $check_society2 = 1;
            $society_option = $request->sel_val2;
            $cae_number4 = $request->cae_number4;
        }else{
            $check_society2 = 0;
            $society_option = "";
            $cae_number4 = "";
        }
        $update_contributor = Session()->get('comtributors');
        $data_song = session("song_info");
        $data = $data_song[$id][0][12];
        $data_comtributor = session('comtributors');
        if(!empty($data_comtributor)){
            $data = $data_comtributor;
        }
        if($data[$contributor_id]){
            $data[$contributor_id][0] = $first_name;
            $data[$contributor_id][1] = $last_name;
            $data[$contributor_id][2] = $role;
            $data[$contributor_id][3] = $member_pro;
            $data[$contributor_id][4] = $which_pro_option;
            $data[$contributor_id][5] = $cae_number3;
            $data[$contributor_id][6] = $check_society2;
            $data[$contributor_id][7] = $society_option;
            $data[$contributor_id][8] = $cae_number4;
            $data[$contributor_id][9] = $data[$contributor_id][9];


            echo '<span id="'.$first_name."_".$last_name.'" hidden>'.'</span>'.
                '<span class="contributors-name">'.$first_name." ".$last_name.'</span>'.'<input type="hidden" name="contributors_firstname[]" value="'.$first_name.'">'.
                '<input type="hidden" name="contributors_lastname[]" value="'.$last_name.'">'.
                '<input type="hidden" name="contributors_role[]" value="'.$role.'">'.
                '<input type="hidden" name="contributors_memberpro[]" value="'.$member_pro.'">'.
                '<input type="hidden" name="contributors_promember[]" value="'.$which_pro_option.'">'.
                '<input type="hidden" name="contributors_cae_number[]" value="'.$cae_number3.'">'.
                '<input type="hidden" name="contributors_member_society[]" value="'.$check_society2.'">'.
                '<input type="hidden" name="contributors_mech_society_member[]" value="'.$society_option.'">'.
                '<input type="hidden" name="contributors_mech_society_number[]" value="'.$cae_number4.'">'.
                '<input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal">'.
                '<span class="role">'.$role.'</span>';
            if($request->check_share == 1){
                echo '<span class="ratio-percent">'.$data[$contributor_id][9].'%</span>';
                echo'<input type="text" name="contributors_equal2[]" class="percent-share2" placeholder="0"  value=""   style="display:none">'.'<span class="percent-icon1" style="display:none">'."%".'</span>';
            }else{
                echo '<span class="ratio-percent" hidden="">'.$data[$contributor_id][9].'%</span>';
                echo'<input type="text" name="contributors_equal2[]" class="percent-share2" placeholder="0"  value="">'.'<span class="percent-icon1">'."%".'</span>';
            }
            echo '<input type="hidden" class="ratio-percent-val">';
                echo '<i class="fa fa-pencil" data-name2="'.$contributor_id.'">'.'</i>'.
                '<i class="fa fa-remove fa-remove2" data-name="'.$contributor_id.'" style="font-size:24px; color: red">'.'</i>'.    '<div class="border">'.'</div>';
        }
        session()->put("comtributors", $data);
    }
}

