<?php

namespace App\Http\Controllers;
use App\BroadcastClaim;
use Illuminate\Http\Request;
use Auth;
use App\Songs;

class ClaimsController extends Controller{
    function postSaveSessionClaims(Request $req){

        $data = new  BroadcastClaim;
        $data->song_title = $req->song;
        $data->station_channel = $req->station_channel;
        $data->program = $req->program;
        $data->air_date = date('Y-m-d', strtotime($req->air_date));
        $data->country = $req->country;
        $data->evidence =$req->evidence;
        $data->id_song =$req->id_song;
        $data->id_user = Auth::id();
        if($req->radio_tv == "TV"){
            $data->radio_tv = 0;
        }else{
            $data->radio_tv = 1;
        }
        $file_upload = $req->file('claim-upload')[0];
        if(isset($file_upload)){
            $filename = $req->file('claim-upload')[0]->getClientOriginalName();
            $url_upload = str_random(4) . "_" . $filename;
            $file_upload->move("source/claims", $url_upload);
            $data->file = $filename;
            $data->file_url =$url_upload;
        }
        if($req->check_publish == 1){
            $data->publish = 1;
        }else{
            $data->publish = 0;
        }
        $data->save();
        return redirect('./');
    }
    function getListClaims(){
        $claims = BroadcastClaim::all();
        return view('admin.claim.list',['claims'=>$claims]);
    }
    function getDelete($id){
        $claim = BroadcastClaim::find($id);
        $claim->delete();
        return redirect('admin/songs/list-claims')->with('thongbao', 'Deleted Successfully!');
    }
    function getSubmitClaims($id){
        $claim = BroadcastClaim::find($id);
        $claim->publish = 1;
        $claim->save();
        return redirect('./');
    }
    function getEditClaims($id){
        $songs = Songs::where('id_user',Auth::id())->orderBy('created_at', 'desc')->get();
        $claim = BroadcastClaim::find($id);
        return view('page.edit_claim',['songs'=>$songs,'claims'=>$claim]);
    }
    function postEditClaims(Request $req,$id){
        $data = BroadcastClaim::find($id);
        $data->song_title = $req->song;
        $data->station_channel = $req->station_channel;
        $data->program = $req->program;
        $data->air_date = date('Y-m-d', strtotime($req->air_date));
        $data->country = $req->country;
        $data->evidence =$req->evidence;
        $data->id_song =$req->id_song;
        $data->id_user = Auth::id();
        if($req->radio_tv == "TV"){
            $data->radio_tv = 0;
        }else{
            $data->radio_tv = 1;
        }
        $file_upload = $req->file('claim-upload')[0];
        if(isset($file_upload)){
            $filename = $req->file('claim-upload')[0]->getClientOriginalName();
            $url_upload = str_random(4) . "_" . $filename;
            $file_upload->move("source/claims", $url_upload);
            $data->file = $filename;
            $data->file_url =$url_upload;
            unlink('source/claims/'.$req->url_file);
        }
        if($req->check_publish == 1){
            $data->publish = 1;
        }else{
            $data->publish = 0;
        }
        $data->save();
        return redirect('./');
    }
    function getDeleteClaim($id){
        $claim = BroadcastClaim::find($id);
        if($claim->file_url != ''){
            unlink('source/claims/'.$claim->file_url);
        }
        $claim->delete();
        return redirect('./');
    }
    function getInfoClaim($id){
        $claim = BroadcastClaim::find($id);
        if($claim->	radio_tv == 1){
            $radio = 'Radio';
        }else{
            $radio = 'TV';
        }
        $position = strpos($claim->file,".");
        $name = substr($claim->file,0,$position);
        if(strlen($name) > 25){
              $name = substr($claim->file,0,25).'..'.strtoupper(substr($claim->file,$position));
        }else{
              $name = substr($claim->file,0,$position).'..'.strtoupper(substr($claim->file,$position));
        }
        $string  = '<div id="claims-info" class="modal" >';
        $string .= '<div class="modal-content modal-content-publish-song modal-saved-claims modal-submit-claim">';
        $string .='<p class="title"> broadcast claim </p>';
        $string .= '<div class="claim-date data-content"><p class="data-title">Submit date:</p><span class="data-date">'.date('d-m-Y',strtotime($claim->updated_at)).'</span></div>';
        $string .= '<div class="claim-song data-content"><p class="data-title">Song:</p><span class="data-song">'.$claim->song_title.'</span></div>';
        $string .='<div class="claim-medium data-content"><p class="data-title ">Medium:</p><span class="data-medium">'.$radio.'</span></div>';
        $string .='<div class="claim-station data-content"><p class="data-title">Station:</p><span class="data-station">'.$claim->station_channel.'</span></div>';
        $string .='<div class="claim-program data-content"><p class="data-title">Program:</p><span class="data-program">'.$claim->program.'</span></div>';
        $string .='<div class="claim-air-date data-content"><p class="data-title ">Air date:</p><span class="data-air-date">'.date('d-m-Y',strtotime($claim->air_date)).'</span></div>';
        $string .='<div class="claim-country data-content"><p class="data-title">Country:</p><span class="data-country">'.$claim->country.'</span></div>';
        $string .= '<div class="claim-evidence data-content"><p class="data-title">Evidence:</p><span class="data-evidence">'.$claim->evidence.'</span></div>';
        $string .='<div class="claim-file data-content"><p class="data-title">File:</p><span class="data-content">'.$name.'</span></div>';
        $string .='<p class="btn-close"><button   class="btn btn-cancel remove-claims-info">CLOSE</button></p></div></div>';
        echo $string;
    }
}
?>