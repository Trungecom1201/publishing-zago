<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\User;
use Hash;
use Auth;
use App\Songs;
use App\Contributors;
use App\BroadcastClaim;
use Carbon\Carbon;

class PageController extends Controller
{
    public function getHomePage(){
        $now = Carbon::now();
        $new_song = Songs::where('id_user', Auth::id())->orderBy('id', 'desc')->first();
        $new_claim = BroadcastClaim::where('id_user', Auth::id())->orderBy('id', 'desc')->first();
        if($new_song != null) {
            $new_song1 = Songs::where('id_user', Auth::id())->orderBy('id', 'desc')->get();
            $count_song = count($new_song1);
            $created_song = new Carbon($new_song->created_at);
            $songs_time = ($created_song->diff($now)->days < 1)
                ? 'today'
                : $created_song->diffForHumans(null,$now).' ago';
        }
        else{
            $count_song = null;
            $songs_time = null;
        }
         if($new_claim != null) {
             $claim_data = BroadcastClaim::where('id_user', Auth::id())->Where('publish', '=', '1')->orderBy('updated_at', 'desc')->get();
             $claim_publish = BroadcastClaim::where('id_user', Auth::id())->orderBy('id', 'updated_at')->Where('publish', '=', '0')->get();
             $count_claim = count($claim_data);
             $created = new Carbon($new_claim->updated_at);
             $claim_time = ($created->diff($now)->days < 1)
                 ? 'today'
                 : $created->diffForHumans(null,$now).' ago';
         }
         else{
             $count_claim = null;
             $claim_time = null;
             $claim_publish = null;
         }


    return view('page.home_saved_song',compact('new_song','count_song','new_claim','count_claim','claim_time','songs_time','claim_publish'));
	}

	public function getAgreement(){	
		return view('page.agreement');
	}
	public function getAddSong(Request $request){
		$request->Session()->forget('comtributors');
		$contributor = Contributors::where('id_user',Auth::id())->orderBy('id', 'desc')->get();
		return view('page.add_song',['contributors'=>$contributor]);
	}
	public function getPublishYourSong(){

		return view('page.publish_your_song');
	}
	public function getSavedSong() {

    if(session('info') != ''){
      Session()->push('song_info', session('info'));
      Session()->forget('info');
      Session()->forget('comtributors');
      session()->forget('filesong1');
      session()->forget('filesong2');
      session()->forget('filename1');
      session()->forget('filename2');
    }
//        $now = Carbon::now();
//        $new_song = Songs::where('id_user', Auth::id())->orderBy('id', 'desc')->first();
//        $new_claim = BroadcastClaim::where('id_user', Auth::id())->orderBy('id', 'desc')->first();
//        if($new_song != null) {
//            $new_song1 = Songs::where('id_user', Auth::id())->orderBy('id', 'desc')->get();
//            $count_song = count($new_song1);
//            $created_song = new Carbon($new_song->created_at);
//            $songs_time = ($created_song->diff($now)->days < 1)
//                ? 'today'
//                : $created_song->diffForHumans(null,$now).' ago';
//        }
//        else{
//            $count_song = null;
//            $songs_time = null;
//        }
//        if($new_claim != null) {
//            $claim_data = BroadcastClaim::where('id_user', Auth::id())->orderBy('id', 'desc')->get();
//            $count_claim = count($claim_data);
//            $created = new Carbon($new_claim->created_at);
//            $claim_time = ($created->diff($now)->days < 1)
//                ? 'today'
//                : $created->diffForHumans(null,$now).' ago';
//        }
//        else{
//            $count_claim = null;
//            $claim_time = null;
//        }

        return redirect('./');
//        return view('page.home_saved_song',compact('new_song','count_song','new_claim','count_claim','claim_time','songs_time'));
	}
	public function getPublishedSong() {
		return view('page.home_published_song');	
	}
	public function getListSongs() {
        $songs = Songs::where('id_user', Auth::id())->orderBy('id', 'desc')->get();
		return view('page.list_songs',compact('songs'));
	}
	public function getSavedClaims() {
		return view('page.saved_claims');	
	}
	public function getFinishedPublishSongs() {
		return view('page.finished_publish_songs');	
	}
    public function getCopySong() {
        return view('page.copy_song');
    }
	public function getCopySong2() {
		return view('page.copy_song2');
	}
    public function getClaims() {
        $claim_submit = BroadcastClaim::where('id_user', Auth::id())->orderBy('updated_at', 'desc')->Where('publish', '=', '1')->get();
        $claim = BroadcastClaim::where('id_user', Auth::id())->orderBy('updated_at', 'desc')->Where('publish', '=', '0')->get();
        return view('page.claims',['claim_submit'=>$claim_submit,'claim'=>$claim]);
    }
	public function getBroadcastClaims() {
		return view('page.broadcast_claims');	
	}
	public function getAbCd() {
		return view('page.abcd');	
	}
	public function getPoIu() {
		return view('page.PoIu');	
	}
	public function getAddClaimEmpty() {
        $songs = Songs::where('id_user',Auth::id())->orderBy('created_at', 'desc')->get();
		return view('page.add_claim_empty',compact('songs'));	
	}
	public function getEditClaim() {
		return view('page.edit_claim');	
	}
	public function getLogin() {
		return view('login');	
	}
	public function postLogin(Request $request) {

    	 $this->validate($request,
           [
              'email' => 'required',
              'password' => 'required|min:3|max:32'
           ],
           [
               'email.required' => 'You have not entered an email',
               'password.required' => 'You have not entered a password',
               'password.min' => 'Too short, must be 3 characters or more',
               'password.max' => 'Too long, up to 32 characters'
           ]);
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
        {
          return redirect('/');
        }
        else
        {
          return redirect('login')->with('thongbao','Login unsuccessful!');
        }
    }
    public function getLogout(Request $request)
    {
      session()->flush();
 	 return redirect('/login');
    } 
    public function getRegister()
    {
 	 return view('register');
    } 
     public function postRegister(Request $request)
    {

 	  $this->validate($request,
           [
              'txtFirstName' => 'required|min:3|max:20',
               'txtLastName' => 'required|min:3|max:20',
              'txtEmail' => 'required|min:6|max:100|email|unique:users,email',
               'txtPhone' => 'required|min:6|max:20',
              'txtPass' => 'required|min:3|max:32',
              'txtRePass' => 'required|min:3|max:32|same:txtPass',
               'txtAddress' => 'required|min:3|max:20',

           ],
           [
               'txtFirstName.required' => 'You have not entered first name',
               'txtFistName.min' => 'Too short, must be 3 characters or more',
               'txtFistName.max' => 'Too long, up to 30 characters',
               'txtEmail.required' => 'You have not entered email',
               'txtEmail.min' => 'Too short, must be 6 characters or more',
               'txtEmail.max' => 'Too long, up to 100 characters',
               'txtEmail.email' => 'Enter the wrong email format',
               'txtEmail.unique' => 'This email name already exists',
               'txtLastName.required' => 'You have not entered last name',
               'txtPhone.required' => 'You have not entered phone',
               'txtAddress.required' => 'You have not entered address',
               'Image.required' => 'You have not entered image',
               'txtPass.required' => 'You have not entered password',
               'txtPass.min' => 'Too short, must be 3 characters or more',
               'txtPass.max' => 'Too long, up to 32 characters',
               'txtRePass.required' => 'You have not entered re-password',
               'txtRePass.min' => 'Too short, must be 3 characters or more',
               'txtRePass.max' => 'Too long, up to 32 characters',
               'txtRePass.same' => 'Passwords are not the same'
           ]); 
		 	  	$user = new User;
		        $user->first_name = $request->txtFirstName;
		        $user->last_name = $request->txtLastName;
		        $user->email = $request->txtEmail;
		        $user->phone = $request->txtPhone;
		        $user->password = bcrypt($request->txtPass);
		        $user->type_user = 0;
		        $user->address = $request->txtAddress;
		        $user->save();
        		return redirect('login')->with('notification','Success!');
    }
    public function getClearSession(){
        Session()->forget('comtributors');
        Session()->forget('info');
        if(Session('filesong1')){
            unlink("source/music/".session('filesong1'));
            Session()->forget('filesong1');
        }
        if(Session('filename1')){
            Session()->forget('filename1');
        }
        if(Session('filesong2')){
            unlink("source/music/".session('filesong2'));
            Session()->forget('filesong2');
        }
        if(Session('filename2')){
            Session()->forget('filename2');
        }
    }
    function getClearContributor(){
        Session()->forget('comtributors');
    }
}
