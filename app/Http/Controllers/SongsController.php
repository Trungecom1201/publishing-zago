<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Requests;
use App\Songs;
use App\Contributors;

class SongsController extends Controller
{
    public function postAdd(Request $request)
    {
        $name_song = $request->txtNameSong;
        $minute = $request->txtMinute;
        $second = $request->txtSecond;
        if ($minute == '') {
            $time = $second . " sec ";
        } elseif ($second == '') {
            $time = $minute . " min ";
        } else {
            $time = $minute . " min " . $second . " sec";
        }
        if ($request->check_contains_sample == 1) {
            $contains_sample = 1;
            $rights_to_use_samples = $request->check_right_sample;
        } else {
            $contains_sample = 0;
            $rights_to_use_samples = 0;
        }
        if ($request->check_remix == 1) {
            $remix = 1;
        } else {
            $remix = 0;
        }
        if ($request->check_prev_registered == 1) {
            $prev_registered = 1;
            $which_pro = $request->sel_songs;
            $prs_tunecode = $request->PRS_Tunecode;
        } else {
            $prev_registered = 0;
            $which_pro = null;
            $prs_tunecode = null;
        }
        
        if ($request->file_music1 != null) {
            $song1 = $request->file_music1;
            $name_random1 = $request->name_random1;
        } else {
            $song1 = null;
            $name_random1 = null;
        }
        if ($request->file_music2 != null) {
            $song2 = $request->file_music2;
            $name_random2 = $request->name_random2;
        } else {
            $song2 = null;
            $name_random2 = null;
        }
        if ($request->contributors_firstname != null) {
            $contributorsfirstname = $request->contributors_firstname;
        } else {
            $contributorsfirstname = null;
        }
        if ($request->contributors_lastname != null) {
            $contributorslastname = $request->contributors_lastname;
        } else {
            $contributorslastname = null;
        }
        
        if ($request->contributors_promember != null) {
            $contributors_promember = $request->contributors_promember;
        } else {
            $contributors_promember = null;
        }
        if ($request->contributors_cae_number != null) {
            $contributors_cae_number = $request->contributors_cae_number;
        } else {
            $contributors_cae_number = null;
        }

        if ($request->contributors_mech_society_member != null) {
            $contributors_mech_society_member = $request->contributors_mech_society_member;
        } else {
            $contributors_mech_society_member = null;
        }
        if ($request->contributors_mech_society_number != null) {
            $contributors_mech_society_number = $request->contributors_mech_society_number;
        } else {
            $contributors_mech_society_number = null;
        }
        $contributorsrole = $request->contributors_role;
        $contributors_memberpro = $request->contributors_memberpro;
        $contributors_member_society = $request->contributors_member_society;

        if ($request->contributors_equal[0] != 0) {
            $contributors_equal = $request->contributors_equal;
            $contributors_equal_ext = 1;
        } else {
            $contributors_equal = $request->contributors_equal2;
            $contributors_equal_ext = 0;
        }

        if($contributorsfirstname != null) {
            $count = count($contributorsfirstname);
            $contributors = [$contributorsfirstname, $contributorslastname, $contributorsrole, $contributors_memberpro, $contributors_promember, $contributors_cae_number, $contributors_member_society, $contributors_mech_society_member, $contributors_mech_society_number, $contributors_equal];
            $contributors2 = [];
            foreach ($contributors as $key => $value) {
                for ($i = 0; $i < $count; $i++) {
                    $contributors2[$i][] = $value[$i];
                }
            }
        }else{
            $contributors2 = null;
        }
        $arr_str = [$name_song, $time, $contains_sample, $rights_to_use_samples, $remix, $prev_registered, $which_pro, $prs_tunecode, $song1, $name_random1, $song2, $name_random2, $contributors2, $contributors_equal_ext, $minute, $second];
        if(session('info') == ''){
            Session()->push('info',$arr_str);
        }else{
           Session()->forget('info');
           Session()->push('info',$arr_str);
        }
        return redirect('publish-your-song');
    }

    public function postAdd2(Request $request)
    {
        $songs = new Songs;
        if ($request->hasFile('file-2')) {
            $file = $request->file('file-2');
            foreach ($file as $filess) {
                $name = $filess->getClientOriginalName();
                $Image = str_random(4) . "_" . $name;
                $filess->move("source/music", $Image);
                $songs->audio_original = $Image;
            }
        } else {
            $songs->audio_original = "";
            echo 'k vao dk';
        }
        $songs->user_first_name = "iiiiiiii";
        $songs->user_last_name = "iiiiiiiimmm";
        $songs->save();
    }

    public function getSua($id)
    {
        $theloai = TheLoai::find($id);
        return view('admin.theloai.sua', ['theloai' => $theloai]);
    }

    public function postSua(Request $request, $id)
    {
        $theloai = TheLoai::find($id);
        $this->validate($request,
            [
                'txtName' => 'required|min:3|max:100|unique:TheLoai,Ten'
            ],
            [
                'txtName.required' => 'Bạn chưa nhập tên thể loại',
                'txtName.unique' => 'Tên thể loại này đã có rồi',
                'txtName.min' => 'Ngắn quá, phải từ 3 ký tự trở lên',
                'txtName.max' => 'Dài quá, tối đa 100 ký tự',
            ]);
        $theloai->Ten = $request->txtName;
        $theloai->TenKhongDau = changeTitle($request->txtName);
        $theloai->save();
        return redirect('admin/theloai/sua/' . $id)->with('thongbao', 'Sửa thành công!');
    }

    public function getDelete($id)
    {
        $songs = Songs::find($id);
        $songs->delete();
        return redirect('admin/songs/list')->with('thongbao', 'Deleted Successfully!');
    }

    public function getList()
    {
        $songs = Songs::all();
        return view('admin.songs.list', ['songs' => $songs]);
    }

    public function getNameSongs()
    {
        $namesong = Songs::all();
        return view('addsong', ['namesong' => $namesong]);
    }

    public function getSong(Request $request)
    {
        $str_m = ' ';
        if ($request->hasFile('file-2')) {
            $file = $request->file('file-2');
            foreach ($file as $filess) {
                $name = $filess->getClientOriginalName();
                $Image = str_random(4) . "_" . $name;
                $filess->move("source/music", $Image);
                $str_m = $Image;
            }
        }
    }

    public function postSong2(Request $request)
    {
        $filename1 = $request->file('file_song0');
        $filename = $request->file('file_song0')->getClientOriginalName();
        session()->put('filename1', $filename);
        $Image2 = str_random(4) . "_" . $filename;
        $filename1->move("source/music", $Image2);
        session()->put('filesong1', $Image2);
        echo '<label id="fileLabel" name="file_music1" class="name-song inputfile inputfile-1 add-margin-top" data-multiple-caption="{count} files selected" multiple=""> <img class ="icon-music"src="source/image/content/music2.jpg">' . $filename . '</label>' . '<input type="hidden" value="' . $filename . '" name="file_music1">' . '<input type="hidden" value="' . $Image2 . '" name="name_random1">' . '<div class="play_music"><i class="fa fa-play"></i><audio controls="" style="display:none" class="upload-song-1">' .
            '<source src="source/music/' . $Image2 . '" type="audio/mpeg">' .
            '</audio></div>' .
            '<i class="fa fa-remove song333 song1 song-original" style="display: block;"  onclick="originalFunction()" data-name=' . $Image2 . '>' . '</i>' .
            '<img class="listen_music1 song111 song11" src="source/image/content/play1.jpg" style="
        /* float: right; */
        ">' .
            '<i class="fa fa-remove song333 song1">' . '</i>';
    }

    public function postSong3(Request $request)
    {
        $filename1 = $request->file('file_song20');
        $filename = $request->file('file_song20')->getClientOriginalName();
        session()->put('filename2', $filename);
        $Image2 = str_random(4) . "_" . $filename;
        $filename1->move("source/music", $Image2);
        session()->put('filesong2', $Image2);
        echo '<label id="fileLabel" name="file_music2" class="name-song inputfile inputfile-1 add-margin-top" data-multiple-caption="{count} files selected" multiple=""> <img class="icon-music" src="source/image/content/music2.jpg">' . $filename . '</label>' . '<input type="hidden" value="' . $filename . '" name="file_music2">' . '<input type="hidden" value="' . $Image2 . '" name="name_random2">' . '<div class="play_music"><i class="fa fa-play"></i><audio style="display:none" controls="" class="upload-song-2">' .
            '<source src="source/music/' . $Image2 . '" type="audio/mpeg">' .
            '</audio></div>' .
            '<i class="fa fa-remove song333 song1 song-intrumental" style="display: block;" onclick="instrumentalFunction()" data-name=' . $Image2 . '>' . '</i>' .
            '<img class="listen_music1 song111 song11" src="source/image/content/play1.jpg" style="
        /* float: right; */
        ">' .
            '<i class="fa fa-remove song333 song1">' . '</i>';
    }

    public function postDeleteSong(Request $request, $name)
    {
        if ((session('filename1') != null) && (session('filesong1') != null)) {
            $request->session()->forget('filename1');
            $request->session()->forget('filesong1');
            $request->session()->save();
            unlink("source/music/" . $name);
        }
        echo '<label id="fileLabel22" class="name-song-file inputfile inputfile-1 instrumental-1 instrumental-2" data-multiple-caption="{count} files selected" multiple="">' . "No file uploaded" . '</label>' .
            '<div onclick="upload_audioFunction()" class="btn btn-upload-audio5 btn-song2">' . "UPLOAD" . '</div>' .
            '<img class="listen_music1 song111 song22" src="source/image/content/play1.jpg">' .
            '<i class="fa fa-remove song333 song2">' . '</i>';
    }

    public function postDeleteSong2(Request $request, $name)
    {
        if ((session('filename2') != null) && (session('filesong2') != null)) {
            $request->session()->forget('filename2');
            $request->session()->forget('filesong2');
            $request->session()->save();
            unlink("source/music/" . $name);
        }
        echo '<label id="fileLabel22" class="name-song-file inputfile inputfile-1 instrumental-1 instrumental-2" data-multiple-caption="{count} files selected" multiple="">' . "No file uploaded" . '</label>' .
            '<div onclick="upload_audioFunction2()" class="btn btn-upload-audio5 btn-song2">' . "UPLOAD" . '</div>' .
            '<img class="listen_music1 song111 song22" src="source/image/content/play1.jpg">' .
            '<i class="fa fa-remove song333 song2">' . '</i>';
    }

    public function postAddSong2(Request $request)
    {
        $song = new Songs;
        $song->song_title = $request->name_song;
        $song->user_first_name = Auth::user()->first_name;
        $song->user_last_name = Auth::user()->last_name;
        $song->user_email = Auth::user()->email;
        $song->user_phone = Auth::user()->phone;
        $song->duration = $request->duration_song;
        $song->contains_samples = $request->contain_sample;
        $song->rights_to_use_samples = $request->right_to_use_sample;
        $song->remix = $request->remix;
        $song->prev_registered_with_pro = $request->prev_registered;
        $song->which_pro = $request->which_pro;
        $song->prs_tunecode = $request->prs_tunecode;
        $song->audio_original = $request->song1;
        $song->file_url = $request->name_random1;
        $song->audio_instrumental = $request->song2;
        $song->file_url_instrumental = $request->name_random2;
        if($request->equal == 1){
            $song->equal_shares = "Yes";
        }else{
            $song->equal_shares = "No";
        }
        $song->id_user =  Auth::id();
        $song->save();
        $idSong = $song->id;
        $song_id = $request->song_id;
        $song_session = Session()->get('song_info');
        foreach ($song_session as $key => $val) {
            if ($key == $song_id) {
                  foreach($val[0][12] as $key_data => $data){
                      $contributor = new Contributors;
                      $contributor->first_name = $data[0];
                      $contributor->last_name =$data[1];
                      $contributor->role = $data[2];
                      $contributor->pro_member_check = $data[3];
                      $contributor->which_pro = $data[4];
                      $contributor->cae_number = $data[5];
                      $contributor->member_society_check = $data[6];
                      $contributor->mech_society_member = $data[7];
                      $contributor->mech_society_number = $data[8];
                      $contributor->share = $data[9];
                      $contributor->id_song = $idSong;
                      $contributor->id_user = Auth::id();
                      $contributor->save();
                  }
                Session()->pull('song_info.' . $key); // retrieving pen and removing
                break;
            }
        }
        return redirect('/');
    }

    public function getDetails($id)
    {
        $details_song = Songs::all()->find($id);
        $contributor_composer = Contributors::all()->where('id_song', $id);
        return view('page.home_published_song', compact('details_song', 'contributor_composer'));
    }

    public function getEditSongSaved($id, $place_saved)
    {
        $id_song_saved = $id;
        $type_saved = $place_saved;
        $contributors = Contributors::where('id_user',Auth::id())->orderBy('id', 'desc')->get();
        return view('page.edit_song', compact('id_song_saved','type_saved','contributors'));
    }

    public function postDeleteMusicOriginal(Request $request, $id_song_save)
    {
        $delete_song = Session()->get('song_info');
        foreach ($delete_song as $key => $val) {
            if ($key == $id_song_save) {
                unlink("source/music/" . $delete_song[$id_song_save][0][9]);
                $delete_song[$id_song_save][0][8] = null;
                $delete_song[$id_song_save][0][9] = null;
                Session()->put('song_info', $delete_song);
                Session()->save();
                break;
            }
        }
        echo '<label id="fileLabel22" class="inputfile inputfile-1 instrumental-1 instrumental-2" data-multiple-caption="{count} files selected" multiple="">' . "No file uploaded" . '</label>' .
            '<div onclick="upload_audioFunction()" class="btn btn-upload-audio5 btn-song2">' . "UPLOAD" . '</div>' .
            '<img class="listen_music1 song111 song22" src="source/image/content/play1.jpg">' .
            '<i class="fa fa-remove song333 song2">' . '</i>';
    }

    public function postDeleteMusicInstrumental(Request $request, $id_song_save)
    {
        $delete_song = Session()->get('song_info');
        foreach ($delete_song as $key => $val) {
            if ($key == $id_song_save) {
                unlink("source/music/" . $delete_song[$id_song_save][0][11]);
                $delete_song[$id_song_save][0][10] = null;
                $delete_song[$id_song_save][0][11] = null;
                Session()->put('song_info', $delete_song);
                break;
            }
        }
        echo '<label id="fileLabel22" class="inputfile inputfile-1 instrumental-1 instrumental-2" data-multiple-caption="{count} files selected" multiple="">' . "No file uploaded" . '</label>' .
            '<div onclick="upload_audioFunction()" class="btn btn-upload-audio5 btn-song2">' . "UPLOAD" . '</div>' .
            '<img class="listen_music1 song111 song22" src="source/image/content/play1.jpg">' .
            '<i class="fa fa-remove song333 song2">' . '</i>';
    }

    public function getDeleteContributor2($id_song, $id_contributor)
    {
        $total_song = Session()->get('song_info');
        foreach ($total_song as $key => $val) {
            if ($key == $id_song) {
                $contributor = $val[12];
                foreach ($contributor as $key1 => $val1) {
                    if ($key1 == $id_contributor) {
                        unset($contributor[$id_contributor]);
                        $contributor2 = $contributor;
                        $total_song[$id_song][12] = $contributor2;
                        Session()->put('song_info', $total_song);
                        Session()->save();
                        break;
                    }
                }
                break;
            }
        }
    }

    public function postSaveContributor2(Request $request)
    {

        if ($request->option18 == "New") {
            $first_name = $request->txtFirstName;
            $last_name = $request->txtLastName;
            if ($request->option2 == "Both") {
                $role = "Composer + Lyricist";
            } else {
                $role = $request->option2;
            }
            if ($request->MemberPro == 1) {
                $member_pro = 1;
                $pro_member = $request->sel2_val;
                $cae_number = $request->txtCAE_Number;
            } else {
                $member_pro = 0;
                $pro_member = null;
                $cae_number = null;
            }
            if ($request->MemberSociety == 1) {
                $member_society = 1;
                $mech_society_member = $request->sel3_val;
                $mech_society_number = $request->txtSocietyNumber;
            } else {
                $member_society = 0;
                $mech_society_member = null;
                $mech_society_number = null;
            }
            $contributors_equal = null;
        } else {
            $first_name = $request->txtFirstName3;
            $last_name = $request->txtLastName3;
            if ($request->option20 == "Both") {
                $role = "Composer + Lyricist";
            } else {
                $role = $request->option20;
            }
            $member_pro = $request->nbMember_Pro;
            $pro_member = $request->sel2_val3;
            $cae_number = $request->nbCAE_Number3;
            $member_society = $request->nbMember_Society;
            $mech_society_member = $request->txtSociety_Member3;
            $mech_society_number = $request->nbSociety_Number3;
        }

        $arr_str = [$first_name, $last_name, $role, $member_pro, $pro_member, $cae_number, $member_society, $mech_society_member, $mech_society_number, $contributors_equal];

        $id_Song = $request->idSong;
        Session()->push("song_info." . $id_Song . ".12", $arr_str);
        Session()->save();
        $contributor_insert = Session()->get('song_info');        
        foreach ($contributor_insert as $key => $val) {
            if($key == $id_Song){
                foreach ($val[12] as $key1 => $val1) {
    echo '<div class="contributors id-'.$key1.'" >'.'<span id="'.$val1[0]."_".$val1[1].'" hidden>'.'</span>'.
        '<span class="contributors-name">'.$val1[0]." ".$val1[1].'</span>'.
        '<input type="hidden" name="contributors_firstname[]" value="'.$val1[0].'" />'.
        '<input type="hidden" name="contributors_lastname[]" value="'.$val1[1].'"/>'.
        '<input type="hidden" name="contributors_role[]" value="'.$val1[2].'"/>'.
        '<input type="hidden" name="contributors_memberpro[]" value="'.$val1[3].'" />'.
        '<input type="hidden" name="contributors_promember[]" value="'.$val1[4].'"/>'.
        '<input type="hidden" name="contributors_cae_number[]" value="'.$val1[5].'"/>'.
        '<input type="hidden" name="contributors_member_society[]" value="'.$val1[6].'" />'.
        '<input type="hidden" name="contributors_mech_society_member[]" value="'.$val1[7].'"/>'.
        '<input type="hidden" name="contributors_mech_society_number[]" value="'.$val1[8].'"/>'.
        '<input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal"/>'.
        '<span class="role">'.$val1[2].'</span>'.
        '<span class="ratio-percent" hidden>'.'</span>'.
        '<input type="text" name="contributors_equal2[]" class="percent-share2" placeholder="0" style="
       ">'.'<span class="percent-icon1">'."&#37;".'</span>'.
        '<i class="fa fa-pencil" data-idsong="'.$id_Song.'" data-name2="'.$key1.'">'.'</i>'.
        '<i class="fa fa-remove fa-remove2" data-name="'.$key1.'" style="font-size:24px; color: red">'.'</i>'.    '<div class="border">'.'</div>'.
        '</div>';
}
       echo '<div class="half-past-percent">'.'<p class="check-agreement check-contributors">'.'<span class="squaredTwo">'.
  '<input type="checkbox" value="None" id="squaredEight" name="check">'.
  '<label for="squaredEight">'.'</label>'.
'</span>'.'<span class="equal-share equal-share2">'. "Equal share" .'</span>'.'</p>'.'</div>';
}
}

    }

    public function getEditContributor2($idsong, $idcontributor)
    {
        foreach (Session()->get('song_info') as $key => $val) {

            if ($key == $idsong) {
                $contributors = $val[12];
                foreach ($contributors as $key1 => $val1) {
                    if ($key1 == $idcontributor) {
                        $check1 = "";
                        $check2 = "";
                        $check3 = "";
                        if ($val1[2] == "Composer") {
                            $check1 = "checked";
                        }
                        if ($val1[2] == "Lyricist") {
                            $check2 = "checked";
                        }
                        if ($val1[2] == "Composer + Lyricist") {
                            $check3 = "checked";
                        }
                        if ($val1[3] == 1) {
                            $check_pro = "checked";
                            $check_pro2 = $val1[4];
                            $cae_number2 = $val1[5];
                        } else {
                            $check_pro = "";
                            $check_pro2 = "";
                            $cae_number2 = "";
                        }
                        if ($val1[6] == 1) {
                            $check_society = "checked";
                            $which_society2 = $val1[7];
                            $cae_society = $val1[8];

                        } else {
                            $check_society = "";
                            $which_society2 = "";
                            $cae_society = "";
                        }
                        echo '<div class="modal-content  modal-add-contributor" data-id="' . $key1 . '">' . '<p class="title-home-warning title-upload-song">' .
                            "EDIT CONTRIBUTOR" .
                            '</p>' . '<p class="btn-choose-file contributor-radio1">' .

                            '<input id="new22" name="option24" type="radio" value="New" onclick="edit_contributorFunction2()" checked="" />'
                            . '<label for="new22">' . "New" . '</label>' .
                            '<input id="new23" name="option24" type="radio" value="Existing" onclick="edit_interfaceExistingFunction()" />' .
                            '<label for="new23">' . "Existing" . '</label>' .

                            '</p>' . '<div class="form-group form-group60" hidden>' .

                            '<label class="label-title label-title-2">' . "Which PRO" . '<img src="source/image/content/question.jpg">' . '</label>' .
                            '<select class="form-control form-control-pro show-member" id="sel1">' .
                            '<option value="">' . '</option>' .

                            '</select>' .
                            '</div>'
                            . '<div class="member_contributor" hidden>' .
                            '<p>' . '<span class="member_left1 color-member-left">' . "MEMBER of a PRO:" . '</span>' . '<span class="member_right">' . "Yes, Buma" . '</span>' . '</p>' .
                            '<p>' . '<span class="member_left2 color-member-left">' . "CAE Number:" . '</span>' . '<span class="member_right">' . "189904BM" . '</span>' . '</p>' .
                            '<p>' . '<span class="member_left3 color-member-left">' . "MEMBER of a mech, society:" . '</span>' . '<span class="member_right">' . "Yes, Stemra" . '</span>' . '</p>' .
                            '<p>' . '<span class="member_left4 color-member-left">' . "CAE Number:" . '</span>' . '<span class="member_right">' . "2884242RP" . '</span>' . '</p>' .
                            '</div>' . '<form id="userform11" method="POST">' . '<input type="hidden" name="_token" value="' . csrf_token() . '">' . '<input type="hidden" name="id_song" value="' . $idsong . '">' . '<input type="hidden" name="id_contributor" value="' . $idcontributor . '">' . '<p class="text-upload-audio">' . '<div class="form-group form-group50 edit_hide">' .
                            '<label class="label-title">' . "First name" . '<img src="source/image/content/question.jpg">' . '</label>' .

                            '<input type="text" name="txtFirstName" value="' . $val1[0] . '" class="form-control form-title" placeholder="First name">' .

                            '</div>' . '</p>' .
                            '<p class="text-upload-audio">' .
                            '<div class="form-group form-group50 edit_hide">' .
                            '<label class="label-title">' . "Last name" . '<img src="source/image/content/question.jpg">' . '</label>' .

                            '<input type="text" name="txtLastName" class="form-control form-title" value="' . $val1[1] . '" placeholder="Last name">' .

                            '</div>' .
                            '</p>' .
                            '<p class="check-agreement check-agreement2  form-group50 edit_hide">' . '<span class="squaredTwo">' .
                            '<input type="checkbox" value="' . $val1[3] . '" id="squaredSix33" class="which_pro1" name="check" onclick="shsel8Function()" ' . $check_pro . ' >' .
                            '<label for="squaredSix33">' . '</label>' . '</span>' . '<span class="color-text-checkbox color-text-checkbox2">' . " Member of a PRO" . '<img src="source/image/content/question.jpg">' . '</span>' .
                            '</p>' . '<div class="form-group form-group300 edit_hide" hidden>' .

                            '<label class="label-title label-title-2">' . "Which PRO" . '<img src="source/image/content/question.jpg">' . '</label>' .
                            '<select class="form-control form-control-pro form-sel1" id="sel1" name="which_pro_option">' .
                            '<option value="' . $check_pro2 . '">' . $check_pro2 . '</option>' . '<option>' . "Please choose" . '</option>' .
                            '<option>' . "BUMA" . '</option>' . '<option>' . "Stemra" . '</option>' .
                            '</select>' .
                            '<input type="hidden" name="sel_val1" class="sel_val1" value="' . $check_pro2 . '">' .
                            '</div>' .
                            '<div class="form-group form-group300 edit_hide" hidden>' .
                            '<label class="label-title">' . "CAE number" . '<img src="source/image/content/question.jpg">' . '</label>' .

                            '<input type="text" name="cae_number3" class="form-control form-title" placeholder="Title" value="' . $cae_number2 . '">' .

                            '</div>' . '<p class="check-agreement check-agreement2 form-group5 edit_hide">' . '<span class="squaredTwo">' .
                            '<input type="checkbox" value="' . $val1[6] . '" id="squaredSeven44" class="mechanical_society1" onclick="shsel7Function()" name="check_society2" ' . $check_society . '>' .
                            '<label for="squaredSeven44">' . '</label>' .
                            '</span>' . '<span class="color-text-checkbox">' . " Member of a mechanical society " . '<img src="source/image/content/question.jpg">' . '</span>' .
                            '</p>' .
                            '<div class="form-group form-group4" hidden>' .

                            '<label class="label-title label-title-2">' . "Which society" . '<img src="source/image/content/question.jpg">' . '</label>' .
                            '<select class="form-control form-control-pro form-sel2" name="society_option" id="sel1">' .
                            '<option value="' . $which_society2 . '">' . $which_society2 . '</option>' . '<option>' . "Please choose" . '</option>' .
                            '<option>' . "BUMA" . '</option>' . '<option>' . "Stemra" . '</option>' .
                            '</select>' . '<input type="hidden" name="sel_val2" class="sel_val2" value="' . $which_society2 . '">' .
                            '</div>' .
                            '<div class="form-group form-group4" hidden>' .
                            '<label class="label-title">CAE number<img src="source/image/content/question.jpg">' . '</label>' .

                            '<input type="text" name="cae_number4" class="form-control form-title" placeholder="Title" value="' . $cae_society . '">' .

                            '</div>' .
                            '<div class="form-group">' .
                            '<label class="label-title label-title-role">' . "Role" . '<img src="source/image/content/question.jpg">' . '</label>' .
                            '</div>' .
                            '<p class="btn-choose-file contributor-radio1">' .

                            '<input id="composer25" name="option28" type="radio" value="Composer" ' . $check1 . '/>' .
                            '<label for="composer25">' . "Composer" . '</label>' .
                            '<input id="lyricist26" name="option28" type="radio" value="Lyricist" ' . $check2 . '/>' .
                            '<label for="lyricist26">' . "Lyricist" . '</label>' .
                            '<input id="both27" name="option28" type="radio" value="Both" ' . $check3 . '/>' .
                            '<label for="both27">Both</label>' .


                            '</p>' .


                            '<p class="btn-choose-file btn-add-contributor5 color-a">' .
                            '<span id="btnchange-color" class="btn btn-upload-audio btn-upload-audio2 btn-add-contributor2 btn-edit-contributor">' . "SAVE" . '</span>' .
                            '<a onclick="document.getElementById(\'idEditContributor\').style.display=\'none\'">' . "Cancel" . '</a>' .
                            '</p>' . '</form>' .
                            '</div>';
                        echo "<script>";
                        echo "if ($('input.which_pro1').is(':checked')){jQuery('.form-group300').css('display', 'block');} ";
                        echo "if ($('input.mechanical_society1').is(':checked')){jQuery('.form-group4').css('display', 'block');} ";
                        echo "</script>";


                        echo "<script>";
                        echo "$( 'input.which_pro1' ).change(function() {
                    if($('input.which_pro1').is(':checked')){
                        $( 'input.which_pro1' ).attr('value', 1);
                    }else{
                        $( 'input.which_pro1' ).attr('value', 0);
                    }
                    });";
                        echo "$( 'input.mechanical_society1' ).change(function() {
                    if($('input.mechanical_society1').is(':checked')){
                        $( 'input.mechanical_society1' ).attr('value', 1);
                    }else{
                        $( 'input.mechanical_society1' ).attr('value', 0);
                    }
                    });";

                        echo "</script>";

                        echo "<script>";
                        echo "$( '.form-sel1' ).change(function() {
                    var optsel4 = $( '.form-sel1 option:selected' ).text();
                       $('.sel_val1').val(optsel4);
                    });";
                        echo "$( '.form-sel2' ).change(function() {
                    var optsel5 = $( '.form-sel2 option:selected' ).text();
                       $('.sel_val2').val(optsel5);
                    });";

                        echo "</script>";

                        //dd($val);
                        break;
                    }
                }
            }

        }
    }

    public function postSaveEditContributor(Request $request)
    {
        $song_id = $request->id_song;
        $contributor_id = $request->id_contributor;
        $first_name = $request->txtFirstName;
        $last_name = $request->txtLastName;
        if ($request->option28 == "Both") {
            $role = "Composer + Lyricist";
        } else {
            $role = $request->option28;
        }
        if ($request->check == 1) {
            $member_pro = 1;
            $which_pro_option = $request->sel_val1;
            $cae_number3 = $request->cae_number3;
        } else {
            $member_pro = 0;
            $which_pro_option = "";
            $cae_number3 = "";
        }
        if ($request->check_society2 == 1) {
            $check_society2 = 1;
            $society_option = $request->sel_val2;
            $cae_number4 = $request->cae_number4;
        } else {
            $check_society2 = 0;
            $society_option = "";
            $cae_number4 = "";
        }
        $update_contributor = Session()->get('song_info');
        if (isset($update_contributor[$song_id][12][$contributor_id])) {
            $update_contributor[$song_id][12][$contributor_id][0] = $first_name;
            $update_contributor[$song_id][12][$contributor_id][1] = $last_name;
            $update_contributor[$song_id][12][$contributor_id][2] = $role;
            $update_contributor[$song_id][12][$contributor_id][3] = $member_pro;
            $update_contributor[$song_id][12][$contributor_id][4] = $which_pro_option;
            $update_contributor[$song_id][12][$contributor_id][5] = $cae_number3;
            $update_contributor[$song_id][12][$contributor_id][6] = $check_society2;
            $update_contributor[$song_id][12][$contributor_id][7] = $society_option;
            $update_contributor[$song_id][12][$contributor_id][8] = $cae_number4;
            Session()->put('song_info', $update_contributor);
            Session()->save();
            echo '<span id="' . $first_name . "_" . $last_name . '" hidden>' . '</span>' .
                '<span class="contributors-name">' . $first_name . " " . $last_name . '</span>' .
                '<span class="role">' . $role . '</span>' .
                '<span class="ratio-percent" hidden="">' . '</span>' .
                '<input type="hidden" class="ratio-percent-val">' .
                '<input type="text" name="contributors_equal2[]" class="percent-share2" placeholder="0" style="
       ">' . '<span class="percent-icon1">' . "&#37;" . '</span>' .
                '<i class="fa fa-pencil" data-idsong="' . $song_id . '" data-name2="' . $contributor_id . '" >' . '</i>' .
                '<i class="fa fa-remove fa-remove2" data-idsong="' . $song_id . '" data-idcontributor="' . $contributor_id . '" style="font-size:24px; color: red" >' . '</i>' . '<div class="border">' . '</div>';
        }
    }

    public function postSaveEditSong(Request $request)
    {
        $song_id = $request->id_song;
        $place_saved = $request->place_saved;
        $name_song = $request->txtNameSong;
        $minute = $request->txtMinute;
        $second = $request->txtSecond;
        if ($minute == '') {
            $time = $second . " sec ";
        } elseif ($second == '') {
            $time = $minute . " min ";
        } else {
            $time = $minute . " min " . $second . " sec";
        }
        if ($request->check_contains_sample == 1) {
            $contains_sample = 1;
            $rights_to_use_samples = $request->check_right_sample;
        } else {
            $contains_sample = 0;
            $rights_to_use_samples = 0;
        }
        if ($request->check_remix == 1) {
            $remix = 1;
        } else {
            $remix = 0;
        }
        if ($request->check_prev_registered == 1) {
            $prev_registered = 1;
            $which_pro = $request->sel_songs;
            $prs_tunecode = $request->PRS_Tunecode;
        } else {
            $prev_registered = 0;
            $which_pro = null;
            $prs_tunecode = null;
        }

        if ($request->file_music1 != null) {
            $song1 = $request->file_music1;
            $name_random1 = $request->name_random1;
        } else {
            $song1 = null;
            $name_random1 = null;
        }
        if ($request->file_music2 != null) {
            $song2 = $request->file_music2;
            $name_random2 = $request->name_random2;
        } else {
            $song2 = null;
            $name_random2 = null;
        }
        $update_song = Session()->get('song_info');

        if ($request->contributors_equal[0] != 0) {
            $contributors_equal = $request->contributors_equal;
            $contributors_equal_ext = 1;
            foreach ($update_song as $key => $val) {
                if($key == $song_id){
                foreach ($val[$key][12] as $key1 => $val1) {
                   $update_song[$song_id][$key][12][$key1][9] = $contributors_equal[0];
                }
                }               
        }

        } else {
            $contributors_equal_ext = 0;
            $contributors_equal = $request->contributors_equal2;
            foreach ($update_song as $key => $val) {
                
                if($key == $song_id and $val[0][12]!= '' ){
                foreach ($val[0][12] as $key1 => $val1) {
                   $update_song[$song_id][$key][12][$key1][9] = $contributors_equal[0];
                }

                }               
        }
        }

        if (isset($update_song[$song_id])) {
            $update_song[$song_id][0][0] = $name_song;
            $update_song[$song_id][0][1] = $time;
            $update_song[$song_id][0][2] = $contains_sample;
            $update_song[$song_id][0][3] = $rights_to_use_samples;
            $update_song[$song_id][0][4] = $remix;
            $update_song[$song_id][0][5] = $prev_registered;
            $update_song[$song_id][0][6] = $which_pro;
            $update_song[$song_id][0][7] = $prs_tunecode;

            if(empty($name_random1)){

                $update_song[$song_id][0][9] = null;
                $update_song[$song_id][0][8] = null;
            }else{
                $update_song[$song_id][0][9] = $name_random1;
                $update_song[$song_id][0][8] = $song1;
            }
            if(empty($name_random2)){

                $update_song[$song_id][0][10] = null;
                $update_song[$song_id][0][11] = null;
            }else{
                $update_song[$song_id][0][10] = $song2;
                $update_song[$song_id][0][11] = $name_random2;
            }


            if(!empty(session('comtributors'))){
                $update_song[$song_id][0][12] = session('comtributors');
            }
            if(session('contributors_edit') == 1){
                $update_song[$song_id][0][12] = null;
            }

            if($request->check == 1){
                $update_song[$song_id][0][13] = $request->check;
            }else{
                $update_song[$song_id][0][13] = 0;
            }

            $update_song[$song_id][0][14] = $minute;
            $update_song[$song_id][0][15] = $second;

            if(!empty($update_song[$song_id][0][12])){

                if($request->check == 'None'){
                    if(!empty($request->contributors_equal)){
                        for($i=0;$i<count($request->contributors_equal);$i++){
                            $update_song[$song_id][0][12][$i][9]= $request->contributors_equal[$i];
                        }
                    }

                }else{
                    if(!empty($request->contributors_equal2)) {
                        for ($i = 0; $i < count($request->contributors_equal2); $i++) {
                            $update_song[$song_id][0][12][$i][9] = $request->contributors_equal2[$i];
                        }
                    }
                }
            }

            Session()->forget('song_info');
            Session()->put('song_info', $update_song);
            Session()->forget('comtributors');
            Session()->forget('contributors_edit');
            if(Session('filesong1')){

                Session()->forget('filesong1');
            }
            if(Session('filename1')){
                Session()->forget('filename1');
            }
            if(Session('filesong2')){
                Session()->forget('filesong2');
            }
            if(Session('filename2')){
                Session()->forget('filename2');
            }
//            dd($request->all(),$update_song);
            if($place_saved == "home_saved"){
            return redirect('saved-song');
            }else{
            return redirect('list-songs');
        }
        }
    }

    public function getDeleteSong1($song1){
        Session()->pull('song_info.' . $song1);
        Session()->forget('comtributors');
        Session()->save();
    }
    
    public function postCopySong(Request $request){

        $song='';
        $data_comtributors='';
        $data_comtributors_info='';
        if($request->check_copy == 'Contributors'){
            $data_comtributors = Contributors::where('id_song',$request->id_song)->get();
        }else{
            $song = Songs::where('id',$request->id_song)->first();
            $data_comtributors_info = Contributors::where('id_song',$request->id_song)->get();
        }

        return view('page.copy_song',['song'=>$song,'data_comtributors'=>$data_comtributors,'data_comtributors_info'=>$data_comtributors_info]);
    }

    public function getDeleteFileSong1(){
        if(Session('filesong1')){
            unlink("source/music/".session('filesong1'));
            Session()->forget('filesong1');
        }
        if(Session('filename1')){
            Session()->forget('filename1');
        }
        if(Session('filesong2')){
            unlink("source/music/".session('filesong2'));
            Session()->forget('filesong2');
        }
        if(Session('filename2')){
            Session()->forget('filename2');
        }
        echo '<label id="fileLabel22" class=" name-song-file inputfile inputfile-1 instrumental-1 instrumental-2" data-multiple-caption="{count} files selected" multiple="">' . "No file uploaded" . '</label>' .
            '<div onclick="upload_audioFunction()" class="btn btn-upload-audio5 btn-song2">' . "UPLOAD" . '</div>' .
            '<img class="listen_music1 song111 song22" src="source/image/content/play1.jpg">' .
            '<i class="fa fa-remove song333 song2">' . '</i>';
    }

    public function getDeleteFileSong2() {
        if(Session('filesong1')){
            unlink("source/music/".session('filesong1'));
            Session()->forget('filesong1');
        }
        if(Session('filename1')){
            Session()->forget('filename1');
        }
        if(Session('filesong2')){
            unlink("source/music/".session('filesong2'));
            Session()->forget('filesong2');
        }
        if(Session('filename2')){
            Session()->forget('filename2');
        }
            echo '<label id="fileLabel22" class="name-song-file inputfile inputfile-1 instrumental-1 instrumental-2" data-multiple-caption="{count} files selected" multiple="">' . "No file uploaded" . '</label>' .
            '<div onclick="upload_audioFunction2()" class="btn btn-upload-audio5 btn-song2">' . "UPLOAD" . '</div>' .
            '<img class="listen_music1 song111 song22" src="source/image/content/play1.jpg">' .
            '<i class="fa fa-remove song333 song2">' . '</i>';

}

public function postAddSongCopy(Request $request)
    {
        $name_song = $request->txtNameSong;
        $minute = $request->txtMinute;
        $second = $request->txtSecond;
        if ($minute == '') {
            $time = $second . " sec ";
        } elseif ($second == '') {
            $time = $minute . " min ";
        } else {
            $time = $minute . " min " . $second . " sec";
        }
        if ($request->check_contains_sample == 1) {
            $contains_sample = 1;
            $rights_to_use_samples = $request->check_right_sample;
        } else {
            $contains_sample = 0;
            $rights_to_use_samples = 0;
        }
        if ($request->check_remix == 1) {
            $remix = 1;
        } else {
            $remix = 0;
        }
        if ($request->check_prev_registered == 1) {
            $prev_registered = 1;
            $which_pro = $request->sel_songs;
            $prs_tunecode = $request->PRS_Tunecode;
        } else {
            $prev_registered = 0;
            $which_pro = null;
            $prs_tunecode = null;
        }
     
        if ($request->file_music1 != null) {
            $song1 = $request->file_music1;
            $name_random1 = $request->name_random1;
        } else {
            $song1 = null;
            $name_random1 = null;
        }
        if ($request->file_music2 != null) {
            $song2 = $request->file_music2;
            $name_random2 = $request->name_random2;
        } else {
            $song2 = null;
            $name_random2 = null;
        }
        if ($request->contributors_firstname != null) {
            $contributorsfirstname = $request->contributors_firstname;
        } else {
            $contributorsfirstname = null;
        }
        if ($request->contributors_lastname != null) {
            $contributorslastname = $request->contributors_lastname;
        } else {
            $contributorslastname = null;
        }
     
        if ($request->contributors_promember != null) {
            $contributors_promember = $request->contributors_promember;
        } else {
            $contributors_promember = null;
        }
        if ($request->contributors_cae_number != null) {
            $contributors_cae_number = $request->contributors_cae_number;
        } else {
            $contributors_cae_number = null;
        }

        if ($request->contributors_mech_society_member != null) {
            $contributors_mech_society_member = $request->contributors_mech_society_member;
        } else {
            $contributors_mech_society_member = null;
        }
        if ($request->contributors_mech_society_number != null) {
            $contributors_mech_society_number = $request->contributors_mech_society_number;
        } else {
            $contributors_mech_society_number = null;
        }
        $contributorsrole = $request->contributors_role;
        $contributors_memberpro = $request->contributors_memberpro;
        $contributors_member_society = $request->contributors_member_society;
        if ($request->contributors_equal[0] != 0) {
            $contributors_equal = $request->contributors_equal;
            $contributors_equal_ext = 1;
        } else {
            $contributors_equal = $request->contributors_equal2;
            $contributors_equal_ext = 0;
        }


        if($contributorsfirstname != null) {
            $count = count($contributorsfirstname);


            $contributors = [$contributorsfirstname, $contributorslastname, $contributorsrole, $contributors_memberpro, $contributors_promember, $contributors_cae_number, $contributors_member_society, $contributors_mech_society_member, $contributors_mech_society_number, $contributors_equal];


            $contributors2 = [];

            foreach ($contributors as $key => $value) {
                for ($i = 0; $i < $count; $i++) {

                    $contributors2[$i][] = $value[$i];
                }

            }
        }else{
            $contributors2 = null;
        }

        $arr_str = [$name_song, $time, $contains_sample, $rights_to_use_samples, $remix, $prev_registered, $which_pro, $prs_tunecode, $song1, $name_random1, $song2, $name_random2, $contributors2, $contributors_equal_ext, $minute, $second];
        Session()->push('song_info', $arr_str);
        Session()->save();
        return redirect('publish-your-song')->with($arr_str);
    }

}
