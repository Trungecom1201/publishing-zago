<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\User;

class UserController extends Controller
{
    public function getDanhSach()
    {
    	$user = User::all();
        return view('admin.user.danhsach',['user'=>$user]);
    }
    public function getThem()
    {
        $user = User::all();
        return view('admin.user.them',['user'=>$user]);	
    }
    public function postThem(Request $request)
    {
        $this->validate($request,
           [
              'txtFirstName' => 'required|min:3|max:20',
               'txtLastName' => 'required|min:3|max:20',
              'txtEmail' => 'required|min:6|max:100|email|unique:users,email',
               'txtPhone' => 'required|min:6|max:20',
              'txtPass' => 'required|min:3|max:32',
              'txtRePass' => 'required|min:3|max:32|same:txtPass',
               'txtAddress' => 'required|min:6|max:20',

           ],
           [
               'txtFirstName.required' => 'You have not entered first name',
               'txtFistName.min' => 'Too short, must be 3 characters or more',
               'txtFistName.max' => 'Too long, up to 30 characters',
               'txtEmail.required' => 'You have not entered email',
               'txtEmail.min' => 'Too short, must be 6 characters or more',
               'txtEmail.max' => 'Too long, up to 100 characters',
               'txtEmail.email' => 'Enter the wrong email format',
               'txtEmail.unique' => 'This email name already exists',
               'txtLastName.required' => 'You have not entered last name',
               'txtPhone.required' => 'You have not entered phone',
               'txtAddress.required' => 'You have not entered address',
               'Image.required' => 'You have not entered image',
               'txtPass.required' => 'You have not entered password',
               'txtPass.min' => 'Too short, must be 3 characters or more',
               'txtPass.max' => 'Too long, up to 32 characters',
               'txtRePass.required' => 'You have not entered re-password',
               'txtRePass.min' => 'Too short, must be 3 characters or more',
               'txtRePass.max' => 'Too long, up to 32 characters',
               'txtRePass.same' => 'Passwords are not the same'
           ]); 
        $user = new User;
        $user->first_name = $request->txtFirstName;
        $user->last_name = $request->txtLastName;
        $user->email = $request->txtEmail;
        $user->phone = $request->txtPhone;
        $user->password = bcrypt($request->txtPass);
        $user->type_user = $request->rdoLevel;
        $user->address = $request->txtAddress;
        if($request->hasFile('Image'))
        {
            $file = $request->file('Image');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
                return redirect('admin/user/them')->with('loi','You only have to select the file with the extension are jpg, png or jpeg');
            }
            $name = $file->getClientOriginalName();
            $Image = str_random(4)."_". $name;
            while(file_exists("source/image/admin/user/".$Image)) {
                $Image = str_random(4)."_". $name;
            }
            $file->move("source/image/admin/user",$Image);
            $user->image = $Image;
        }else
        {
            $user->image = "";
        }
        //$user->image = $request->Image;
       // $user->quyen = $request->rdoLevel;
        $user->save();
        return redirect('admin/user/them')->with('thongbao','Thêm thành công!');
    }
    public function getSua($id)
    {
      $user = User::find($id);
    	return view('admin.user.sua',['user'=>$user]);
    }
    public function postSua(Request $request,$id)
    {
    	$this->validate($request,
           [
              'txtFirstName' => 'required|min:3|max:20',
           ],
           [
               'txtFirstName.required' => 'Bạn chưa nhập tên',
               'txtFirstName.min' => 'Ngắn quá, phải từ 3 ký tự trở lên',
               'txtFirstName.max' => 'Dài quá, tối đa 30 ký tự'
           ]); 
        $user = User::find($id);
        $user->first_name = $request->txtFirstName;
        $user->last_name = $request->txtLastName;
        $user->email = $request->txtEmail;
        $user->phone = $request->txtPhone;
        $user->type_user = $request->rdoLevel;
        $user->address = $request->txtAddress;

        if($request->changePass == "on")
        {
          $this->validate($request,
           [
              'txtPass' => 'required|min:3|max:32',
              'txtRePass' => 'required|min:3|max:32|same:txtPass'
           ],
           [
               'txtPass.required' => 'You have not entered a password',
               'txtPass.min' => 'Too short, must be 3 characters or more',
               'txtPass.max' => 'Too long, up to 32 characters',
               'txtRePass.required' => 'You have not entered a password',
               'txtRePass.min' => 'Too short, must be 3 characters or more',
               'txtRePass.max' => 'Too long, up to 32 characters',
               'txtRePass.same' => 'Passwords are not the same'
           ]);
        $user->password = bcrypt($request->txtPass);
        }
        $user->save();
        return redirect('admin/user/sua/'.$id)->with('thongbao','Edit successfully!');
    }
    public function getXoa($id)
    {
    	$user = User::find($id);
    	$user->delete();
    	return redirect('admin/user/danhsach')->with('thongbao','Delete successfully!');
    }
    public function getLoginAdmin()
    {
      return view('admin.login');
    }
    public function postLoginAdmin(Request $request)
    {
      // dd($request->all());
       $this->validate($request,
           [
              'email' => 'required',
              'password' => 'required|min:3|max:32'
           ],
           [
               'email.required' => 'You have not entered an email',
               'password.required' => 'You have not entered a password',
               'password.min' => 'Too short, must be 3 characters or more',
               'password.max' => 'Too long, up to 32 characters'
           ]);
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
        {
          return redirect('admin/user/danhsach');
        }
        else
        {
          return redirect('admin/login')->with('thongbao','Login unsuccessful!');
        }
    }
    public function getDangxuatAdmin()
    {
      Auth::logout();
      return redirect('admin/dangnhap');
    } 
}
