<div class="header">
    <a href="#"><img src="source/image/header/logo1.jpg"></a>
    <div class="user">
        <i id="bell1" class="fa fa-bell-o"></i>
        <p class="count-news">4</p>
        @if(Auth::check())
            <div class="dropdown dropdown-user">
                <a data-toggle="dropdown" href=""><img src="source/image/header/user.jpg"><span
                        class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <div class="divider"></div>
                    <li><a href="logout">Log out</a></li>
                </ul>
            </div>
        @else
            <a class="login" href="login">Login</a>
        @endif
    </div>
    <nav class="navbar navbar-inverse">

        <div class="menu-mobi">
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="navbar-collapse collapse" id="menu">
            <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Manager</a></li>
                <li><a href="{{route('home')}}">Create</a></li>
                <li><a href="#">Distribute</a></li>
                <li><a href="#">Promote</a></li>
                <li><a href="#">Gig</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Support</a></li>
                <li><a href="#"><img src="source/image/header/icon-menu2.jpg">Academy</a></li>
                <li class="dropdown">
                    <a data-toggle="dropdown" href="" style="color: #4e4949;">Toolbox<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Abcd</a></li>
                        <li><a href="#">Abcd</a></li>
                        <div class="divider"></div>
                        <li><a href="#">Abcd</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>
</div>
<div class="border-horizon"></div>
<div class="notification">
    <p>One man like</p>
    <p>One man like</p>
    <p>One man like</p>
</div>
<style>
    #bell1 {
        position: relative;
    }

    .notification {
        display: none;
        position: absolute;
        right: 0;
        min-width: 150px;
        border-radius: 4px;
        border: 1px solid;
        padding: 5px;
        background-image: linear-gradient(to right, #D45EF2, #4794CC);
        max-height: 300px;
        overflow: scroll;
    }
    .notification > p:not(:last-child){
        padding-bottom: 5px;
        border-bottom: 1px solid;
    }
</style>
<script>
    $('#bell1').append($('.notification'));
    window.onscroll = function () {
        myfunction()
    };

    function myfunction() {
        var abc = $('.rev-slider').offset().top;
        if (document.body.scrollTop > abc) {
            $('body').addClass('menu-fixed');
        } else {
            $('body').removeClass('menu-fixed');
            $('#menu').slideUp();
            $('.menu-mobi #nav-icon1').removeClass('change');
        }
    }

    $('.menu-mobi #nav-icon1').click(function () {
        $(this).toggleClass('change');
        $('#menu').slideToggle();
    });
    $('#bell1').click(function () {
        $('.notification').slideToggle();
    });
</script>
