@extends('admin.layout.index')
@section('content')
    <div id="page-wrapper" class="page-admin-claims">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">List Song
                        <small>list</small>
                    </h1>
                </div>
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Song Title</th>
                        <th>Station/Channel</th>
                        <th>Program</th>
                        <th>Air Date</th>
                        <th>Country</th>
                        <th>evidence</th>
                        <th>Song</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($claims as $key=>$claim)
                        <?php $key += 1 ?>
                        <tr class="odd gradeX" align="center">
                            <td>{{$key}}</td>
                            <td>{{$claim->song_title}}</td>
                            <td>{{$claim->station_channel}}</td>
                            <td>{{$claim->program}}</td>
                            <td>{{$claim->air_date}}</td>
                            <td>{{$claim->country}}</td>
                            <td>{{$claim->evidence}}</td>
                            <td><a href="source/music/{{$claim->file_url}}" download>{{$claim->file}}</a></td>
                            <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/songs/delete-claims/{{$claim->id}}">Delete</a></td>
                            <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/songs/edit/{{$claim->id}}">Edit</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@endsection