@extends('admin.layout.index')
@section('content')
<div id="page-wrapper" style="width: 135%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">List Song
                            <small>list</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                   <!-- <div class="col-lg-7" style="padding-bottom:120px">-->
                    @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>User first name</th>
                                <th>User last name</th>
                                <th>User email</th>
                                <th>Song title</th>
                                {{--<th>Status</th>--}}
                                <th>Duration</th>
                                <th>Contains samples</th>
                                <th>Rights to use samples</th>
                                {{--<th>Samples info</th>--}}
                                <th>Remix</th>
                                <th>Prev registered with pro</th>
                                <th>Which pro</th>
                                <th>Prs tunecode</th>
                                <th>Audio original</th>
                                <th>File url</th>
                                <th>Audio instrumental</th>
                                <th>File url instrumental</th>
                                <th>Equal shares</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($songs as $s)
                            <tr class="odd gradeX" align="center">
                                <td>{{$s->id}}</td>
                                <td>{{$s->user_first_name}}</td>
                                <td>{{$s->user_last_name}}</td>
                                <td>{{$s->user_email}}</td>
                                <td>{{$s->song_title}}</td>
                                {{--<td>{{$s->status}}</td>--}}
                                <td>{{$s->duration}}</td>
                                <td>
                                @if($s->contains_samples == 1)
                                    {{'Yes'}}
                                    @else
                                    {{'No'}}
                                    @endif
                                </td>
                                <td>
                                @if($s->rights_to_use_samples == 1)
                                    {{'Yes'}}
                                    @else
                                    {{'No'}}
                                    @endif
                                </td>
                                {{--<td>{{$s->samples_info}}</td>--}}
                                <td>
                                @if($s->remix == 1)
                                    {{'Yes'}}
                                    @else
                                    {{'No'}}
                                    @endif
                                </td>
                                <td>
                                @if($s->prev_registered_with_pro == 1)
                                    {{'Yes'}}
                                    @else
                                    {{'No'}}
                                    @endif
                                </td>
                                <td>{{$s->which_pro}}</td>
                                <td>{{$s->prs_tunecode}}</td>
                                <td>{{$s->audio_original}}</td>
                                <td>{{$s->file_url}}</td>
                                <td>{{$s->audio_instrumental}}</td>
                                <td>{{$s->file_url_instrumental}}</td>
                                <td>{{$s->equal_shares}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/songs/delete/{{$s->id}}">Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/songs/edit/{{$s->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        @endsection