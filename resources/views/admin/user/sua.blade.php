@extends('admin.layout.index')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User
                            <small>{{$user->name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                        {{$err}}<br>
                        @endforeach
                    </div>
                    @endif

                    @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                    @endif
                        <form action="admin/user/sua/{{$user->id}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <div class="form-group">
                                <label>First name</label>
                                <input type="text" id="txtFirstName" name="txtFirstName" value="{{$user->first_name}}" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <input type="text" id="txtLastName" name="txtLastName" value="{{$user->last_name}}" class="form-control" />
                            </div>
                            <div class="form-group">    
                            <label>Email</label>
                            <input type="email" id="txtEmail" name="txtEmail" value="{{$user->email}}" class="form-control" readonly="" />
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" id="txtPhone" name="txtPhone" value="{{$user->phone}}" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Type user</label>
                                <label class="radio-inline">
                                    <input name="rdoLevel" value="0"
                                           @if($user->type_user == 0)
                                           {{"checked"}}
                                           @endif
                                           type="radio">User
                                </label>
                                <label class="radio-inline">
                                    <input name="rdoLevel" value="1"
                                           @if($user->type_user == 1)
                                           {{"checked"}}
                                           @endif
                                           type="radio">Admin
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" id="txtAddress" name="txtAddress" value="{{$user->address}}" class="form-control" />
                            </div>
                            <div class="form-group">
                            <input type="checkbox" name="changePass" id="changePass">    
                            <label>Change Password</label>
                            <input type="password" id="txtPass" name="txtPass" class="form-control password" placeholder="Password" disabled="" />
                            </div>
                            <div class="form-group">    
                            <label>Repassword</label>
                            <input type="password" id="txtRePass" name="txtRePass" placeholder="Repassword" class="form-control password" disabled="" />
                            </div>
                            <button type="submit" class="btn btn-default">Edit</button>
                            <button type="reset" class="btn btn-default">Refresh</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        @endsection
        

        @section('script')
        <script>
        $(document).ready(function(){
        $("#changePass").change(function() {
        if($(this).is(":checked"))
        {
         $(".password").removeAttr('disabled');
        }else
        {
         $(".password").attr('disabled','');
        }
        });
        });
        </script>
        @endsection
