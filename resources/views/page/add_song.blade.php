@extends('master')
@section('content')
    <?php
    $data = session('info');
    //dd($data);
    ?>
    <div class="content content-add-song add-song-page">
        <form action="add" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <p class="title-content title-content-agreement">ADD SONG</p>
            <div class="add-song-1">
                <p class="title-add-song">SONG INFO</p>
                <div class="song-info">
                    <div class="form-group">
                        <label class="label-title">Title<img src="source/image/content/question.jpg"></label>
                        <input type="text" required name="txtNameSong" value="{{$data[0][0]}}"
                               class="form-control form-title form-title5" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <?php
                        $min = strpos($data[0][1], " min");
                        $sec = strpos($data[0][1], " sec");
                        ?>
                        <label class="label-title">Duration<img src="source/image/content/question.jpg"></label>
                        <p class="add-song-duration minute"><input type="number" name="txtMinute"
                                                                   value="{{substr($data[0][1],$min-1,1)}}"
                                                                   class="form-control form-title form-duration form-title6"
                                                                   placeholder="0"><span>min</span>
                        </p>
                        <p class="add-song-duration second"><input type="number" name="txtSecond"
                                                                   value="{{substr($data[0][1],$sec-1,1)}}"
                                                                   class="form-control form-title form-duration form-title7"
                                                                   placeholder="0"><span>sec</span>
                        </p>
                        <div class="clear"></div>
                    </div>

                    <p class="check-agreement check-agreement2"><span class="squaredTwo">
  <input type="checkbox" id="squaredTwo" @if($data[0][2] == 1)value="1" checked @endif class="check_contains_sample"
         onclick="shsel5Function()" name="check_contains_sample"/>
  <label for="squaredTwo"></label>
</span><span class="color-text-checkbox"> Contains sample(s) <img src="source/image/content/question.jpg"> </span>
                    </p>
                    <p class="check-agreement check-agreement2 abcd" style="@if($data[0][2] != 1)display: none; @endif"><span
                            class="squaredFive">
  <input type="checkbox" @if($data[0][3] == 1)value="1" checked @endif id="squaredFive" class="check_right_sample"
         name="check_right_sample"/>
  <label for="squaredFive"></label>
</span><span class="color-text-checkbox"> Do you have the right to use the sample(s)? <img
                                src="source/image/content/question.jpg"> </span>
                    </p>
                    <p class="check-agreement check-agreement2"><span class="squaredThree">
  <input type="checkbox" @if($data[0][4] == 1)value="1" checked @endif id="squaredThree" class="check_remix"
         name="check_remix"/>
  <label for="squaredThree"></label>
</span><span class="color-text-checkbox"> Remix <img src="source/image/content/question.jpg"> </span>
                    </p>
                    <p class="check-agreement check-agreement2"><span class="squaredFour">
  <input type="checkbox" @if($data[0][5] == 1)value="1" checked @endif id="squaredFour" class="check_prev_registered"
         onclick="shsel1Function()" name="check_prev_registered"/>
  <label for="squaredFour"></label>
</span><span class="color-text-checkbox"> Previously registered with a PRO <img src="source/image/content/question.jpg"></span>
                    </p>
                    <div class="form-group form-group2" @if($data[0][5] != 1) style="display:none" @endif>
                        <label class="label-title label-title-2">Which PRO <img src="source/image/content/question.jpg"></label>
                        <div class="styleSelect">
                            <select class="form-control form-control-pro units" id="sel1" name="sel_songs">
                                <option value="0">Please choose</option>
                                <option value="BUMA" @if($data[0][6] == 'BUMA') selected @endif>BUMA</option>
                                <option value="Stemra" @if($data[0][6] == 'Stemra') selected @endif>Stemra</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group2 add-song-tunecode"
                         @if($data[0][5] != 1) style="display:none" @endif >
                        <label class="label-title">PRS Tunecode<img src="source/image/content/question.jpg"></label>
                        <input type="text" @if($data[0][7]) value="{{$data[0][7]}}" @endif name="PRS_Tunecode"
                               class="form-control form-title" placeholder="PRS Tunecode">
                    </div>
                </div>
            </div>
            <div class="add-song-2">
                <div style="margin-bottom: 12px">
                    <span class="title-add-song">CONTRIBUTORS</span>
                    <span class="title-add-song title-role ">role</span>
                    <span class="title-add-song title-share">share</span>
                </div>
                <p id="note_contributor" class="group-contributors" style="border-radius: 4px; overflow: hidden;"></p>

                @if(empty($data))
                    <p id="note_contributor" class="sub-title-add-song" style="">No contributors added yet. Please at
                        least one.</p>
                @else
                    @if(!empty($data[0][12]))
                        <?php $var = 0;?>
                            {{--<div class="group-contributors" style="border-radius: 4px; overflow: hidden;">--}}
                        @foreach($data[0][12] as $key => $comtributors)
                            <div class="contributors id-{{$var}}">
                                <span id="teat_asd" hidden=""></span>
                                <span class="contributors-name">{{$comtributors[0]}} {{$comtributors[1]}}</span>
                                <input type="hidden" name="contributors_firstname[]" value="{{$comtributors[0]}}">
                                <input type="hidden" name="contributors_lastname[]" value="{{$comtributors[1]}}">
                                <input type="hidden" name="contributors_role[]" value="{{$comtributors[2]}}">
                                <input type="hidden" name="contributors_memberpro[]" value="{{$comtributors[3]}}">
                                <input type="hidden" name="contributors_promember[]" value="{{$comtributors[4]}}">
                                <input type="hidden" name="contributors_cae_number[]" value="{{$comtributors[5]}}">
                                <input type="hidden" name="contributors_member_society[]" value="{{$comtributors[6]}}">
                                <input type="hidden" name="contributors_mech_society_member[]"
                                       value="{{$comtributors[7]}}">
                                <input type="hidden" name="contributors_mech_society_number[]"
                                       value="{{$comtributors[8]}}">
                                <input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal">
                                <span class="role"><span class="role-mobi">ROLE: </span>{{$comtributors[2]}}</span>
                                <span class="ratio-percent" @if($data[0][13] == 1) style="display:block" @endif >{{$comtributors[9]}}%</span>
                                <input type="text" name="contributors_equal2[]"
                                       @if($data[0][13] != 1) value="{{$comtributors[9]}}" @endif class="percent-share2"
                                       placeholder="0" @if($data[0][13] == 1) style="display:none"@endif>
                                <span class="percent-icon1" @if($data[0][13] == 1) style="display:none" @endif>%</span>
                                <i class="fa fa-pencil" data-name2="{{$key}}"></i>
                                <i class="fa fa-remove fa-remove2" data-name="{{$key}}"
                                   style="font-size:24px; color: red"></i>
                                <div class="border"></div>
                            </div>
                            <?php $var += 1?>
                        @endforeach
                            {{--</div>--}}
                        <div class="half-past-percent">
                            <p class="check-agreement check-contributors">
                  <span class="squaredTwo">
                  <input type="checkbox" id="squaredEight" name="check" @if($data[0][13] == 1) checked value="1"
                         @else value="None" @endif>
                  <label for="squaredEight">   </label>
                  </span>
                                <span class="equal-share equal-share2">Equal share</span>
                            </p>
                        </div>
                    @endif
                @endif
                <div onclick="add_contributorFunction()" class="btn btn-add-contributor btn-add-contributors">+ ADD
                    CONTRIBUTOR
                </div>
                <p class="title-add-song">AUDIO FILES</p>
                <div class="audio-files">
                    <div class="box-audio-files">
                        <span class="option-audio">Original <img src="source/image/content/question.jpg"></span>
                        <div class="box box4">
                            @if(session('filename1'))
                                <label id="fileLabel" name="file_music1"
                                       class="name-song inputfile inputfile-1 add-margin-top"
                                       data-multiple-caption="{count} files selected" multiple=""><img
                                        class="icon-music"
                                        src="source/image/content/music2.jpg">{{session('filename1')}}</label>
                                <input type="hidden" value="{{session('filename1')}}" name="file_music1">
                                <input type="hidden" value="{{session('filesong1')}}" name="name_random1">
                                <div class="play_music"><i class="fa fa-play"></i>
                                    <audio controls="" style="display:none" class="upload-song-1">
                                        <source src="source/music/{{session('filesong1')}}" type="audio/mpeg">
                                    </audio>
                                </div>
                                <i class="fa fa-remove song333 song1 song-original" style="display: block;"
                                   onclick="originalFunction()" data-name="{{session('filesong1')}}"></i><img
                                    class="listen_music1 song111 song11" src="source/image/content/play1.jpg"><i
                                    class="fa fa-remove song333 song1"></i>
                            @else
                                <label id="fileLabel"
                                       class="name-song-file inputfile inputfile-1 add-margin-top no-name-file"
                                       data-multiple-caption="{count} files selected" multiple="">No file
                                    uploaded.</label>
                                <div onclick="upload_audioFunction()" class="btn btn-upload-audio5 btn-song1">UPLOAD
                                </div>
                                <img class="listen_music1 song111 song11" src="source/image/content/play1.jpg" style="
     /* float: right; */
">
                                <i class="fa fa-remove song333 song1"></i>
                            @endif
                        </div>
                    </div>
                    <div class="border"></div>
                    <div class="box-audio-files">
                        <span class="option-audio">Instrumental <img src="source/image/content/question.jpg"></span>
                        <div class="box box5">
                            @if(session('filename2'))
                                <label id="fileLabel" name="file_music2"
                                       class="name-song inputfile inputfile-1 add-margin-top"
                                       data-multiple-caption="{count} files selected" multiple=""><img
                                        class="icon-music"
                                        src="source/image/content/music2.jpg">{{session('filename2')}}</label>
                                <input type="hidden" value="{{session('filename2')}}" name="file_music2">
                                <input type="hidden" value="{{session('filesong2')}}" name="name_random2">
                                <div class="play_music"><i class="fa fa-play"></i>
                                    <audio style="display:none" controls="" class="upload-song-2">
                                        <source src="source/music/{{session('filesong2')}}" type="audio/mpeg">
                                    </audio>
                                </div>
                                <i class="fa fa-remove song333 song1 song-intrumental" style="display: block;"
                                   onclick="instrumentalFunction()" data-name="{{session('filesong2')}}"></i>
                                <img class="listen_music1 song111 song11" src="source/image/content/play1.jpg"><i
                                    class="fa fa-remove song333 song1"></i>
                            @else
                                <label id="fileLabel22"
                                       class="name-song-file inputfile inputfile-1 instrumental-1 instrumental-2 no-name-file"
                                       data-multiple-caption="{count} files selected" multiple="">No file
                                    uploaded.</label>
                                <div onclick="upload_audioFunction2()" class="btn btn-upload-audio5 btn-song2">UPLOAD
                                </div>
                                <img class="listen_music1 song111 song22" src="source/image/content/play1.jpg">
                                <i class="fa fa-remove song333 song2"></i>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <p class="btn-warning1 btn-agreement1 btn-add-song1">
                <button onclick="warningnotcheckFunction()" class="btn btn-continue btn-add-song2 btn-publish-add-song">
                    PUBLISH
                </button>
                <button class="btn btn-continue btn-add-song2 btn-save-add-song">SAVE</button>
                <a class="cancel-add" onclick="cancel()">Cancel</a>
            </p>

            <div id="id04" class="modal upload-audio-file">
                <div class="modal-content modal-upload-song">
                    <form id="music_form" action="" method="POST" enctype="multipart/form-data">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <p class="title-home-warning title-upload-song">
                            UPLOAD AUDIO
                        </p>

                        <div class="box box2 box3">
                            <input type="file" name="file-2[]" id="file188" title="Choose a video please"
                                   onchange="pressed2()"><label id="fileLabel25"
                                                                class="inputfile name-song-file inputfile-1 add-margin-top inputfile-7 label-box"
                                                                data-multiple-caption="{count} files selected"
                                                                multiple="" onclick="upload_audioFunction5()">No file
                                chosen</label><label for="file188"> <span>CHOOSE FILE</span></label>
                        </div>
                        <button hidden="" class="upload-music-hd">submit</button>

                        <p class="text-upload-audio">You can upload an MP3 file with a max size of 20MB. The file name
                            can contain letters, numbers, spaces, underscores(_) and dashes(-).</p>
                        <p class="btn-choose-file">
                            <span class="btn btn-upload-audio btn-upload-audio2 upload-music">UPLOAD</span>
                            <a onclick="document.getElementById('id04').style.display='none'">Cancel</a>
                        </p>
                    </form>
                </div>
            </div>

            <div id="id03" class="modal upload-audio-file">
                <div class="modal-content modal-upload-song">
                    <form id="music_form_2" action="" method="POST" enctype="multipart/form-data">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <p class="title-home-warning title-upload-song">
                            UPLOAD AUDIO
                        </p>
                        <div class="box box2">
                            <input type="file" name="file-2[]" id="file18" title="Choose a video please"
                                   onchange="pressed()">
                            <label id="fileLabel23" class="inputfile  inputfile-1 add-margin-top inputfile-7 label-box"
                                   data-multiple-caption="{count} files selected" multiple=""
                                   onclick="upload_audioFunction()">No file chosen</label><label for="file18"> <span>CHOOSE FILE</span></label>
                        </div>
                        <button hidden="" class="upload-music-hd-2">submit</button>
                        <p class="text-upload-audio">You can upload an MP3 file with a max size of 20MB. The file name
                            can contain letters, numbers, spaces, underscores(_) and dashes(-).</p>
                        <p class="btn-choose-file">
                            <span class="btn btn-upload-audio btn-upload-audio2 upload-music-2">UPLOAD</span>
                            <a onclick="document.getElementById('id03').style.display='none'">Cancel</a>
                        </p>
                    </form>
                </div>
            </div>

            <div id="idAddContributor" class="modal">
                <div class="modal-content modal-add-contributor">
                    <form id="userform" method="POST">
                        {{ csrf_field() }}
                        <p class="title-home-warning title-upload-song">
                            ADD CONTRIBUTOR
                        </p>
                        <p class="btn-choose-file contributor-radio1">
                            <input id="new14" name="option18" type="radio" value="New"
                                   onclick="add_contributorFunction2()" checked=""/>
                            <label for="new14" class="add-new-contributor">New</label>
                            <input id="new8" name="option18" type="radio" value="Existing"
                                   onclick="interfaceExistingFunction()"/>
                            <label for="new8" class="add-exist-contributor">Existing</label>
                        </p>
                        <div class="form-group form-group6" hidden>

                            <select class="form-control form-control-pro show-member show-member222 exis-contributors"
                                    id="sel1" name="sel1">
                                @if(count($contributors) > 0)
                                    <option value="Please choose">Please choose</option>
                                    @foreach ($contributors as $key => $val)
                                        <option data-first_name="{{$val->first_name}}"
                                                data-last_name="{{$val->last_name}}" data-role="{{$val->role}}"
                                                data-share="{{$val->share}}"
                                                data-pro_member_check="{{$val->pro_member_check}}"
                                                data-which_pro="{{$val->which_pro}}"
                                                data-cae_number="{{$val->cae_number}}"
                                                data-member_society_check="{{$val->member_society_check}}"
                                                data-mech_society_member="{{$val->mech_society_member}}"
                                                data-mech_society_number="{{$val->mech_society_number}}"
                                                value="<?php echo $key; ?>">{{$val->first_name}} {{$val->last_name}}</option>
                                    @endforeach
                                @else
                                    <option value="Please choose">No contributors</option>
                                @endif
                            </select>
                            <div class="existing-container">

                            </div>
                        </div>
                        <div class="member_contributor" hidden></div>
                        <p class="text-upload-audio">
                        <div class="form-group form-group5">
                            <label class="label-title">First name<img src="source/image/content/question.jpg"></label>
                            <input type="text" required name="txtFirstName"
                                   class="form-control form-title form-first-name" placeholder="First name">
                        </div>
                        </p>
                        <p class="text-upload-audio">
                        <div class="select-option-form">
                            <div class="form-group form-group5">
                                <label class="label-title">Last name<img
                                        src="source/image/content/question.jpg"></label>
                                <input type="text" id="test1" name="txtLastName"
                                       class="form-control form-title form-last-name" placeholder="Last name">
                            </div>
                        </p>
                        <p class="check-agreement check-agreement2  form-group5"><span class="squaredTwo">
  <input type="checkbox" value="0" id="squaredSix11" class="member_pro" onclick="shsel6Function()" name="MemberPro">
  <label for="squaredSix11"></label>
</span><span class="color-text-checkbox"> Member of a PRO <img src="source/image/content/question.jpg"> </span>
                        </p>
                        <div class="form-group form-group3" hidden>
                            <label class="label-title label-title-2">Which PRO <img
                                    src="source/image/content/question.jpg"></label>
                            <select class="form-control form-control-pro member_pro_which_pro" id="sel2" name="sel2"
                                    value="">
                                <option>Please choose</option>
                                <option>BUMA</option>
                                <option>Stemra</option>
                            </select>
                            <input type="hidden" class="sel2_val" name="sel2_val">
                        </div>
                        <div class="form-group form-group3" hidden>
                            <label class="label-title">CAE number<img src="source/image/content/question.jpg"></label>
                            <input type="text" name="txtCAE_Number" class="form-control form-title member_pro_number"
                                   placeholder="CAE number">

                        </div>
                        <p class="check-agreement check-agreement2 form-group5"><span class="squaredTwo">
  <input type="checkbox" value="0" id="squaredSeven" class="member_society" onclick="shsel7Function()"
         name="MemberSociety">
  <label for="squaredSeven"></label>
</span><span class="color-text-checkbox"> Member of a mechanical society <img src="source/image/content/question.jpg"> </span>
                        </p>
                        <div class="form-group form-group4" hidden>
                            <label class="label-title label-title-2">Which society <img
                                    src="source/image/content/question.jpg"></label>
                            <select class="form-control form-control-pro member_society_which_pro" id="sel3"
                                    name="sel3">
                                <option>Please choose</option>
                                <option>BUMA</option>
                                <option>Stemra</option>
                            </select>
                            <input type="hidden" class="sel3_val" name="sel3_val">
                        </div>
                        <div class="form-group form-group4" hidden>
                            <label class="label-title">CAE number<img src="source/image/content/question.jpg"></label>
                            <input type="text" name="txtSocietyNumber"
                                   class="form-control form-title member_society_number" placeholder="CAE number">

                        </div>
                </div>
                <div class="form-group form-group7 role-show">
                    <label class="label-title label-title-role">Role<img
                            src="source/image/content/question.jpg"></label>
                </div>
                <p class="btn-choose-file contributor-radio1 contributor-radio3 role-show">
                    <input id="composer7" name="option2" type="radio" value="Composer" checked=""/>
                    <label for="composer7">Composer</label>
                    <input id="lyricist7" name="option2" type="radio" value="Lyricist"/>
                    <label for="lyricist7">Lyricist</label>
                    <input id="both7" name="option2" type="radio" value="Both"/>
                    <label for="both7">Both</label>
                </p>
                <p class="btn-choose-file btn-add-contributor5 color-a">
                    <span id="btnchange-color212"
                          class="btn btn-upload-audio btn-upload-audio2 btn-add-contributor2 btn-save-contributor3 add-btn-save-contributor">SAVE</span>
                    <a class="cancel-contributor" style="margin-left: 10px"
                       onclick="document.getElementById('idAddContributor').style.display='none'">Cancel</a>
                </p>
        </form>
    </div>
    </div>

    <div id="idEditContributor" class="modal">
    </div>

    <div id="checkright-sample" class="modal">
        <div class="modal-content modal-content-warning-agreement">
            <p class="btn-ok-warning-agreement">Please confirm that you have the right to use the sample(s) you've
                used.</p>
            <p class="btn-warning1">
                <button onclick="document.getElementById('checkright-sample').style.display='none'" class="btn">OKAY
                </button>
            </p>
        </div>
    </div>

    <div id="check-song-contributor" class="modal">
        <div class="modal-content modal-content-warning-agreement">
            <p class="btn-ok-warning-agreement">Please add a title and contributor to save this song.</p>
            <p class="btn-warning1">
                <button onclick="document.getElementById('check-song-contributor').style.display='none'" class="btn">
                    OKAY
                </button>
            </p>
        </div>
    </div>
    </form>
    </div>


    <script type="text/javascript">
        if($('.contributors').length >= 1 ){
            $('.title-role, .title-share').show();
        }else {
            $('.title-role, .title-share').hide();
        }
        function agreeFunction() {
            location.href = "{{route('home')}}";
        }

        function warningnotcheckFunction() {
            if (!$('input.form-title').val()) {
                document.getElementById('id01').style.display = 'block';
            } else if (!$('input#squaredFive').is(":checked")) {
                document.getElementById('id02').style.display = 'block';
            } else {
                location.href = "{{route('publish your song')}}";
            }
        }

        function upload_audioFunction() {
            document.getElementById('id03').style.display = 'block';
        }

        function upload_audioFunction2() {
            document.getElementById('id04').style.display = 'block';
        }

        function shsel1Function() {
            $(".form-group2").slideToggle();
        }

        function shsel5Function() {
            $("p.abcd").slideToggle(0.001);
        }

        function shsel6Function() {
            $(".form-group3").slideToggle();
        }

        function shsel7Function() {
            $(".form-group4").slideToggle();
        }

        function shsel8Function() {
            $(".form-group3").slideToggle();
        }

        function add_contributorFunction() {
            document.getElementById('idAddContributor').style.display = 'block';
        }

        function add_contributorFunction2() {
            $(".form-group6").hide();
            $(".form-group5").show();
            $(".form-group7").show();
            $(".contributor-radio3").show();
            if ($('.member_contributor').is(':visible')) {

                $(".member_contributor").css("display", "none");
            }

        }

        function cancel() {
            event.preventDefault();
            event.stopPropagation();
            window.location.href = "./"
        }

        function interfaceExistingFunction() {

            $(".form-group5").hide();
            $(".form-group6").show();
            $(".form-group7").hide();
            $(".contributor-radio3").hide();

        }

        function shsel2Function() {
            $('#squaredTwo').click(function () {
                $(".form-group2").toggle(this.checked);
            });
        }

        function edit_interfaceExistingFunction() {
            $(".edit_hide").hide();
            $(".form-group60").show();
        }

        function edit_contributorFunction2() {
            $(".form-group60").hide();
            $(".edit_hide").show();
            if ($('.member_contributor').is(':visible')) {
                $(".member_contributor").css("display", "none");
            }

        }

        function change_color111Function() {
            if (!$("#squaredSix33").is(':checked')) {
                $(".btn-add-contributor2").css("background-color", "#AAAAAA");
            } else {
                $(".btn-add-contributor2").css("background-color", "#F35667");
            }
        }

        function save_songFunction() {
            var b = document.getElementById('file18').value;
            if (b == "") {
                fileLabel22.innerHTML = "Choose file";
            } else {
                var theSplit = b.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
                $("#id03").hide();
                $(".btn-song1").css("display", "none");
                $(".song1").css("display", "block");
                $(".song11").css("display", "inline");
            }
        }

        function save_songFunction2() {
            var b = document.getElementById('file188').value;
            if (b == "") {
                fileLabel22.innerHTML = "Choose file";
            } else {
                var theSplit = b.split('\\');
                fileLabel22.innerHTML = theSplit[theSplit.length - 1];
                $("#id04").hide();
                $(".btn-song2").css("display", "none");
                $(".song2").css("display", "block");
                $(".song22").css("display", "inline");
            }
        }

        $(document).ready(function () {
            window.pressed = function () {
                var a = document.getElementById('file18').value;
                if (a == "") {
                    fileLabel23.innerHTML = "Choose file";
                } else {
                    var theSplit = a.split('\\');
                    fileLabel23.innerHTML = theSplit[theSplit.length - 1];
                    $(".btn-upload-audio2").css("background-color", "#F35A5C");
                }
            };

            window.pressed2 = function () {
                var d = document.getElementById('file188').value;
                if (d == "") {
                    fileLabel25.innerHTML = "Choose file";
                } else {
                    var theSplit = d.split('\\');
                    fileLabel25.innerHTML = theSplit[theSplit.length - 1];
                    $(".btn-upload-audio2").css("background-color", "#F35A5C");
                }
            };
            $('#squaredEight').change(function () {
                $(".show-member222").change(function () {
                    var optcontributors = $(".show-member222 option:selected").val();
                    $.get("exis-contributors/" + optcontributors, function () {

                    }).done(function (data) {
                        $('.member_contributor').html(data);
                        $('.member_contributor').css('display', 'block');

                    }).fail(function () {
                        alert("error");
                    });
                });

                if ($("#squaredSix33").is(':checked')) {
                    $(".form-group300").slideToggle();
                }
                if ($("#squaredEight").is(':checked')) {
                    debugger;
                    var numItems = $('.contributors').length;
                    var number_percent = Math.floor((1 / numItems) * 100);

                    $('.ratio-percent').css("display", "block");
                    $('.ratio-percent').text(number_percent + "%");
                    $('.ratio-percent-val').val(number_percent);
                    $('.percent-share2').css("display", "none");
                    $('span.percent-icon1').css("display", "none");
                    $('.contributors_equal').val(number_percent);
                } else {

                    $('.percent-share2').css("display", "block");
                    $('span.percent-icon1').css("display", "block");
                    $('.ratio-percent-val').val("");

                }

            });
            $("#squaredSix33").click(function () {
                $(".form-group300").slideToggle();
            });

            $('.form-title7').keyup(function () {
                if ($('.form-title5').val() != "" && $('.form-title6').val() != "" && $('.form-title7').val() != "") {
                    $(".btn-add-song2").css("background-color", "#F35667");
                } else {
                    $(".btn-add-song2").css("background-color", "#AAAAAA");
                }
            });
            $('.form-title6').keyup(function () {
                if ($('.form-title5').val() != "" && $('.form-title6').val() != "" && $('.form-title7').val() != "") {
                    $(".btn-add-song2").css("background-color", "#F35667");
                } else {
                    $(".btn-add-song2").css("background-color", "#AAAAAA");
                }
            });
            $('.form-title5').keyup(function () {
                if ($('.form-title5').val() != "" && $('.form-title6').val() != "" && $('.form-title7').val() != "") {
                    $(".btn-add-song2").css("background-color", "#F35667");
                } else {
                    $(".btn-add-song2").css("background-color", "#AAAAAA");
                }
            });
            var play_song_upload1 = true;
            $('.audio-files').on('click', 'img.song11', function () {
                if (play_song_upload1 === true) {
                    $('img.song11').attr("src", "source/image/content/pause.jpg");
                    play_song_upload1 = false;
                } else {
                    $('img.song11').attr("src", "source/image/content/play1.jpg");
                    play_song_upload1 = true;
                }
            })
            var play_song_upload2 = true;
            $('.audio-files').on('click', 'img.song22', function () {
                if (play_song_upload2 === true) {
                    $('img.song22').attr("src", "source/image/content/pause.jpg");
                    play_song_upload2 = false;
                } else {
                    $('img.song22').attr("src", "source/image/content/play1.jpg");
                    play_song_upload2 = true;
                }
            })
            $('.form-last-name').keyup(function () {
                if ($('.form-first-name').val() != "" && $('.form-last-name').val() != "") {
                    $(".btn-save-contributor3").css("background-color", "#F35667");
                } else {
                    $(".btn-save-contributor3").css("background-color", "#AAAAAA");
                }
            });
            $('.form-first-name').keyup(function () {
                if ($('.form-first-name').val() != "" && $('.form-last-name').val() != "") {
                    $(".btn-save-contributor3").css("background-color", "#F35667");
                } else {
                    $(".btn-save-contributor3").css("background-color", "#AAAAAA");
                }
            });
            $("#squaredThree").change(function () {
                if ($("#squaredThree").is(':checked')) {
                    $(".check_remix").attr('value', 1);
                } else {
                    $(".check_remix").attr('value', 0);
                }
            });
            $(".check_contains_sample").change(function () {
                if ($(".check_contains_sample").is(':checked')) {
                    $(".check_contains_sample").attr('value', 1);
                } else {
                    $(".check_contains_sample").attr('value', 0);
                }
            });
            $(".check_right_sample").change(function () {
                if ($(".check_right_sample").is(':checked')) {
                    $(".check_right_sample").attr('value', 1);
                } else {
                    $(".check_right_sample").attr('value', 0);
                }
            });
            $(".check_prev_registered").change(function () {
                if ($(".check_prev_registered").is(':checked')) {
                    $(".check_prev_registered").attr('value', 1);
                } else {
                    $(".check_prev_registered").attr('value', 0);
                }
            });
            $(".member_pro").change(function () {
                if ($(".member_pro").is(':checked')) {
                    $(".member_pro").attr('value', 1);
                } else {
                    $(".member_pro").attr('value', 0);
                }
            });
            $(".member_society").change(function () {
                if ($(".member_society").is(':checked')) {
                    $(".member_society").attr('value', 1);
                } else {
                    $(".member_society").attr('value', 0);
                }
            });
            $(".which_pro1").change(function () {
                if ($(".which_pro1").is(':checked')) {
                    $(".which_pro1").attr('value', 1);
                } else {
                    $(".which_pro1").attr('value', 0);
                }
            });
            $("#sel2").change(function () {
                var optsel2 = $("#sel2 option:selected").text();
                $(".sel2_val").val(optsel2);
            });
            $("#sel3").change(function () {
                var optsel3 = $("#sel3 option:selected").text();
                $(".sel3_val").val(optsel3);
            });
            $("body").delegate(".fa-remove2", "click", function () {
                var contributor1 = jQuery(this).data('name');
                var item_class = $(this);
                $.get("delete-contributor/" + contributor1, function () {

                })
                    .done(function (data) {
                        item_class.parents('.contributors').remove();
                        if ($('.fa-remove2').length == 0) {
                            $('.half-past-percent').remove();
                            $('.title-role, .title-share').hide();
                            $('.sub-title-add-song').show();
                        }
                    })
                    .fail(function () {
                        alert("error");
                    });
            });
            $("body").delegate(".fa-pencil", "click", function () {
                var contributor2 = $(this).data('name2');
                $.get("edit-contributor/" + contributor2, function () {

                })
                    .done(function (data) {
                        $('#idEditContributor').html(data);
                        document.getElementById('idEditContributor').style.display = 'block';
                        $('.btn-edit-contributor').on('click', function () {
                            var contributor_edit = $(this).parent().parent().parent().data("id");
                            $.ajax({
                                type: 'POST',
                                url: 'edit-contributor1',
                                data: $("#userform11").serialize()
                            })
                                .done(function (data) {
                                    $(".id-" + contributor_edit).html(data);
                                    $('#idEditContributor').hide();
                                    $('.fa-pencil').click(function () {
                                        var contributor2 = $(this).data('name2');
                                        $.get("edit-contributor/" + contributor2, function () {

                                        }).done(function (data) {
                                            $('#idEditContributor').html(data);
                                            document.getElementById('idEditContributor').style.display = 'block';
                                            $('.btn-edit-contributor').on('click', function () {
                                                var contributor_edit = $(this).parent().parent().parent().data("id");
                                                $.ajax({
                                                    type: 'POST',
                                                    url: 'edit-contributor1',
                                                    data: $("#userform11").serialize()
                                                })
                                                    .done(function (data) {
                                                        $(".id-" + contributor_edit).html(data);
                                                        $('#idEditContributor').hide();
                                                    })
                                                    .fail(function () {
                                                        alert("Please choose a file to upload.");
                                                    });
                                                return false;
                                            });
                                        })
                                            .fail(function () {
                                                alert("error");
                                            });
                                    });
                                })
                                .fail(function () {
                                    alert("Please choose a file to upload.");
                                });
                            return false;
                        });
                    }).fail(function () {
                    alert("error");
                });
            });
            $('#btnchange-color212').on('click', function () {
                if ($('input[name="txtFirstName"]').val() == '' && $('input[name="txtLastName"]').val() == '') {
                } else {
                    $.ajax({
                        type: 'POST',
                        url: 'save-contributor',
                        data: $("#userform").serialize()
                    })
                        .done(function (data) {
                            $('.contributors').remove();
                            $('.half-past-percent').remove();
                            if ($('.sub-title-add-song').length > 0) {
                                $('.sub-title-add-song').css('display', 'none');
                                $('.title-role, .title-share').show();
                            }
                            $('#note_contributor').html(data);

                            $('#squaredEight').change(function () {
                                if ($("#squaredEight").is(':checked')) {
                                    var numItems = $('.contributors').length;
                                    var number_percent = Math.floor((1 / numItems) * 100);

                                    $('.ratio-percent').css("display", "block");
                                    $('.ratio-percent').text(number_percent + "%");
                                    $('.ratio-percent-val').val(number_percent);
                                    $('.percent-share2').css("display", "none");
                                    $('span.percent-icon1').css("display", "none");
                                    $('.contributors_equal').val(number_percent);
                                } else {
                                    $('.percent-share2').val("");
                                    $('.percent-share2').css("display", "block");
                                    $('span.percent-icon1').css("display", "block");
                                    $('.ratio-percent-val').val("");
                                    $('.contributors_equal').val(0);
                                }

                            });

                            $('#idAddContributor').hide();
                            // $('.fa-remove2').click(function(){
                            //     var contributor1 = jQuery(this).data('name');
                            //     var item_class =  $(this);
                            //     $.get("delete-contributor/"+contributor1, function()
                            //     {
                            //
                            //     }).done(function(data) {
                            //         item_class.parents('.contributors').remove();
                            //         if($('.fa-remove2').length == 0){
                            //             $('.half-past-percent').remove();
                            //         }
                            //     })
                            //         .fail(function() {
                            //             alert( "error" );
                            //         });
                            // });
                        })
                        .fail(function () {
                            alert("Please choose a file to upload.");
                        });
                }
                return false;
            });
            $('.upload-music-2').on('click', function () {
                var name = document.getElementById('file18');
                var data = new FormData();
                for (var i = 0; i < name.files.length; i++) {
                    data.append('file_song' + i, name.files[i]);
                    console.log(data);
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "add-file",
                    method: "POST",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                }).done(function (data) {
                    $('.box4').html(data);
                    $('#id03').css('display', 'none');
                })
                    .fail(function () {
                        alert("Please choose a file to upload.");
                    });
            });
            $('.upload-music').on('click', function () {
                var name = document.getElementById('file188');
                var data = new FormData();
                for (var i = 0; i < name.files.length; i++) {
                    data.append('file_song2' + i, name.files[i]);
                }
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "add-file2",
                    method: "POST",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                }).done(function (data) {
                    $('.box5').html(data);
                    $('#id04').css('display', 'none');
                })
                    .fail(function () {
                        alert("Please choose a file to upload.");
                    });
            });
        });

        function originalFunction() {
            var name_music = $('.upload-song-1').children().attr('src').substring(13);
            $.get("delete-file/" + name_music, function () {
            })
                .done(function (data) {
                    $('.box4').html(data);
                })
                .fail(function () {
                    alert("error");
                });
        }

        $('.remove-original').click(function () {
            var name_music = $('.upload-song-1').children().attr('src').substring(13);
            $.get("delete-file/" + name_music, function () {
            })
                .done(function (data) {
                    $('.box4').html(data);
                })
                .fail(function () {
                    alert("error");
                });
        });


        function instrumentalFunction() {
            name_music = $('.upload-song-2').children().attr('src').substring(13);
            $.get("delete-file2/" + name_music, function () {
            }).done(function (data) {
                $('.box5').html(data);
            }).fail(function () {
                alert("error");
            });

        }

        $(function () {
            $('.remove-instrumental').click(function () {
                var name_music = $('.upload-song-2').children().attr('src').substring(13);
                $.get("delete-file2/" + name_music, function () {
                }).done(function (data) {
                    $('.box5').html(data);
                }).fail(function () {
                    alert("error");
                });
            });
            $('input[type=file]').change(function () {
                var val = $(this).val().toLowerCase(),
                    regex = new RegExp("(.*?)\.(mp3|mp4)$");
                if (!(regex.test(val))) {
                    $(this).val('');
                    $(this).next().text('No file chosen');
                    alert('Please select correct file format');
                }
            });
        });
    </script>
@endsection
