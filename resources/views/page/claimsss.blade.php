@extends('master')
@section('content')
<div class="content content-claim-empty">
<p class="title-content title-content-agreement">ADD BROADCAST CLAIM</p>
<div class="modal-claim-empty modal-add-contributor">
<p class="btn-choose-file contributor-radio1">
      
  <input id="new14" name="option18" type="radio" value="New" onclick="add_contributorFunction2()" checked="" />
  <label for="new14">New</label>
  <input id="new8" name="option18" type="radio" value="Existing" onclick="interfaceExistingFunction()" />
  <label for="new8">Existing</label>
  
    </p>
    <div class="form-group form-group6" hidden>
  
  <label class="label-title label-title-2">Which PRO <img src="source/image/content/question.jpg"></label>
  <select class="form-control form-control-pro show-member show-member222" id="sel1">
    <option value="">Please choose</option>
    <option value="Buma">BUMA</option>
  </select>
</div>
<div class="member_contributor" hidden>
  <p><span class="member_left1 color-member-left">MEMBER of a PRO:</span><span class="member_right">Yes, Buma</span></p>
  <p><span class="member_left2 color-member-left">CAE Number:</span><span class="member_right">189904BM</span></p>
  <p><span class="member_left3 color-member-left">MEMBER of a mech, society:</span><span class="member_right">Yes, Stemra</span></p>
  <p><span class="member_left4 color-member-left">CAE Number:</span><span class="member_right">2884242RP</span></p>
</div>
    <p class="text-upload-audio">
    	<div class="form-group form-group5">
                        <label class="label-title">First name<img src="source/image/content/question.jpg"></label>
                        
                            <input type="text" id="testabcd" name="hubt" class="form-control form-title" placeholder="First name">
                        
  </div>
</p>
<div class="form-group form-group3" hidden>
  
  <label class="label-title label-title-2">Which PRO <img src="source/image/content/question.jpg"></label>
  <select class="form-control form-control-pro" id="sel1">
    <option>BUMA</option>
  </select>
</div>
<div class="form-group form-group3" hidden>
                        <label class="label-title">Title<img src="source/image/content/question.jpg"></label>
                        
                            <input type="text" class="form-control form-title" placeholder="Title">
                        
  </div>
<p class="check-agreement check-agreement2 form-group5"><span class="squaredTwo">
  <input type="checkbox" value="1" id="squaredSeven" onclick="shsel7Function()" name="check">
  <label for="squaredSeven"></label>
</span><span class="color-text-checkbox"> Member of a mechanical society <img src="source/image/content/question.jpg"> </span>
</p>
<div class="form-group form-group4" hidden>
  
  <label class="label-title label-title-2">Which PRO <img src="source/image/content/question.jpg"></label>
  <select class="form-control form-control-pro" id="sel1">
    <option>BUMA</option>
  </select>
</div>
<div class="form-group form-group4" hidden>
                        <label class="label-title">Title<img src="source/image/content/question.jpg"></label>
                        
                            <input type="text" class="form-control form-title" placeholder="Title">
                        
  </div>
<div class="form-group">
                        <label class="label-title label-title-role">Role<img src="source/image/content/question.jpg"></label>
                        </div>
<p class="btn-choose-file contributor-radio1">
                            
  <input id="composer7" name="option2" type="radio" value="Composer" checked="" />
  <label for="composer7">Composer</label>
  <input id="lyricist7" name="option2" type="radio" value="Lyricist" />
  <label for="lyricist7">Lyricist</label>
  <input id="both7" name="option2" type="radio" value="Both" />
  <label for="both7">Both</label>
  
                        
  </p>
    <p class="btn-choose-file btn-add-contributor5 color-a">      
      <button onclick="agreeFunction()" id="btnchange-color" class="btn btn-upload-audio btn-upload-audio2 btn-add-contributor2 btn-save-contributor">SAVE</button>
      <a onclick="document.getElementById('idAddContributor').style.display='none'">Cancel</a>
    </p>
</div>
</div>
@endsection