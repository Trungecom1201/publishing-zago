@extends('master')
@section('content')
<div class="content page-claims">
    <h2 class="title-page text-style">broadcast claims</h2>
    <a href="add-claim-empty" class="btn-add-claim text-style">+&ensp;add claim</a>
    <!-- table 1 -->
    @if(count($claim) >=  1)
    <div class="table-concept table-claims">
        <div class="row-title">
            <div class="fleft col-concept text-style">concept</div>
            <div class="mobile-show hide-jq">
            <div class="fleft col-medium text-style">Medium</div>
            <div class="fleft col-channel text-style">Station/channel</div>
            <div class="fleft col-program text-style">program</div>
            <div class="fleft col-airdate text-style">air date</div>
            <div class="fleft col-county text-style">country</div>
            <div class="fleft col-submit text-style"></div>
            </div>
        </div>
        <div class="row-background">

                @foreach($claim as $key=>$claims)
            <div class="row-1 row-content">
                <div class="fleft col-content">
                    <div class="fleft col-concept">{{$claims->song_title}}</div>
                    {{--mobi--}}
                    <div class="mobile-show hide-jq">
                        <div class="fleft col-medium"><span class="text-style style-mobi">Medium :</span>
                            @if($claims->radio_tv == 1)
                                Radio
                            @else
                                TV
                            @endif</div>
                        <div class="fleft col-channel"><span class="text-style style-mobi">Station/channel :</span>
                            @if($claims->station_channel)
                                {{$claims->station_channel}}
                            @else
                                -
                            @endif</div>
                        <div class="fleft col-program"><span class="text-style style-mobi">program :</span>
                            @if($claims->program)
                                {{$claims->program}}
                            @else
                                -
                            @endif
                        </div>
                        <div class="fleft col-airdate"><span class="text-style style-mobi">air date :</span>{{date('d-m-Y', strtotime($claims->air_date))}}</div>
                        <div class="fleft col-county"><span class="text-style style-mobi">country :</span>
                            @if($claims->country)
                                {{$claims->country}}
                            @else
                                -
                            @endif
                        </div>
                    </div>
                    <div class="fleft col-submit">
                        @if($claims->station_channel == '' || $claims->program == '' || $claims->country == '' )
                        <a class="text-style bth-data-disable">submit</a>
                        @else
                            <a class="text-style bth-data-claim-submit btn-claim" data-id="{{$claims->id}}">Submit</a>
                        @endif

                    </div>
                </div>
                <div class="fleft col-click">
                    <a href="edit-claim/{{$claims->id}}">
                    <img src="source/image/content/icon-edit-claims.png">
                    </a>
                </div>
            </div>
                @endforeach
        </div>
    </div>
@endif
@if(count($claim_submit) >=  1)
    <!-- table 2 -->
    <div class="table-submitted table-claims">
        <div class="row-title">
            <div class="fleft col-concept text-style">submitted</div>
            <div class="mobile-show hide-jq">
            <div class="fleft col-medium text-style">Medium</div>
            <div class="fleft col-channel text-style">Station/channel</div>
            <div class="fleft col-program text-style">program</div>
            <div class="fleft col-airdate text-style">air date</div>
            <div class="fleft col-county text-style">country</div>
            <div class="fleft col-submit text-style">submit date</div>
            </div>
        </div>

            <div class="row-background">
                {{--row 1--}}
                @foreach($claim_submit as $key=>$val)
                <div class="row-3 row-content">
                    <div class="fleft col-content">
                        <div class="fleft col-concept">{{$val->	song_title}}</div>
                        {{--mobi--}}
                        <div class="mobile-show hide-jq">
                            <div class="fleft col-medium"><span class="text-style style-mobi">Medium :</span>@if($val->radio_tv == 1) Radio @else TV @endif</div>
                            <div class="fleft col-channel"><span class="text-style style-mobi">Station/channel :</span>{{$val->station_channel}}</div>
                            <div class="fleft col-program"><span class="text-style style-mobi">program :</span>{{$val->program}}</div>
                            <div class="fleft col-airdate"><span class="text-style style-mobi">air date :</span>{{date('d-m-Y',strtotime($val->air_date))}}</div>
                            <div class="fleft col-county"><span class="text-style style-mobi">country :</span>{{$val->country}}</div>
                            <div class="fleft col-submit"><span class="text-style style-mobi">submit date :</span>{{date('d-m-Y',strtotime($val->updated_at))}}</div>
                        </div>
                    </div>
                    <div class="fleft col-click get-info-claim" data-id="{{$val->id}}">
                        <img src="source/image/content/icon-see-claims.png">
                    </div>
                </div>
                @endforeach
            </div>


    </div>
    @endif
</div>
<div id="idSubmitClaim" class="modal" >
    <!-- Modal Content -->
    <div class="modal-content modal-content-publish-song modal-saved-claims modal-submit-claim">
        <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
            SUBMIT CLAIM?
        </p>
        <p class="text-home-warning">Are you sure you want to submit this broadcast<br>claim? You can't edit your claim later.</p>
        <p class="btn-warning1">
            <button onclick="document.getElementById('idSubmitClaim').style.display='none'" class="btn btn-cancel">CANCEL</button>
            <button onclick="agreeFunction()" class="btn btn-confirm">CONFIRM</button>
        </p>
    </div>
</div>
    <script>
                $('.col-content .col-concept').click(function () {
                    $(this).next($('.mobile-show')).slideToggle();
                });
    </script>
@endsection
