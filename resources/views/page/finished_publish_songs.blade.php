@extends('master')
@section('content')
<div class="content content-agreement content-publish-song">
<p class="title-content title-content-agreement">PUBLISHED SONG</p>
<p class="note-before-read publish-song-title">Publish date: 23-09-2018.</p>
<div class="parent-publish-song-info">
  <p class="title-publish-song title-song-info2">SONG INFO</p>
<div class="publish-song-info">
  <div class="publish-song-info1 padding-for-title-publish-song">
  <p class="title-option-publish-song">Title</p>
  <p class="content-option-publish-song">Volovia</p>
</div>
<div class="publish-song-info2 padding-for-title-publish-song border-publish-song">
  <p class="title-option-publish-song">Duration</p>
  <p class="content-option-publish-song">4 min 12 sec</p>
</div>
<div class="publish-song-info3 padding-for-title-publish-song">
  <p class="option-publish-song1"><img src="source/image/content/ticker1.jpg">Contains Sample(s)</p>
  <p class="option-publish-song1"><img src="source/image/content/ticker1.jpg">Do you have the right to use the sample(s)?</p>
</div>
<div class="publish-song-info4 padding-for-title-publish-song border-publish-song padding-special">
  <p class="option-publish-song1"><img src="source/image/content/unticker.jpg">Remix</p>
</div>
<div class="publish-song-info5 padding-for-title-publish-song">
  <p class="option-publish-song1 margin-bottom-table"><img src="source/image/content/ticker1.jpg">Previously registered with a PRO</p>
  <table border="0px" width="350px" height="50px">
    <tr class="change-color-row">
        <td class="padding-zero">which PRO</td>
        <td class="padding-zero">PRS TurnCode</td>
        
    </tr>
    <tr>
        <td class="padding-zero">BUMA</td>
        <td class="padding-zero">189904BM</td>
        
    </tr>
    
</table>
</div>
<div class="publish-song-info6 padding-for-title-publish-song border-publish-song">
  <p class="title-option-publish-song">Audio file (original)</p>
  <p class="content-option-publish-song test123"><img src="source/image/content/music2.jpg"> Volovia MASTERED final.mp3<span class="size-song">7.511 KB</span></p>
  <img class="listen_music1" src="source/image/content/play1.jpg" style="
    float: right;
    margin: -28px 0 0 0;
    cursor: pointer;
">
</div>
<div class="publish-song-info7 padding-for-title-publish-song">
  <p class="title-option-publish-song">Audio file (instrumental)</p>
  <p class="content-option-publish-song"><img src="source/image/content/music2.jpg"> Volovia INSTRUMENTAL.mp3<span class="size-song">7.290 KB</span></p>
  <img class="listen_music2" src="source/image/content/pause.jpg" style="
    float: right;
    margin: -28px 0 0 0;
    cursor: pointer;
">
</div>
</div>
</div>
<div class="parent-publish-contributor">
  <p class="title-publish-song title-publish-contributor">CONTRIBUTORS</p>
<div class="publish-contributor"> 
<div class="publish-contributor1 padding-for-title-publish-song"> 
<div class="publish-contributor-sub1 publish-contributor-sub"> 
<p class="title-option-publish-song">Name</p>
  <p class="content-option-publish-song contributors-details">Sander Wit</p>
    <p class="title-option-publish-song">Role</p>
  <p class="content-option-publish-song contributors-details">Composer + Lyricist</p>
    <p class="title-option-publish-song">Share</p>
  <p class="content-option-publish-song">65%</p>
</div>
<div class="publish-contributor-sub2 publish-contributor-sub"> 
<p class="option-publish-song1 margin-bottom-table"><img src="source/image/content/ticker1.jpg">Member of a PRO</p>
  <table border="0px" width="250px" height="50px">
    <tbody><tr class="change-color-row">
        <td class="padding-zero">which PRO</td>
        <td class="padding-zero">CAE Number</td>
        
    </tr>
    <tr>
        <td class="padding-zero">Stemra</td>
        <td class="padding-zero">7511055RA</td>
        
    </tr>
    
</tbody></table>
<p class="option-publish-song1 margin-bottom-table contributors-details3"><img src="source/image/content/unticker.jpg">Member of a mechanical society</p>
</div>
</div>
<div class="publish-contributor2 padding-for-title-publish-song border-publish-song"> 
<div class="publish-contributor-sub3 publish-contributor-sub"> 
<p class="title-option-publish-song">Name</p>
  <p class="content-option-publish-song contributors-details">Jelto van Nieuwburg</p>
    <p class="title-option-publish-song">Role</p>
  <p class="content-option-publish-song contributors-details">Lyricist</p>
    <p class="title-option-publish-song">Share</p>
  <p class="content-option-publish-song">35%</p>
</div>
<div class="publish-contributor-sub4 publish-contributor-sub"> 
<p class="option-publish-song1 contributors-details2"><img src="source/image/content/unticker.jpg">Member of a PRO</p>
    <p class="option-publish-song1"><img src="source/image/content/unticker.jpg">Member of a mechanical society</p>
</div>
</div>
<div class="publish-contributor3 padding-for-title-publish-song padding-special"> 
<p class="option-publish-song1"><img src="source/image/content/unticker.jpg">Equal shares</p>
</div>
</div>
</div>
<p class="btn-warning1 btn-agreement1 btn-publish-song">
  <button onclick="backFunction()" class="btn btn-continue btn-back1 btn-publish-song1">BACK</button>
</p>
<!-- The Modal -->
<div id="id01" class="modal" >
  <!-- Modal Content -->
  <div class="modal-content modal-content-publish-song">
    <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
      ARE YOU SURE?
    </p>
    <p class="text-home-warning">Please make sure everything is correct.<br>Your song will now be published.</p>
    <p class="btn-warning1">
      <button onclick="document.getElementById('id01').style.display='none'" class="btn btn-cancel">CANCEL</button>
      <button onclick="agreeFunction()" class="btn">CONFIRM</button>
    </p>
    <p class="check-agreement check-agreement5"><span class="squaredTwo">
  <input type="checkbox" value="None" id="squaredSixtys" name="check">
  <label for="squaredSixtys"></label>
</span><span class="text-publish-song"> Don't show this message again. </span></p> 
  

    
    
     
	
	<script type='text/javascript'>
    function agreeFunction() {
        location.href = "{{route('savedsong')}}";
    }
    function warningnotcheckFunction(){
      if ($('input#squaredTwo').is(':checked')) {
      location.href = "{{route('addsong')}}";
    }
    else{
      document.getElementById('id01').style.display='block';  
    }
  }
  var play_music1 = true;
  $('.publish-song-info6').on('click','img',function(){
    
    if (play_music1 === true){
     //alert($('img.testtest').attr('src'));
     $('img.listen_music1').attr("src","source/image/content/pause.jpg");
     play_music1 = false;
   }
     else {
      $('img.listen_music1').attr("src","source/image/content/play1.jpg");
      play_music1 = true;
     }   
  })
  var play_music2 = true;
  $('.publish-song-info7').on('click','img',function(){
    
    if (play_music2 === true){
     //alert($('img.testtest').attr('src'));
     $('img.listen_music2').attr("src","source/image/content/play1.jpg");
     play_music2 = false;
   }
     else {
      $('img.listen_music2').attr("src","source/image/content/pause.jpg");
      play_music2 = true;
     }   
  })
  function backFunction() {
        location.href = "{{route('list_songs')}}";
      // alert("Agree");
      //location.href = "{{route('agreement')}}";
      
    }
	</script>
    
    </div>
  </div>
</div>

</div>
@endsection