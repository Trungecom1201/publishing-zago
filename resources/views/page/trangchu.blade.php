@extends('master')
@section('content')
<div class="content">
<p class="title-content">PUBLISHING</p>
<div class="content-option content-option-1">
	<p class="title-option">PUBLISHED</p>
	<img src="source/image/content/music1.jpg">

	<p><button onclick="document.getElementById('id01').style.display='block'" class="btn">+ ADD SONG</button></p>
	<p class="note1">YOU HAVEN'T PUBLISHED ANY SONGS YET</p>
</div>
<div class="content-option">
	<p class="title-option">BROADCAST CLAIMS</p>
	<img src="source/image/content/movie1.jpg">
	<p><button class="btn btn1">+ ADD CLAIM</button></p>
	<p class="note1">PLEASE FIRST PUBLISH A SONG</p>
</div>

<!-- The Modal -->
<div id="id01" class="modal" >
  <span onclick="document.getElementById('id01').style.display='none'" 
class="close" title="Close Modal">&times;</span>

  <!-- Modal Content -->
  <div class="modal-content">
    <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
    	ORIGINAL MATERIAL
    </p>
    <p class="text-home-warning">Indieplant can only publish original material. Please confirm that this is your own work, or work of composer(s) and/or lyricist(s) you are entitled to publish work from.</p>
    <p class="btn-warning1"><button onclick="document.getElementById('id01').style.display='none'" class="btn btn-cancel">CANCEL</button><button onclick="agreeFunction()" class="btn">CONFIRM</button></p>
    <p class="checked1"><img src="source/image/content/checked1.jpg">Confirm for this, and all future songs <br/><span>I will publish via Indieplant.</span></p> 
	
	<script type='text/javascript'>
    function agreeFunction() {
        location.href = "{{route('home')}}";
    }
	</script>
    
    </div>
  </div>
</div>

</div>
@endsection