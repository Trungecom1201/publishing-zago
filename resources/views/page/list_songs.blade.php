@extends('master')
@section('content')
    <div class="content content-songs list-song-page">
        <p class="title-content">SONGS</p>
        <p class="btn-warning1 btn-agreement1 btn-publish-song btn-add-song3">
            <button onclick="document.getElementById('id01').style.display='block'"
                    class="btn btn-continue btn-back1 btn-publish-song1">+ ADD SONG
            </button>
        </p>
        <?php  $place_saved = "list_song"; ?>
        @if ( session('song_info') != null )
            <div class="concept-songs concept-songs2">
                <div class="table-songs">
                    <div class="row-title">
                        <div class="color-text20 text-style fleft col-concept">CONCEPT</div>
                        <div class="mobile-show hide-jq">
                            <div class="small-text2 text-style fleft td-composers">COMPOSERS</div>
                            <div class="small-text2 text-style fleft td-lyricists">LYRICISTS</div>
                            <div class="small-text2 text-style fleft td-last-saved">LAST SAVED</div>
                        </div>

                    </div>

                    <div class="row-background">
                        @foreach (Session()->get('song_info') as $key => $val)

                            <form action="saved-song/add" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="name_song" value="<?php echo $val[0][0] ?>"/>
                                <input type="hidden" name="duration_song" value="<?php echo $val[0][1] ?>"/>
                                <input type="hidden" name="contain_sample" value="<?php echo $val[0][2] ?>"/>
                                <input type="hidden" name="right_to_use_sample" value="<?php echo $val[0][3] ?>"/>
                                <input type="hidden" name="remix" value="<?php echo $val[0][4] ?>"/>
                                <input type="hidden" name="prev_registered" value="<?php echo $val[0][5] ?>"/>
                                <input type="hidden" name="which_pro" value="<?php echo $val[0][6] ?>"/>
                                <input type="hidden" name="prs_tunecode" value="<?php echo $val[0][7] ?>"/>
                                <input type="hidden" name="song1" value="<?php echo $val[0][8] ?>"/>
                                <input type="hidden" name="name_random1" value="<?php echo $val[0][9] ?>"/>
                                <input type="hidden" name="song2" value="<?php echo $val[0][10] ?>"/>
                                <input type="hidden" name="name_random2" value="<?php echo $val[0][11] ?>"/>
                                @if($val[0][12] != null)
                                    @foreach ($val[0][12] as $key1 => $val1)

                                        <input type="hidden" name="contributor_id" value="<?php echo $key ?>"/>

                                    @endforeach
                                @endif
                                <div class="row-content">
                                    <div class="background-row1 border-row list-publish fleft col-content">
                                        <div class="padding-text col-concept title-click"><?php echo $val[0][0]; ?></div>
                                        <div class="mobile-show hide-jq">
                                            <div class="padding-zero small-text td-composers">
                                                <span class="text-style style-mobi">composers :</span>
                                                @if($val[0][12] != null)
                                                    @foreach ($val[0][12] as $key2 => $val2)
                                                        @if($val2[2] == "Composer")
                                                            <?php echo substr($val[0][12][$key2][0], 0, 1) . '.' . $val[0][12][$key2][1]; ?>
                                                        @endif
                                                    @endforeach
                                                @endif

                                            </div>
                                            <div class="padding-zero  td-lyricists">
                                                <span class="text-style style-mobi">lyricists :</span>
                                                @if($val[0][12] != null)
                                                    @foreach ($val[0][12] as $key2 => $val2)
                                                        @if($val2[2] == "Lyricist")
                                                            <?php echo substr($val[0][12][$key2][0], 0, 1) . '.' . $val[0][12][$key2][1]; ?>
                                                        @endif
                                                    @endforeach
                                                @endif

                                            </div>
                                            <div class="padding-zero small-text  td-last-saved" class="">
                                                <span class="text-style style-mobi">LAST SAVED :</span>
                                                <?php echo date('d-m-Y'); ?></div>
                                        </div>
                                        <div>
                                            <?php if( !empty($val[0][9]) ){ ?>
                                            <div class="dowload_music ">
                                                <a href="source/music/{{$val[0][9]}}" download="">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="padding-zero play_song1 data-songs-play">

                                            <?php if( !empty($val[0][9]) ){ ?>
                                            <div class="play_music home-play_music"><i class="fa fa-play"></i>
                                                <audio controls=""
                                                       class="play-publish-song1 play-publish-song2 play-publish-song3">
                                                    <source src="source/music/<?php
                                                    echo $val[0][9];
                                                    ?>" type="audio/mpeg">
                                                </audio>
                                            </div>
                                            <?php } else {?> <i class="fa fa-play fa-play-color"></i><?php } ?>

                                        </div>
                                        <div class="padding-zero play_song1 data-submit">
                                            <button
                                                class="btn btn1 btn17 btn-publish-song2 <?php if ($val[0][8] != null) {
                                                    echo "btn-publish-song-color";
                                                } else {
                                                    echo "disabled";
                                                } ?>" onclick="published_songFunction()">
                                                PUBLISH
                                            </button>
                                        </div>
                                    </div>
                                    <div class="padding-zero">
                                        <a href="saved-song/<?php echo $key . '/' . $place_saved; ?>"><i
                                                class="fa fa-pencil fa-pencil2 color-pencil" style=""></i></a>
                                    </div>
                                </div>

                            </form>
                        @endforeach
                    </div>

                </div>
            </div>
        @endif


        <div class="concept-songs5">
            <div class="table-songs">
                <div class="row-title">
                    <div class="color-text20 text-style fleft col-concept song-title">PUBLISHED</div>
                    <div class="mobile-show hide-jq">
                        <div class="small-text2 text-style fleft col-14 song-composer">COMPOSERS</div>
                        <div class="small-text2 text-style fleft col-14 song-lyricists">LYRICISTS</div>
                        <div class="small-text2 text-style fleft song-date">PUBLISH DATE</div>
                    </div>
                </div>
                <div class="row-background">
                    @foreach($songs as $s)
                        <?php $id_song = $s->id;
                        $file_song = $s->file_url;
                        $composer_text = "Composer";
                        $lyricists_text = "Lyricist";
                        $contributor_composer = \DB::table('contributors')->where('id_song', $id_song)->where('role', $composer_text)->get();
                        $contributor_lyricists = \DB::table('contributors')->where('id_song', $id_song)->where('role', $lyricists_text)->get();

                        ?>

                        <div class="background-row2 background-row1 border-row row-3 row-content">
                            <div class="fleft col-content">
                                <div class="padding-text table-mobile song-title title-click">{{$s->song_title}}</div>
                                <div class="mobile-show hide-jq mobi-song-detail">
                                    <div class="padding-zero small-text  table-mobile song-composer">
                                        <span class="text-style style-mobi">COMPOSERS :</span>
                                        @foreach($contributor_composer as $ctc)
                                            {{substr($ctc->first_name, 0, 1).'.'.$ctc->last_name}}
                                        @endforeach
                                    </div>
                                    <div class="padding-zero small-text  table-mobile song-lyricists">
                                        <span class="text-style style-mobi">LYRICISTS :</span>
                                        @foreach($contributor_lyricists as $ctl)
                                            {{substr($ctl->first_name, 0, 1).'.'.$ctl->last_name}}
                                        @endforeach
                                    </div>
                                </div>
                                <div class="table-mobile song-download">
                                    <div class="dowload_music ">
                                        <a href="source/music/<?php echo $file_song; ?>" download><i
                                                class="fa fa-download"></i></a>
                                    </div>
                                </div>

                                <div class="padding-zero publish-date table-mobile song-play">
                                    <?php if( !empty($file_song) ){ ?>
                                    <div class="play_music"><i class="fa fa-play"></i>
                                        <audio controls=""
                                               class="play-publish-song1 play-publish-song2 play-publish-song3">
                                            <source src="source/music/<?php echo $file_song; ?>" type="audio/mpeg">
                                        </audio>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="mobile-show hide-jq mobi-song-date">
                                    <div class="padding-zero small-text table-mobile song-date">
                                        <span class="text-style style-mobi">PUBLISH DATE :</span>
                                        {{date_format($s->created_at,'d-m-Y')}}
                                    </div>
                                </div>
                            </div>
                            <div class="padding-zero table-mobile fleft col-click get-info-claim">
                                <a href="published-song/<?php echo $id_song; ?>" class="btn-published-song2"><i
                                        class="fa fa-eye fa-pencil2 color-pencil color-pencil2 color-pencil3"
                                        style=""></i></a>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>

        <div id="id01" class="modal">
            <!-- Modal Content -->
            <div class="modal-content  modal-content-publish-song modal-saved-claims modal-add-song1 modal-add-song2">
                <form action="list-songs/add" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    @if ( session('song_info') != null )
                        <?php $song3 = Session()->get('song_info');
                        $min_key_song = min(array_keys($song3));
                        ?>
                    @else
                        <?php
                        $min_key_song = null;
                        ?>
                    @endif
                    <input type="hidden" name="id_song" class="id-song-copy" value="<?php echo $min_key_song; ?>">
                    <p class="title-home-warning">
                        COPY EXISTING SONG?
                    </p>
                    <p class="text-home-warning">You can select a song to copy its contributors<br>and song info for
                        your new song.</p>
                    <select class="form-control form-control-pro form-control-pro1 form-copy-song" required id="sel1">
                        @if (!empty($songs))
                            <option value="">Please Select Song</option>
                            @foreach ($songs as $key => $val)
                                <option value="<?php echo $val->id; ?>"><?php echo $val->song_title; ?></option>
                            @endforeach
                        @else
                            <option value="">No Song</option>
                        @endif
                    </select>
                    <p class="btn-choose-file contributor-radio1 contributor-radio2">

                        <input id="composer25" name="check_copy" type="radio" value="Contributors" checked="">
                        <label for="composer25">Contributors</label>
                        <input id="lyricist26" name="check_copy" type="radio" value="Contributors + Song info">
                        <label for="lyricist26">Contributors + Song info</label>


                    </p>
                    <p class="btn-warning1">
                        <span onclick="blankFunction()" class="btn btn-cancel btn-blank">START BLANK</span>
                        <button class="btn list-copy-song" <?php if (session('song_info') == null) {
                            echo "disabled";
                        } ?> >COPY SONG
                        </button>
                    </p>
                </form>
                <script type='text/javascript'>
                    function agreeFunction() {
                        location.href = "{{route('agreement')}}";

                    }

                    function blankFunction() {
                        location.href = "{{route('addsong')}}";
                    }

                    function published_songFunction() {
                        location.href = "{{route('published_song')}}";
                    }

                    var play_music4 = true;
                    var play_music5 = true;
                    var play_music6 = true;
                    var play_music7 = true;

                    function play_pause_music1() {
                        if (play_music4 === true) {
                            //alert($('img.testtest').attr('src'));
                            $('img.test765').attr("src", "source/image/content/pause.jpg");
                            play_music4 = false;
                            //alert($(this).parent().attr("src"));
                            //alert($(this).attr("src"));
                        } else if (play_music4 === false) {
                            $('img.test765').attr("src", "source/image/content/play1.jpg");
                            play_music4 = true;
                        }

                    }

                    function play_pause_music2() {
                        if (play_music5 === true) {
                            //alert($('img.testtest').attr('src'));
                            $('img.test766').attr("src", "source/image/content/pause.jpg");
                            play_music5 = false;
                        } else if (play_music5 === false) {
                            $('img.test766').attr("src", "source/image/content/play1.jpg");
                            play_music5 = true;
                        }

                    }

                    function play_pause_music3() {
                        if (play_music6 === true) {
                            //alert($('img.testtest').attr('src'));
                            $('img.test767').attr("src", "source/image/content/pause.jpg");
                            play_music6 = false;
                        } else if (play_music6 === false) {
                            $('img.test767').attr("src", "source/image/content/play1.jpg");
                            play_music6 = true;
                        }

                    }

                    function play_pause_music4() {
                        if (play_music7 === true) {
                            //alert($('img.testtest').attr('src'));
                            $('img.test768').attr("src", "source/image/content/play1.jpg");
                            play_music7 = false;
                        } else if (play_music7 === false) {
                            $('img.test768').attr("src", "source/image/content/pause.jpg");
                            play_music7 = true;
                        }

                    }

                    function finished_publish_songsFuction() {
                        location.href = "{{route('finished_publish_songs')}}";
                    }

                    function add_songFunction() {
                        location.href = "{{route('addsong')}}";
                    }

                    function edit_songFunction() {
                        location.href = "{{route('edit_song2')}}";
                    }

                    function copy_songFunction() {
                        location.href = "{{route('copy_song')}}";
                    }
                </script>
                <script>
                    // Get the modal
                    var modal = document.getElementById('id01');

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    $('.title-click').click(function () {
                        $(this).parent('.col-content').find($('.mobile-show')).slideToggle();
                    });
                    // $('.row-content .col-concept').click(function () {
                    //     $(this).parent('.col-content').find($('.mobile-show')).slideToggle();
                    // });
                </script>
            </div>
        </div>
    </div>

@endsection
