@extends('master')
@section('content')

<div class="content content-add-song edit-song-pages">
<form action="save-edit-song" method="POST" enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{csrf_token()}}" />
<input type="hidden" name="id_song" value="<?php echo $id_song_saved; ?>" />
<input type="hidden" name="place_saved" value="<?php echo $type_saved; ?>" />
<p class="title-content title-content-agreement">EDIT SONG</p>
<div class="add-song-1">  
  <p class="title-add-song">SONG INFO</p>

    <?php
        $data = Session()->get('song_info');
//        dd($data);
    foreach( $data as $key => $val) {
        if($id_song_saved == $key){
            $name_song1 = $val[0][8];
            $file_song1 = $val[0][9];
            $name_song2 = $val[0][10];
            $file_song2 = $val[0][11];
            $contributors1 = $val[0][12];
              Session()->put('comtributors',$contributors1);
            

        ?>
  <div class="song-info">
    <div class="form-group">
                        <label class="label-title">Title<img src="source/image/content/question.jpg"></label>
                        
                            <input type="text" name="txtNameSong" class="form-control form-title form-title5" placeholder="Title" value="<?php echo $val[0][0]; ?>">
                        
  </div>
  <div class="form-group">
                        <label class="label-title">Duration<img src="source/image/content/question.jpg"></label>
                                         
                           <p class="add-song-duration minute"> <input type="number" name="txtMinute" class="form-control form-title form-duration form-title6" placeholder="0" value="<?php echo $val[0][14]; ?>"><span>min
                           </span></p>
                            
                            <p class="add-song-duration second"><input type="number" name="txtSecond" class="form-control form-title form-duration form-title7" placeholder="0" value="<?php echo $val[0][15]; ?>"><span>sec</span>
                            </p>
                        <div class="clear"></div>
  </div>



<p class="check-agreement check-agreement2"><span class="squaredTwo">
  <input type="checkbox" value="" id="squaredTwo" class="check_contains_sample" onclick="shsel5Function()" name="check_contains_sample" <?php if($val[0][2] == 1) {echo "checked";} ?> />
  <label for="squaredTwo"></label>
</span><span class="color-text-checkbox"> Contains sample(s) <img src="source/image/content/question.jpg"> </span>
</p>
<p class="check-agreement check-agreement2 show-right-sample1 right-use-sample1" style="display: none;"><span class="squaredFive">
  <input type="checkbox" value="" id="squaredFive" class="check_right_sample" name="check_right_sample" @if($val[0][3] == 1) checked value='1' @endif />
  <label for="squaredFive"></label>
</span><span class="color-text-checkbox"> Do you have the right to use the sample(s)? <img src="source/image/content/question.jpg"> </span>
</p>
<p class="check-agreement check-agreement2"><span class="squaredThree">
  <input type="checkbox" value="" id="squaredThree" class="check_remix" name="check_remix" <?php if($val[0][4] == 1) {echo "checked";} ?> />
  <label for="squaredThree"></label>
</span><span class="color-text-checkbox"> Remix <img src="source/image/content/question.jpg"> </span>
</p>
<p class="check-agreement check-agreement2"><span class="squaredFour">
  <input type="checkbox" value="" id="squaredFour" class="check_prev_registered" onclick="shsel1Function()" name="check_prev_registered" <?php if($val[0][5] == 1) {echo "checked";} ?> />
  <label for="squaredFour"></label>
</span><span class="color-text-checkbox"> Previously registered with a PRO <img src="source/image/content/question.jpg"></span>
</p>
<div class="form-group form-group2" hidden="">

  <label class="label-title label-title-2">Which PRO <img src="source/image/content/question.jpg"></label>
  <div class="styleSelect">

  <select class="form-control form-control-pro units" id="sel1" name="sel_songs">
      <option value="BUMA" @if($val[0][6] == 'BUMA') selected @endif >BUMA</option>
      <option value="Stemra" @if($val[0][6] == 'Stemra') selected @endif>Stemra</option>
  </select>
</div>
</div>
<div class="form-group form-group2" hidden="">
                        <label class="label-title">PRS Tunecode<img src="source/image/content/question.jpg"></label>
                        
                            <input type="text" name="PRS_Tunecode" class="form-control form-title" placeholder="PRS Tunecode" value="<?php echo $val[0][7]; ?>">
                        
  </div>

  </div>
    <?php }} ?>
</div>
<div class="add-song-2">
  <p class="title-add-song">CONTRIBUTORS</p>
  <p id="note_contributor" class="" style=""></p>
      <!--  <p id="note_contributor" class="sub-title-add-song" style="">No contributors added yet. Please at least one.</p> -->
    <div class="contributor-total contributor-total2" >

     @if(empty($data[$id_song_saved][0][12]))
        <p id="note_contributor" class="sub-title-add-song" style="">No contributors added yet. Please at least one.</p>
      @else
      <?php $var = 0; count($data[$id_song_saved][0][12]); ?>

        @foreach($data[$id_song_saved][0][12] as $key => $comtributors)
         <div class="contributors id-{{$var}}">
            <span id="teat_asd" hidden=""></span>
            <span class="contributors-name">{{$comtributors[0]}} {{$comtributors[1]}}</span>
            <input type="hidden" name="contributors_firstname[]" value="{{$comtributors[0]}}">
            <input type="hidden" name="contributors_lastname[]" value="{{$comtributors[1]}}">
            <input type="hidden" name="contributors_role[]" value="{{$comtributors[2]}}">
            <input type="hidden" name="contributors_memberpro[]" value="{{$comtributors[3]}}">
            <input type="hidden" name="contributors_promember[]" value="{{$comtributors[4]}}">
            <input type="hidden" name="contributors_cae_number[]" value="{{$comtributors[5]}}">
            <input type="hidden" name="contributors_member_society[]" value="{{$comtributors[6]}}">
            <input type="hidden" name="contributors_mech_society_member[]" value="{{$comtributors[7]}}">
            <input type="hidden" name="contributors_mech_society_number[]" value="{{$comtributors[8]}}">
            <input type="hidden" name="contributors_equal[]" value="0" class="contributors_equal">
            <span class="role">{{$comtributors[2]}}</span>
            <span class="ratio-percent" hidden=""></span>
            <input type="text" name="contributors_equal2[]" class="percent-share2" value="{{$comtributors[9]}}"  placeholder="0">
               <span class="percent-icon1">%</span>
               <i class="fa fa-pencil" data-name2="{{$key}}"></i>
               <i class="fa fa-remove fa-remove2" data-name="{{$key}}" style="font-size:24px; color: red"></i>
               <div class="border"></div>
           </div>
            
           <?php $var += 1?>
        @endforeach

         @if($var > 0)
           <div class="half-past-percent">
               <p class="check-agreement check-contributors">
                  <span class="squaredTwo">  
                  <input type="checkbox"  id="squaredEight" class="@if($val[0][13] == 1)edit-equal @endif" @if($val[0][13] == 1) checked  @endif value="1" name="check">
                  <label for="squaredEight">   </label>  
                  </span>   
                  <span class="equal-share equal-share2">Equal share</span>   
                </p> 
              </div>
            @endif
      @endif

    </div>

 
  <p><div onclick="add_contributorFunction()" class="btn btn-add-contributor btn-add-contributors">+ ADD CONTRIBUTOR</div></p>
  <p class="title-add-song">AUDIO FILES</p>
    <div class="audio-files">
        <div class="box-audio-files">
        <span class="option-audio">Original <img src="source/image/content/question.jpg"></span>

        <?php

        if (( $name_song1 != null) && ($file_song1 != null )){?>
            <div class="box box4">
                <label id="fileLabel" name="file_music1" class="inputfile name-song inputfile-1 add-margin-top name-song-file" data-multiple-caption="{count} files selected" multiple=""><img class ="icon-music"src="source/image/content/music2.jpg"><?php echo $name_song1; ?></label>
                <input type="hidden" value="<?php echo $name_song1; ?>" name="file_music1" class="name-song-original">
                <input type="hidden" value="<?php echo $file_song1; ?>" name="name_random1">
                <input type="hidden" value="<?php echo $file_song1; ?>" data-id="<?php echo $id_song_saved; ?>" name="name_random1" class="id-song-original">
                <div class="play_music"><i class="fa fa-play"></i>
                <audio controls="" class="upload-song-1">
                    <source src="source/music/<?php echo $file_song1; ?>" type="audio/mpeg">
                </audio>
                </div>
                <i class="fa fa-remove song333 song1 song-original  " style="display: block;" onclick="" data-name="<?php echo $file_song1; ?>"></i>
                <img class="listen_music1 song111 song11" src="source/image/content/play1.jpg" style="
        /* float: right; */
        "><i class="fa fa-remove song333 song1"></i>
            </div>
        <?php }else{ ?>
            <div class="box box4">

                <label id="fileLabel" class="inputfile name-song-file inputfile-1 add-margin-top no-name-file" data-multiple-caption="{count} files selected" multiple="" >No file uploaded.</label>
                <div onclick="upload_audioFunction()" class="btn btn-upload-audio5 btn-song1">UPLOAD</div>
                <img class="listen_music1 song111 song11" src="source/image/content/play1.jpg" style="
     /* float: right; */
">
                <i class="fa fa-remove song333 song1"></i>
            </div>
        <?php } ?>
    </div>
        <div class="border"></div>
    <div class="box-audio-files">
        <span class="option-audio">Instrumental <img src="source/image/content/question.jpg"></span>
       <?php if (( $name_song2 != null) && ($file_song2 != null )){?>
            <div class="box box5"><label id="fileLabel" name="file_music2" class="inputfile name-song inputfile-1 add-margin-top" data-multiple-caption="{count} files selected" multiple=""><img class="icon-music" src="source/image/content/music2.jpg"><?php echo $name_song2; ?></label>
                <input type="hidden" value="<?php echo $name_song2; ?>" name="file_music2">
                <input type="hidden" value="<?php echo $file_song2; ?>" name="name_random2">
                <input type="hidden" value="<?php echo $file_song2; ?>" data-id="<?php echo $id_song_saved; ?>" name="name_random1" class="id-song-original">
                <div class="play_music"><i class="fa fa-play"></i>
                <audio controls="" class="upload-song-2"><source src="source/music/<?php echo $file_song2; ?>" type="audio/mpeg"></audio>
                </div>
                <i class="fa fa-remove song333 song1 song-intrumental" style="display: block;" onclick="" data-name="<?php echo $file_song2; ?>"></i><img class="listen_music1 song111 song11" src="source/image/content/play1.jpg" style="
        /* float: right; */
        "><i class="fa fa-remove song333 song1"></i></div>
        <?php }else{ ?>
            <div class="box box5">

                <label id="fileLabel22" class="inputfile name-song-file no-name-file inputfile-1 instrumental-1 instrumental-2" data-multiple-caption="{count} files selected" multiple="">No file uploaded.</label>
                <div onclick="upload_audioFunction2()" class="btn btn-upload-audio5 btn-song2">UPLOAD</div>

                <img class="listen_music1 song111 song22" src="source/image/content/play1.jpg">
                <i class="fa fa-remove song333 song2"></i>
            </div>
        <?php } ?>
    </div>
    </div>
</div>

    <p class="btn-warning1 btn-agreement1 btn-add-song1">
        <button onclick="warningnotcheckFunction()" class="btn btn-continue btn-default-color btn-add-song2 btn-publish-add-song">PUBLISH</button>
        <button class="btn btn-continue btn-add-song2 btn-save-add-song btn-default-color">SAVE</button>
        <a class="btn-edit-cancel" href="./">Cancel</a>
        <button class="delete-song1 btn-edit-delete" onclick="delete_songFunction()" style="
    /* float: left; */
    /* margin-left: 6%; */

">Delete</button>
    </p>
<!-- The Modal -->

<!-- The Modal -->


<div id="id04" class="modal">
  

  <!-- Modal Content -->
  <div class="modal-content modal-upload-song">
  <form id="music_form" action="" method="POST" enctype="multipart/form-data">
  <meta name="csrf-token" content="{{ csrf_token() }}">
    <p class="title-home-warning title-upload-song">
      UPLOAD AUDIO
    </p>
    
    <div class="box box2 box3">
          <input type="file" name="file-2[]" id="file188" title="Choose a video please" onchange="pressed2()"><label id="fileLabel25" class="inputfile name-song-file inputfile-1 add-margin-top inputfile-7 label-box" data-multiple-caption="{count} files selected" multiple="" onclick="upload_audioFunction5()">No file chosen</label><label for="file188"> <span>CHOOSE FILE</span></label>
        </div>

        <button hidden="" class="upload-music-hd">submit</button>

    <p class="text-upload-audio">You can upload an MP3 file with a max size of 20MB. The file name can contain letters, numbers, spaces, underscores(_) and dashes(-).</p>
    <p class="btn-choose-file">      
      <span class="btn btn-upload-audio btn-upload-audio2 upload-music">UPLOAD</span>
      <a onclick="document.getElementById('id04').style.display='none'">Cancel</a>
    </p>
     </form>
  
  
</div>
</div>

<div id="id03" class="modal">
  

  <!-- Modal Content -->
  <div class="modal-content modal-upload-song">
    <form id="music_form_2" action="" method="POST" enctype="multipart/form-data">

    <meta name="csrf-token" content="{{ csrf_token() }}">
  <input type="hidden" name="kem" value="kem" />
    <div class="kem"></div>
    <p class="title-home-warning title-upload-song">
      UPLOAD AUDIO
    </p>
    
    <div class="box box2">
          <input type="file" name="file-2[]" id="file18" title="Choose a video please" onchange="pressed()"><label id="fileLabel23" class="inputfile inputfile-1 add-margin-top inputfile-7 label-box " data-multiple-caption="{count} files selected" multiple="" onclick="upload_audioFunction()">No file chosen</label><label for="file18"> <span>CHOOSE FILE</span></label>
    </div>
    <button hidden="" class="upload-music-hd-2">submit</button>
    <p class="text-upload-audio">You can upload an MP3 file with a max size of 20MB. The file name can contain letters, numbers, spaces, underscores(_) and dashes(-).</p>
    <p class="btn-choose-file">      
      <span class="btn btn-upload-audio btn-upload-audio2 upload-music-2">UPLOAD</span>
      <a onclick="document.getElementById('id03').style.display='none'">Cancel</a>
    </p>
     
  
  </form>
</div>
</div>

<div id="idAddContributor" class="modal">
  <!-- Modal Content -->
  <div class="modal-content modal-add-contributor">
  <form id="userform" method="POST">
  {{ csrf_field() }}
    <p class="title-home-warning title-upload-song">
      ADD CONTRIBUTOR
    </p>
      <input type="hidden" name="idSong" class="id-songinfo" value="<?php echo $id_song_saved; ?>"/>
    <p class="btn-choose-file contributor-radio1">
      
  <input id="new14" name="option18" type="radio" value="New" onclick="add_contributorFunction2()" checked="" />
  <label for="new14" class="add-new-contributor">New</label>
  <input id="new8" name="option18" type="radio" value="Existing" onclick="interfaceExistingFunction()" />
  <label for="new8" class="add-exist-contributor">Existing</label>
  
    </p>

    <div class="form-group form-group6" hidden>
  
  <label class="label-title label-title-2">Which PRO <img src="source/image/content/question.jpg"></label>
  <select class="form-control form-control-pro show-member show-member222 exis-contributors" id="sel1" name="sel1">
      @if(count($contributors) > 0)
          <option value="Please choose">Please choose</option>
          @foreach ($contributors as $key => $val)
              <option data-first_name="{{$val->first_name}}" data-last_name="{{$val->last_name}}" data-role="{{$val->role}}" data-share="{{$val->share}}" data-pro_member_check="{{$val->pro_member_check}}" data-which_pro="{{$val->which_pro}}" data-cae_number="{{$val->cae_number}}" data-member_society_check="{{$val->member_society_check}}" data-mech_society_member="{{$val->mech_society_member}}" data-mech_society_number="{{$val->mech_society_number}}" value="<?php echo $key; ?>">{{$val->first_name}} {{$val->last_name}}</option>
          @endforeach
      @else
          <option value="Please choose">No contributors</option>
      @endif
  </select>
</div>
<div class="member_contributor" hidden>
</div>
    <p class="text-upload-audio">
      <div class="form-group form-group5">
          <label class="label-title">First name<img src="source/image/content/question.jpg"></label>
          <input type="text" name="txtFirstName" class="form-control form-title form-first-name" placeholder="First name">
      </div>
      </p>
    <p class="text-upload-audio">
      <div class="form-group form-group5">
        <label class="label-title">Last name<img src="source/image/content/question.jpg"></label>
        <input type="text" id="test1" name="txtLastName" class="form-control form-title form-last-name" placeholder="Last name">
      </div>
    </p>
<div class="select-option-form">
    <p class="check-agreement check-agreement2  form-group5">
        <span class="squaredTwo">
            <input type="checkbox" value="0" id="squaredSix11" class="member_pro" onclick="shsel6Function()" name="MemberPro">
            <label for="squaredSix11"></label>
        </span>
        <span class="color-text-checkbox"> Member of a PRO <img src="source/image/content/question.jpg"> </span>
    </p>
    <div class="form-group form-group3" hidden>
      <label class="label-title label-title-2">Which PRO <img src="source/image/content/question.jpg"></label>
      <select class="form-control form-control-pro member_pro_which_pro" id="sel2" name="sel2" value="">
        <option>Please choose</option>
          <option>BUMA</option>
          <option>Stemra</option>
      </select>
        <input type="hidden" class="sel2_val" name="sel2_val">
    </div>
    <div class="form-group form-group3" hidden>
        <label class="label-title">CAE number<img src="source/image/content/question.jpg"></label>
        <input type="text" name="txtCAE_Number" class="form-control form-title member_pro_number" placeholder="CAE number">
      </div>
    <p class="check-agreement check-agreement2 form-group5">
        <span class="squaredTwo">
            <input type="checkbox" value="0" id="squaredSeven" class="member_society" onclick="shsel7Function()" name="MemberSociety">
            <label for="squaredSeven"></label>
        </span>
        <span class="color-text-checkbox"> Member of a mechanical society <img src="source/image/content/question.jpg"> </span>
    </p>
    <div class="form-group form-group4" hidden>
      <label class="label-title label-title-2">Which society <img src="source/image/content/question.jpg"></label>
      <select class="form-control form-control-pro member_society_which_pro" id="sel3" name="sel3">
        <option>Please choose</option>
          <option>BUMA</option>
          <option>Stemra</option>
      </select>
        <input type="hidden" class="sel3_val" name="sel3_val">
    </div>
    <div class="form-group form-group4" hidden>
        <label class="label-title">CAE number<img src="source/image/content/question.jpg"></label>
        <input type="text" name="txtSocietyNumber" class="form-control form-title member_society_number" placeholder="CAE number">
      </div>
</div>
<div class="form-group form-group7">
    <label class="label-title label-title-role">Role<img src="source/image/content/question.jpg"></label>
</div>

<p class="btn-choose-file contributor-radio1 contributor-radio3">
  <input id="composer7" name="option2" type="radio" value="Composer" checked="" />
  <label for="composer7">Composer</label>
  <input id="lyricist7" name="option2" type="radio" value="Lyricist" />
  <label for="lyricist7">Lyricist</label>
  <input id="both7" name="option2" type="radio" value="Both" />
  <label for="both7">Both</label>
  </p>
<p class="btn-choose-file btn-add-contributor5 color-a">
  <span id="btnchange-color213" class="btn btn-upload-audio btn-upload-audio2 btn-add-contributor2 btn-save-contributor3">SAVE</span>
    <a class="cancel-contributor" style="margin-left: 10px" onclick="document.getElementById('idAddContributor').style.display='none'">Cancel</a>
</p>
 </form> 
</div>
</div>

    <div id="idEditContributor" class="modal">
        <!-- Modal Content -->
    </div>

<div id="id10" class="modal" >
<!-- Modal Content -->
    <div class="modal-content modal-content-publish-song modal-saved-claims">
    <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
      DELETE SONG?
    </p>
    <p class="text-home-warning">Are you sure you want to delete your song?<br>This action cannot be undone.</p>
    <p class="btn-warning1">
      <button onclick="document.getElementById('id10').style.display='none'" class="btn btn-cancel">CANCEL</button>
      <button onclick="agreeFunction()" data-idsong="<?php echo $id_song_saved ?>" class="btn btn-delete1">CONFIRM</button>
    </p>
    </div>
</div>

<div id="id02" class="modal" >
  <!-- Modal Content -->
  <div class="modal-content modal-content-warning-agreement">
    <p class="btn-ok-warning-agreement">Please confirm that you have the right to use the sample(s) you've used.</p>
    <p class="btn-warning1"><button onclick="document.getElementById('id02').style.display='none'" class="btn">OKAY</button></p>
  </div>
</div>

<div id="id01" class="modal" >
  <!-- Modal Content -->
  <div class="modal-content modal-content-warning-agreement">
    <p class="btn-ok-warning-agreement">Please add a title and contributor to save this song.</p>
    <p class="btn-warning1"><button onclick="document.getElementById('id01').style.display='none'" class="btn">OKAY</button></p>
	<script type='text/javascript'>
    function agreeFunction() {

    }
    function warningnotcheckFunction(){
      if (!$('input.form-title').val()) {
        document.getElementById('id01').style.display='block';
      }else if (!$('input#squaredFive').is(":checked")) {
        document.getElementById('id02').style.display='block';        
      }else{
        location.href = "{{route('publish your song')}}";
      }
  }
  function upload_audioFunction() {
      document.getElementById('id03').style.display='block';
  }
  function upload_audioFunction2() {
      document.getElementById('id04').style.display='block';
  }

 
      function shsel1Function() {
         $(".form-group2").slideToggle();
    
}

function shsel5Function() {
         $("p.right-use-sample1").slideToggle(0.001);
    
}
function shsel6Function() {
         $(".form-group3").slideToggle();
}
function shsel7Function() {
        $(".form-group4").slideToggle();
}

function shsel8Function() {
        $(".form-group300").slideToggle();
}

function add_contributorFunction(){
        document.getElementById('idAddContributor').style.display='block';

}

function add_contributorFunction2(){  
        $(".form-group6").hide();
        $(".form-group5").show();
        if($('.member_contributor').is(':visible')){
            $(".member_contributor").css("display","none");
        }
}
function interfaceExistingFunction(){
      $(".form-group5").hide();
      $(".form-group6").show();
}
window.pressed = function(){
    var a = document.getElementById('file18').value;
    if(a == "")
    {
        fileLabel23.innerHTML = "Choose file";
    }
    else
    {
        var theSplit = a.split('\\');
        fileLabel23.innerHTML = theSplit[theSplit.length-1];
        $(".btn-upload-audio2").css("background-color","#F35A5C");
    }
};

window.pressed2 = function(){
    var d = document.getElementById('file188').value;
    if(d == "")
    {
        fileLabel25.innerHTML = "Choose file";
    }
    else
    {
        var theSplit = d.split('\\');
        fileLabel25.innerHTML = theSplit[theSplit.length-1];
        $(".btn-upload-audio2").css("background-color","#F35A5C");
    }
};

    function shsel2Function() {
    $('#squaredTwo').click(function() {
     $(".form-group2").toggle(this.checked);
    });
    }
    function edit_interfaceExistingFunction() {
      $(".edit_hide").hide();
      $(".form-group60").show();
    }
    function edit_contributorFunction2() {
      $(".form-group60").hide();
      $(".edit_hide").show();
      if($('.member_contributor').is(':visible')){
          $(".member_contributor").css("display","none");
      }
    }
     
 
    // $( ".show-member222" ).change(function() {
    //     var optcontributors = $( ".show-member222 option:selected" ).val();
    //     $.get("exis-contributors/"+optcontributors, function()
    //     {
    //
    //     })
    //         .done(function(data) {
    //             $('.member_contributor').html(data);
    //             $('.member_contributor').css('display','block');
    //         })
    //         .fail(function() {
    //             alert( "error" );
    //         });
    // });

if($("#squaredSix33").is(':checked')){
  $(".form-group3").slideToggle();
}
    $("body").delegate("#squaredEight", "change", function(){
        if($("#squaredEight").is(':checked')){
            var numItems = $('.contributors').length;
            var number_percent = Math.floor((1/numItems)*100);

            $('.ratio-percent').css("display","block");
            $('.ratio-percent').text(number_percent + "%");
            $('.ratio-percent-val').val(number_percent);
            $('.percent-share2').css("display","none");
            $('span.percent-icon1').css("display","none");
            $('.contributors_equal').val(number_percent);
        }else{
            $('.percent-share2').val("");
            $('.percent-share2').css("display","block");
            $('span.percent-icon1').css("display","block");
            $('.ratio-percent-val').val("");
            $('.contributors_equal').val(0);
        }

    });
 
function change_color111Function() {
  if(!$("#squaredSix33").is(':checked')){
  $(".btn-add-contributor2").css("background-color","#AAAAAA");
}else{
 $(".btn-add-contributor2").css("background-color","#F35667"); 
}
} 
 
  $( "#squaredSix33" ).click(function() {
  $(".form-group300").slideToggle();
});
 
$('.form-title7').keyup(function(){ 
if($('.form-title5').val() != "" && $('.form-title6').val() != "" && $('.form-title7').val() != ""){
$(".btn-add-song2").css("background-color","#F35667");
}
else {
  $(".btn-add-song2").css("background-color","#AAAAAA");
}
});
$('.form-title6').keyup(function(){ 
if($('.form-title5').val() != "" && $('.form-title6').val() != "" && $('.form-title7').val() != ""){
$(".btn-add-song2").css("background-color","#F35667");
}
else {
  $(".btn-add-song2").css("background-color","#AAAAAA");
}
});
$('.form-title5').keyup(function(){ 
if($('.form-title5').val() != "" && $('.form-title6').val() != "" && $('.form-title7').val() != ""){
$(".btn-add-song2").css("background-color","#F35667");
}
else {
  $(".btn-add-song2").css("background-color","#AAAAAA");
}
});
    function save_songFunction() {
      
         var b = document.getElementById('file18').value;
    if(b == "")
    {
        fileLabel22.innerHTML = "Choose file";
    }
    else
    {
        var theSplit = b.split('\\');
        fileLabel.innerHTML = theSplit[theSplit.length-1];
        //$(".btn-upload-audio2").css("background-color","#F35A5C");
        $("#id03").hide();
        $(".btn-song1").css("display","none");
        $(".song1").css("display","block");
        $(".song11").css("display","inline");
    }

    
           
          
    }
    function save_songFunction2() {
         var b = document.getElementById('file188').value;
    if(b == "")
    {
        fileLabel22.innerHTML = "Choose file";
    }
    else 
    {
        var theSplit = b.split('\\');
        fileLabel22.innerHTML = theSplit[theSplit.length-1];
        $("#id04").hide();
        $(".btn-song2").css("display","none");
        $(".song2").css("display","block");
        $(".song22").css("display","inline");
      }
    }
    var play_song_upload1 = true;
  $('.audio-files').on('click','img.song11',function(){
    
    if (play_song_upload1 === true){
     $('img.song11').attr("src","source/image/content/pause.jpg");
     play_song_upload1 = false;
   }
     else {
      $('img.song11').attr("src","source/image/content/play1.jpg");
      play_song_upload1 = true;
     }   
  })
  var play_song_upload2 = true;
  $('.audio-files').on('click','img.song22',function(){
    
    if (play_song_upload2 === true){
     $('img.song22').attr("src","source/image/content/pause.jpg");
     play_song_upload2 = false;
   }
     else {
      $('img.song22').attr("src","source/image/content/play1.jpg");
      play_song_upload2 = true;
     }   
  })
  $('.form-last-name').keyup(function(){ 
if($('.form-first-name').val() != "" && $('.form-last-name').val() != ""){
$(".btn-save-contributor3").css("background-color","#F35667");
}
else {
  $(".btn-save-contributor3").css("background-color","#AAAAAA");
}
});
  $('.form-first-name').keyup(function(){ 
if($('.form-first-name').val() != "" && $('.form-last-name').val() != ""){
$(".btn-save-contributor3").css("background-color","#F35667");
}
else {
  $(".btn-save-contributor3").css("background-color","#AAAAAA");
}
});
  $( "#squaredThree" ).change(function() {
  if($("#squaredThree").is(':checked')){
  $( ".check_remix" ).attr('value', 1);
}else{
  $( ".check_remix" ).attr('value', 0);
  }
});
  $( ".check_contains_sample" ).change(function() {
  if($(".check_contains_sample").is(':checked')){
  $( ".check_contains_sample" ).attr('value', 1);
}else{
  $( ".check_contains_sample" ).attr('value', 0);
  }
});
  $( ".check_right_sample" ).change(function() {
  if($(".check_right_sample").is(':checked')){
  $( ".check_right_sample" ).attr('value', 1);
}else{
  $( ".check_right_sample" ).attr('value', 0);
  }
});  
  $( ".check_prev_registered" ).change(function() {
  if($(".check_prev_registered").is(':checked')){
  $( ".check_prev_registered" ).attr('value', 1);
}else{
  $( ".check_prev_registered" ).attr('value', 0);
  }
});
  $( ".member_pro" ).change(function() {
  if($(".member_pro").is(':checked')){
  $( ".member_pro" ).attr('value', 1);
}else{
  $( ".member_pro" ).attr('value', 0);
  }
});
  $( ".member_society" ).change(function() {
  if($(".member_society").is(':checked')){
  $( ".member_society" ).attr('value', 1);
}else{
  $( ".member_society" ).attr('value', 0);
  }
});
    $( ".which_pro1" ).change(function() {
        if($(".which_pro1").is(':checked')){
            $( ".which_pro1" ).attr('value', 1);
        }else{
            $( ".which_pro1" ).attr('value', 0);
        }
    });
    $( "#sel2" ).change(function() {
        var optsel2 = $( "#sel2 option:selected" ).text();
        $(".sel2_val").val(optsel2);
        //alert( optsel2 );
    });
    $( "#sel3" ).change(function() {
        var optsel3 = $( "#sel3 option:selected" ).text();
        $(".sel3_val").val(optsel3);
        //alert( optsel3 );
    });
 
$("body").delegate(".fa-remove2", "click", function(){

  var contributor1 = jQuery(this).data('name');
            var item_class =  $(this);
            $.get("delete-edit-contributor/"+contributor1+"/"+{{$id_song_saved}}, function()
            {

            })
                .done(function(data) {
                    item_class.parents('.contributors').remove();
                    if($('.fa-remove2').length == 0){
                        $('.half-past-percent').remove();
                        $('#note_contributor').after('<p id="" class="sub-title-add-song" style="">No contributors added yet. Please at least one.</p>');
                    }
                })
                .fail(function() {
                    alert( "error" );
                });
});

    $("body").delegate(".fa-pencil", "click", function(){

 var contributor2 = $(this).data('name2');
 var songid = $('.id-songinfo').val();
        $.get("edit-contributor/"+contributor2+"/"+songid, function()
        {

        }).done(function(data) {
                $('#idEditContributor').html(data);
                document.getElementById('idEditContributor').style.display='block';
            if($('#squaredEight').is(':checked')){
                $('#userform11').append('<input type="hidden" name="check_share" value="1"/>');
            }
                $('.btn-edit-contributor').on('click',function(){
                  var contributor_edit =  $(this).parent().parent().parent().data("id");
                    // show that something is loading
                    $.ajax({
                        type: 'POST',
                        url: 'edit-song-contributor/'+songid,
                        data: $("#userform11").serialize()
                    })
                        .done(function(data){
                            $(".id-"+contributor_edit).html(data);
                            $('#idEditContributor').hide();
                            $('.fa-pencil').click(function(){
                                var contributor2 = $(this).data('name2');
                                $.get("edit-contributor/"+contributor2, function()
                                {

                                }).done(function(data) {
                                        $('#idEditContributor').html(data);
                                        document.getElementById('idEditContributor').style.display='block';
                                        $('.btn-edit-contributor').on('click',function(){
                                            var contributor_edit =  $(this).parent().parent().parent().data("id");
                                            $.ajax({
                                                type: 'POST',
                                                url: 'edit-contributor1',
                                                data: $("#userform11").serialize()
                                            })
                                                .done(function(data){
                                                    $(".id-"+contributor_edit).html(data);
                                                    $('#idEditContributor').hide();
                                                })
                                                .fail(function() {
                                                    alert( "Please choose a file to upload." );
                                                });
                                            return false;

                                        });

                                    })
                                    .fail(function() {
                                        alert( "error" );
                                    });
                            });
                        })
                        .fail(function() {
                            alert( "Please choose a file to upload." );
                        });
                    return false;

                });

            })
            .fail(function() {
                alert( "error" );
            });
});

	</script>
  
  <script type="text/javascript">
        $(document).ready(function(){
            $('#btnchange-color213').on('click',function(){
                if($('input[name="txtFirstName"]').val() != '' ){
                    $.ajax({
                        type: 'POST',
                        url: 'save-contributor',
                        data: $("#userform").serialize()
                    })
                        .done(function(data){
                            // show the response
                            $('.contributors').remove();
                            $('.half-past-percent').remove();
                            if($('.sub-title-add-song').length > 0) {
                                $('.sub-title-add-song').css('display', 'none');
                            }
                            $('#note_contributor').html(data);

                            $('#squaredEight').change(function() {
                                if($("#squaredEight").is(':checked')){
                                    var numItems = $('.contributors').length;
                                    var number_percent = Math.floor((1/numItems)*100);

                                    $('.ratio-percent').css("display","block");
                                    $('.ratio-percent').text(number_percent + "%");
                                    $('.ratio-percent-val').val(number_percent);
                                    $('.percent-share2').css("display","none");
                                    $('span.percent-icon1').css("display","none");
                                    $('.contributors_equal').val(number_percent);
                                }else{
                                    $('.percent-share2').val("");
                                    $('.percent-share2').css("display","block");
                                    $('span.percent-icon1').css("display","block");
                                    $('.ratio-percent-val').val("");
                                    $('.contributors_equal').val(0);
                                }

                            });

                            $('#idAddContributor').hide();
                            // $('.fa-remove2').click(function(){
                            //     var contributor1 = jQuery(this).data('name');
                            //     var item_class =  $(this);
                            //     $.get("delete-edit-contributor/"+contributor1, function()
                            //     {
                            //
                            //     })
                            //         .done(function(data) {
                            //             item_class.parents('.contributors').remove();
                            //
                            //         })
                            //         .fail(function() {
                            //             alert( "error" );
                            //         });
                            //
                            //
                            // });


                        })
                        .fail(function() {
                            // just in case posting your form failed
                            alert( "Please choose a file to upload." );
                        });

                }

                // to prevent refreshing the whole page page
                return false;
            });
        });
    </script>



 <script type="text/javascript">
 

 

    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('body').delegate('.song-original', 'click',function(){
                var name_music = $(this).data('name');
                $.get("delete-file/"+name_music, function()
                {

                }).done(function(data) {
                        $('.box4').html(data);
                    }).fail(function() {
                        alert( "error" );
                    });
            });
            $('body').delegate('.song-intrumental', 'click',function(){
                name_music = $(this).data('name');
                $.get("delete-file2/"+name_music, function()
                {

                })
                    .done(function(data) {
                        $('.box5').html(data);
                    })
                    .fail(function() {
                        alert( "error" );
                    });
            });

 $('.upload-music-2').on('click',function(){
                // show that something is loading
                var name = document.getElementById('file18');
                  var data = new FormData();
                  for(var i = 0; i < name.files.length; i++){
               data.append('file_song'+i, name.files[i]);
              console.log(data);
              }
              
                    $.ajax({
               headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
                url:"add-file",
                method:"POST",
                data:data
                ,
                cache: false,
                        contentType: false,
                        processData: false,

                
              }).done(function(data){
                    $('#id03').hide();
                     $('.box4').html(data);
                        function originalFunction()
                        {
                            var name_music = $('.upload-song-1').children().attr('src').substring(13);
                            $.get("delete-file/"+name_music, function()
                            {

                            })
                                .done(function(data) {
                                    $('.box4').html(data);
                                })
                                .fail(function() {
                                    alert( "error" );
                                });

                        }

                })
                .fail(function() {
                    // just in case posting your form failed
                    alert( "Please choose a file to upload." );
                });
                // to prevent refreshing the whole page page
              
            });
     });

         
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
 $('.upload-music').on('click',function(){
                // show that something is loading
                var name = document.getElementById('file188');
                  var data = new FormData();
                  for(var i = 0; i < name.files.length; i++){
               data.append('file_song2'+i, name.files[i]);
              }
              
                    $.ajax({
               headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
                url:"add-file2",
                method:"POST",
                data:data
                ,
                cache: false,
                        contentType: false,
                        processData: false,

                
              }).done(function(data){
                     $('.box5').html(data);
                        $('#id04').hide();
                        function instrumentalFunction()
                        {
                            name_music = $('.upload-song-2').children().attr('src').substring(13);
                            $.get("delete-file2/"+name_music, function()
                            {

                            })
                                .done(function(data) {
                                    $('.box5').html(data);
                                })
                                .fail(function() {
                                    alert( "error" );
                                });

                        }
                })
                .fail(function() {
                    // just in case posting your form failed
                    alert( "Please choose a file to upload." );
                });
                // to prevent refreshing the whole page page
              
            });
     });
    </script>
    <script type="text/javascript">

       $('.remove-original').click(function(){
           var id_song_original = $('.id-song-original').data('id');
           $.get("delete-music/"+id_song_original, function()
           {

           })
               .done(function(data) {
                   $('.box4').html(data);
               })
               .fail(function() {
                   alert( "error" );
               });


       });

       $('.remove-instrumental').click(function(){
           var id_song_original = $('.id-song-original').data('id');
           $.get("delete-music2/"+id_song_original, function()
           {

           })
               .done(function(data) {
                   $('.box5').html(data);
               })
               .fail(function() {
                   alert( "error" );
               });


       });
     
    </script>

      <script type="text/javascript">
          if ($('#squaredTwo').is(':checked')) {
              $("p.show-right-sample1").slideDown(0.001);
              $("#squaredFive").attr("checked",1);
          }
          if ($('#squaredFour').is(':checked')) {
              $(".form-group2").slideDown(0.001);
          }
      </script>

<script type="text/javascript">
function delete_songFunction() {
    event.preventDefault();
    event.stopPropagation();
      document.getElementById('id10').style.display='block';
    }
    $('.btn-delete1').click(function(){
                        var song1 = jQuery(this).data('idsong');
                        $.get("delete-song1/"+song1, function()
                        {
                        })
                            .done(function(data) {
                              window.location.href = window.location.pathname = "saved-song";
                            })
                            .fail(function() {
                                alert( "error" );
                            });


                    });
     $(function () {
    $('input[type=file]').change(function () {
        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(mp3|mp4)$");

        if (!(regex.test(val))) {
            $(this).val('');
            $(this).next().text('No file chosen');
            alert('Please select correct file format');
        }
    });
}); 
</script> 

<script type="text/javascript">
$( ".percent-share2" ).keyup(function() {
  $(".contributors_equal").val($( ".percent-share2" ).val()); 
});     
</script>

    </div>
  </div>
  

</form>
</div>
<div id="checkright-sample" class="modal" >
    <div class="modal-content modal-content-warning-agreement">
        <p class="btn-ok-warning-agreement">Please confirm that you have the right to use the sample(s) you've used.</p>
        <p class="btn-warning1"><button onclick="document.getElementById('checkright-sample').style.display='none'" class="btn">OKAY</button></p>
    </div>
</div>
<div id="check-song-contributor" class="modal" >
    <div class="modal-content modal-content-warning-agreement">
        <p class="btn-ok-warning-agreement">Please add a title and contributor to save this song.</p>
        <p class="btn-warning1"><button onclick="document.getElementById('check-song-contributor').style.display='none'" class="btn">OKAY</button></p>
    </div>
</div>
@endsection
