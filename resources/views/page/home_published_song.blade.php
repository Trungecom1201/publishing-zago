@extends('master')
@section('content')
    <?php
    //$arr_str;
    if(Session()->get('song_info') != null){
    $key1 = max(array_keys(Session()->get('song_info')));
    foreach (Session()->get('song_info') as $key => $val) {

        if($key == $key1){
            $val1 = $val[0][0];
            $val2 = $val[0][1];
            $val3 = $val[0][2];
            $val4 = $val[0][3];
            $val5 = $val[0][4];
            $val6 = $val[0][5];
            $val7 = $val[0][6];
            $val8 = $val[0][7];
            $val9 = $val[0][8];
            $val10 = $val[0][9];
            $val11 = $val[0][10];
            $val12 = $val[0][11];
            $val13 = $val[0][12];
            $val14 = $val[0][13];
        }

    }
    };
    ?>
    <div class="content content-agreement content-publish-song page-publish-song-item">
        <p class="title-content title-content-agreement">PUBLISHED SONG</p>
        <p class="note-before-read publish-song-title">Publish date: {{date_format($details_song->created_at,'d-m-Y')}}</p>
        <div class="parent-publish-song-info">
            <p class="title-publish-song title-song-info2">SONG INFO</p>
            <div class="publish-song-info">
                <div class="publish-song-info1 padding-for-title-publish-song">
                    <p class="title-option-publish-song">Title</p>
                    <p class="content-option-publish-song">{{$details_song->song_title}}</p>
                </div>
                <div class="publish-song-info2 padding-for-title-publish-song border-publish-song">
                    <p class="title-option-publish-song">Duration</p>
                    <p class="content-option-publish-song">{{$details_song->duration}}</p>
                </div>
                <div class="publish-song-info3 padding-for-title-publish-song">
                    <p class="option-publish-song1"><?php if( $details_song->contains_samples == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Contains Sample(s)</p>
                    <p class="option-publish-song1"><?php if( $details_song->rights_to_use_samples == "1" ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Do you have the right to use the sample(s)?</p>
                </div>
                <div class="publish-song-info4 padding-for-title-publish-song border-publish-song padding-special">
                    <p class="option-publish-song1"><?php if( $details_song->remix == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Remix</p>
                </div>
                <div class="publish-song-info5 padding-for-title-publish-song">
                    <p class="option-publish-song1 margin-bottom-table"><?php if( $details_song->prev_registered_with_pro == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Previously registered with a PRO</p>
                    <?php if( $details_song->prev_registered_with_pro == 1 ) { ?>
                    <table border="0px" width="350px" height="50px">
                        <tr class="change-color-row">
                            <td class="padding-zero">which PRO</td>
                            <td class="padding-zero">PRS TurnCode</td>

                        </tr>
                        <tr>
                            <td class="padding-zero"><?php echo $details_song->which_pro; ?></td>
                            <td class="padding-zero"><?php echo $details_song->prs_tunecode; ?></td>

                        </tr>

                    </table>
                    <?php } ?>
                </div>
                <div class="publish-song-info6 padding-for-title-publish-song border-publish-song publish-song1">
                    <p class="title-option-publish-song">Audio file (original)</p>
                    <?php if( $details_song->audio_original != null ){?><p class="content-option-publish-song name-text-music"><img src="source/image/content/music2.jpg"> <?php echo $details_song->audio_original; ?><span class="size-song">7.511 KB</span></p><?php }else{ ?>There are no songs<?php } ?>
                    <?php if( $details_song->file_url != null ){?>
                    <div class="dowload_music publish-song-dowload_music"><a href="source/music/<?php echo $details_song->file_url; ?>" download><i class="fa fa-download"></i></a></div>
                    <div class="play_music publish-song-play_music"><i class="fa fa-play"></i>
                    <audio controls="" class="play-publish-song1"><source src="source/music/<?php echo $details_song->file_url; ?>" type="audio/mpeg"></audio>
                    </div>
                        <?php } ?>
                </div>
                <div class="publish-song-info7 padding-for-title-publish-song publish-song1">
                    <p class="title-option-publish-song">Audio file (instrumental)</p>
                    <?php if( $details_song->audio_instrumental != null ){?><p class="content-option-publish-song name-text-music"><img src="source/image/content/music2.jpg"> <?php echo $details_song->audio_instrumental; ?><span class="size-song">7.290 KB</span></p><?php }else{ ?>There are no songs<?php } ?>
                    <?php if( $details_song->file_url_instrumental != null ){?>
                    <div class="dowload_music publish-song-dowload_music"><a href="source/music/<?php echo $details_song->file_url_instrumental; ?>" download><i class="fa fa-download"></i></a></div>
                    <div class="play_music publish-song-play_music"><i class="fa fa-play"></i>
                    <audio controls="" class="play-publish-song1"><source src="source/music/<?php echo $details_song->file_url_instrumental; ?>" type="audio/mpeg"></audio>
                    </div>
                        <?php } ?>
                </div>
            </div>
        </div>
        <div class="parent-publish-contributor">
            <p class="title-publish-song title-publish-contributor">CONTRIBUTORS</p>
            <div class="publish-contributor"><?php
                foreach ($contributor_composer as $ctc) {
                ?>
                <div class="publish-contributor1 padding-for-title-publish-song">
                    <div class="publish-contributor-sub1 publish-contributor-sub">
                        <p class="title-option-publish-song">Name</p>
                        <p class="content-option-publish-song contributors-details">{{$ctc->first_name.' '.$ctc->last_name}}</p>
                        <p class="title-option-publish-song">Role</p>
                        <p class="content-option-publish-song contributors-details">{{$ctc->role}}</p>
                        <p class="title-option-publish-song">Share</p>
                        <p class="content-option-publish-song">{{$ctc->share}}%</p>
                    </div>
                    <div class="publish-contributor-sub2 publish-contributor-sub">
                        <p class="option-publish-song1 margin-bottom-table"><?php if( $ctc->pro_member_check == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Member of a PRO</p>
                        <?php if( $ctc->pro_member_check == 1 ) { ?>
                        <table border="0px" width="250px" height="50px">
                            <tbody><tr class="change-color-row">
                                <td class="padding-zero">which PRO</td>
                                <td class="padding-zero">CAE Number</td>

                            </tr>
                            <tr>
                                <td class="padding-zero">{{$ctc->which_pro}}</td>
                                <td class="padding-zero">{{$ctc->cae_number}}</td>

                            </tr>

                            </tbody></table>
                        <?php } ?>
                        <p class="option-publish-song1 margin-bottom-table contributors-details3"><?php if( $ctc->member_society_check == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Member of a mechanical society</p>
                        <?php if( $ctc->member_society_check == 1 ) { ?>
                        <table border="0px" width="250px" height="50px">
                            <tbody><tr class="change-color-row">
                                <td class="padding-zero">which PRO</td>
                                <td class="padding-zero">CAE Number</td>

                            </tr>
                            <tr>
                                <td class="padding-zero">{{$ctc->mech_society_member}}</td>
                                <td class="padding-zero">{{$ctc->mech_society_number}}</td>

                            </tr>

                            </tbody></table>
                        <?php } ?>
                    </div>
                </div>
                <?php
                }
                //}

                //}
                ?>


                <div class="publish-contributor3 padding-for-title-publish-song padding-special">
                    <p class="option-publish-song1"><?php if( $details_song->equal_shares == 'No' ){?><img src="source/image/content/unticker.jpg"><?php }else {?><img src="source/image/content/ticker1.jpg"><?php } ?>Equal shares</p>
                </div>
            </div>
        </div>
        <p class="btn-warning1 btn-agreement1 btn-publish-song page-check-publish">
            <button onclick="backFunction()" class="btn btn-continue btn-back1 btn-publish-song1">BACK</button>
        </p>
        <!-- The Modal -->
        <div id="id01" class="modal" >
            <!-- Modal Content -->
            <div class="modal-content modal-content-publish-song">
                <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
                    ARE YOU SURE?
                </p>
                <p class="text-home-warning">Please make sure everything is correct.<br>Your song will now be published.</p>
                <p class="btn-warning1">
                    <button onclick="document.getElementById('id01').style.display='none'" class="btn btn-cancel">CANCEL</button>
                    <button onclick="agreeFunction()" class="btn">CONFIRM</button>
                </p>
                <p class="check-agreement check-agreement5"><span class="squaredTwo">
  <input type="checkbox" value="None" id="squaredSixtys" name="check">
  <label for="squaredSixtys"></label>
</span><span class="text-publish-song"> Don't show this message again. </span></p>






                <script type='text/javascript'>
                    function agreeFunction() {
                        location.href = "{{route('savedsong')}}";
                    }
                    function warningnotcheckFunction(){
                        if ($('input#squaredTwo').is(':checked')) {
                            location.href = "{{route('addsong')}}";
                        }
                        else{
                            document.getElementById('id01').style.display='block';
                        }
                    }
                    var play_music1 = true;
                    $('.publish-song-info6').on('click','img',function(){

                        if (play_music1 === true){
                            //alert($('img.testtest').attr('src'));
                            $('img.listen_music1').attr("src","source/image/content/pause.jpg");
                            play_music1 = false;
                        }
                        else {
                            $('img.listen_music1').attr("src","source/image/content/play1.jpg");
                            play_music1 = true;
                        }
                    })
                    var play_music2 = true;
                    $('.publish-song-info7').on('click','img',function(){

                        if (play_music2 === true){
                            //alert($('img.testtest').attr('src'));
                            $('img.listen_music2').attr("src","source/image/content/play1.jpg");
                            play_music2 = false;
                        }
                        else {
                            $('img.listen_music2').attr("src","source/image/content/pause.jpg");
                            play_music2 = true;
                        }
                    })
                    function backFunction() {
                        location.href = "{{route('listsongs')}}";
                        // alert("Agree");


                    }
                </script>

            </div>
        </div>
    </div>

    </div>
@endsection