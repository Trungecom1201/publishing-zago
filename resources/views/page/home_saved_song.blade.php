@extends('master')
@section('content')
    <?php
    //    dd(session()->all());
    if ($new_song != null) {
        $new_song2 = $new_song->song_title;
        $new_song3 = strtotime($new_song->created_at);
        $date = strtotime(date('d-m-Y', $new_song3));
        $date2 = strtotime(date('d-m-Y'));
        $date3 = ($date2 - $date);
        $days = $date3 / 86400;
        if ($days == 0) {
            $days_extend = "TODAY";
        } elseif ($days == 1) {
            $days_extend = "YESTERDAY";
        } elseif (($days <= 6)) {
            $days_extend = $days . ' ' . 'DAYS AGO';
        } elseif (($days >= 7) && ($days < 30)) {
            $weeks = round($days / 7, 0, PHP_ROUND_HALF_DOWN);
            if ($weeks == 1) {
                $days_extend = $weeks . ' ' . 'WEEK AGO';
            } else {
                $days_extend = $weeks . ' ' . 'WEEKS AGO';
            }
        } elseif (($days >= 30) && ($days < 365)) {
            $months = round($days / 30, 0, PHP_ROUND_HALF_DOWN);
            if ($months == 1) {
                $days_extend = $months . ' ' . 'MONTH AGO';
            } else {
                $days_extend = $months . ' ' . 'MONTHS AGO';
            }
        } elseif ($days == 365) {
            $days_extend = "1 YEAR AGO";
        } else {
            $days_extend = "OVER A YEAR AGO";
        }
    }
    //echo $days_extend;
    ?>
    <div class="content content-saved-song page-home-song">
        <p class="title-content">PUBLISHING</p>
        <div class="group-content-option">
            <div
                class="content-option content-option-1 <?php if($count_song > 0){?> content-option-published1 <?php }?>">
                <p class="title-option">PUBLISHED</p>
                <?php if($count_song > 0){?>
                <p>
                    <button class="btn btn-published-song"
                            onclick="list_songsFunction()"><?php echo $count_song; ?></button>
                </p>
                <p class="note1 @if($count_song < 0)info1 @else home-last-song @endif">LAST SONG</p>
                <p class="note1 home-title-song"><?php echo strtoupper($new_song2); ?></p>
                <p class="note1 home-song-day"><?php echo $songs_time; ?></p> <?php }else{?>
                <img src="source/image/content/music1.jpg">

                <p>
                    @if(Auth::check())
                        <button onclick="document.getElementById('id01').style.display='block'" class="btn empty-song">+
                            ADD SONG
                        </button>
                    @else
                        <button onclick="window.location.href = 'login'" class="btn">+ ADD SONG</button>

                    @endif
                </p>

                <p class="note1">YOU HAVEN'T PUBLISHED ANY SONGS YET</p>

                <?php } ?>
            </div>
            <div class="content-option content-claims <?php if($count_song > 0){?> content-option-published2 <?php }?>">
                <p class="title-option">BROADCAST CLAIMS</p>

                @if($count_claim == null)
                    <img @if(Auth::check()) class="login-home-icon-claim"
                         @endif   src="source/image/content/movie1.jpg">
                    <p>
                        <button class="btn btn1<?php if($count_song > 0){?> btn-publish <?php }else {
                            echo " disabled";
                        } ?>">+ ADD CLAIM
                        </button>
                    </p>
                    @if($count_song <= 0)
                        <p class="note1">PLEASE FIRST PUBLISH A SONG</p>
                    @endif
                    @if(Auth::check())
                        @if($count_claim <= 0 && $count_song >= 1)
                            <p class="home-claim-text">you haven’t submitted any claims yet</p>
                        @endif
                    @endif
                @else
                    <div class="home-count-claim">
                        <button class="btn btn-published-song"
                                onclick="window.location.href ='claims'">{{$count_claim}}</button>
                        <p class="note1  home-last-song ">LAST SONG</p>
                        <p class="note1 home-title-song">{{$new_claim->song_title}}</p>
                        <p class="note1 home-song-day">{{$claim_time}}</p>
                    </div>
                @endif
            </div>
        </div>
        <div class="clear"></div>
        @if ( session('song_info') != null )
            <div class="concept-songs">
                <table width="99%" height="50px" class="">
                    <tbody>
                    <tr>
                        <th class="color-text20 song">CONCEPT SONGS</th>
                        <th class="small-text2 composers">COMPOSERS</th>
                        <th class="small-text2 lyricists">LYRICISTS</th>
                        <th class="small-text2 last-saved">LAST SAVE</th>
                        <th class="icon-play-song"></th>
                        <th class="btn-edit"></th>
                    </tr>

                    @foreach (Session()->get('song_info') as $key => $val)
                        <?php $place_saved = "home_saved"; ?>
                        <form action="saved-song/add" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <input type="hidden" name="song_id" value="<?php echo $key ?>"/>
                            <input type="hidden" name="name_song" value="<?php echo $val[0][0] ?>"/>
                            <input type="hidden" name="duration_song" value="<?php echo $val[0][1] ?>"/>
                            <input type="hidden" name="contain_sample" value="<?php echo $val[0][2] ?>"/>
                            <input type="hidden" name="right_to_use_sample" value="<?php echo $val[0][3] ?>"/>
                            <input type="hidden" name="remix" value="<?php echo $val[0][4] ?>"/>
                            <input type="hidden" name="prev_registered" value="<?php echo $val[0][5] ?>"/>
                            <input type="hidden" name="which_pro" value="<?php echo $val[0][6] ?>"/>
                            <input type="hidden" name="prs_tunecode" value="<?php echo $val[0][7] ?>"/>
                            <input type="hidden" name="song1" value="<?php echo $val[0][8] ?>"/>
                            <input type="hidden" name="name_random1" value="<?php echo $val[0][9] ?>"/>
                            <input type="hidden" name="song2" value="<?php echo $val[0][10] ?>"/>
                            <input type="hidden" name="name_random2" value="<?php echo $val[0][11] ?>"/>
                            <input type="hidden" name="equal" value="<?php echo $val[0][13] ?>"/>
                            <tr class="concept-song list-publish">
                                <td class="padding-text"><?php echo $val[0][0]; ?></td>
                                <td class="padding-zero small-text td-composers">
                                    @if($val[0][12] != null)
                                        @foreach ($val[0][12] as $key2 => $val2)
                                            @if($val2[2] == "Composer")
                                                <?php echo substr($val[0][12][$key2][0], 0, 1) . '.' . $val[0][12][$key2][1]; ?>
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                                <td class="padding-zero td-lyricists">
                                    @if($val[0][12] != null)
                                        @foreach ($val[0][12] as $key2 => $val2)
                                            @if($val2[2] == "Lyricist")
                                                <?php echo substr($val[0][12][$key2][0], 0, 1) . '.' . $val[0][12][$key2][1]; ?>
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                                <td class="padding-zero small-text" class=""><?php echo date('d-m-Y'); ?></td>
                                <td>
                                    <?php if( !empty($val[0][9]) ){ ?>
                                    <div class="play_music home-play_music"><i class="fa fa-play"></i>
                                        <audio controls="" class="play-publish-song1 play-publish-song2">
                                            <source src="source/music/<?php
                                            echo $val[0][9];
                                            ?>" type="audio/mpeg">
                                        </audio>
                                    </div>
                                    <?php } else {?>
                                    <div class="play_music home-play_music play_hiden"><i class="fa fa-play"></i>
                                    </div><?php } ?>
                                </td>
                                <td class="padding-zero play_song1 home-td-content">

                                    <button class="btn btn1 btn17 btn-publish-song2 <?php if ($val[0][8] != null) {
                                        echo "btn-publish-song-color";
                                    } else {
                                        echo "disabled";
                                    } ?>" onclick="published_songFunction()">
                                        PUBLISH
                                    </button>
                                </td>
                                <td class="padding-zero home-td-edit"><a
                                        href="saved-song/<?php echo $key . '/' . $place_saved; ?>"><i
                                            class="fa fa-pencil fa-pencil2" style=""></i></a></td>
                            </tr>
                        </form>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
        @if ( session('song_info') != null )
            <div class="concept-songs-container">
                <div class="concept-songs-row ">
                    <div class="concept-songs-title songs-title-row">
                        concept songs
                    </div>
                    <div class="mobile-show hide-jq">
                        <div class="concept-songs-composers songs-title-row">
                            composers
                        </div>
                        <div class="concept-songs-lyricists songs-title-row">
                            lyricists
                        </div>
                        <div class="concept-songs-last-save songs-title-row">
                            last save
                        </div>
                    </div>
                </div>
                <div class="row-background">
                    @foreach (Session()->get('song_info') as $key => $val)
                        <div class="concept-songs-data first-child-row">
                            <div class="align-content-center">
                                <form action="saved-song/add" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="song_id" value="<?php echo $key ?>"/>
                                    <input type="hidden" name="name_song" value="<?php echo $val[0][0] ?>"/>
                                    <input type="hidden" name="duration_song" value="<?php echo $val[0][1] ?>"/>
                                    <input type="hidden" name="contain_sample" value="<?php echo $val[0][2] ?>"/>
                                    <input type="hidden" name="right_to_use_sample" value="<?php echo $val[0][3] ?>"/>
                                    <input type="hidden" name="remix" value="<?php echo $val[0][4] ?>"/>
                                    <input type="hidden" name="prev_registered" value="<?php echo $val[0][5] ?>"/>
                                    <input type="hidden" name="which_pro" value="<?php echo $val[0][6] ?>"/>
                                    <input type="hidden" name="prs_tunecode" value="<?php echo $val[0][7] ?>"/>
                                    <input type="hidden" name="song1" value="<?php echo $val[0][8] ?>"/>
                                    <input type="hidden" name="name_random1" value="<?php echo $val[0][9] ?>"/>
                                    <input type="hidden" name="song2" value="<?php echo $val[0][10] ?>"/>
                                    <input type="hidden" name="name_random2" value="<?php echo $val[0][11] ?>"/>
                                    <input type="hidden" name="equal" value="<?php echo $val[0][13] ?>"/>
                                    <input type="submit" class="publish-hidden-button"
                                           style="display:none" <?php if ($val[0][8] == null) {
                                        echo "disabled";
                                    } ?>/>
                                </form>
                                <div class="data-songs-title data-text">{{$val[0][0]}}</div>
                                <div class="mobile-show hide-jq">
                                    <div class="data-songs-composers data-text">
                                        <span class="text-style style-mobi">composers :</span>
                                        @if($val[0][12] != null)
                                            @foreach ($val[0][12] as $key2 => $val2)

                                                @if($val2[2] == "Composer")
                                                    <?php echo substr($val[0][12][$key2][0], 0, 1) . '.' . $val[0][12][$key2][1]; ?>
                                                @endif
                                            @endforeach
                                        @endif

                                    </div>
                                    <div class="data-songs-lyricists data-text">
                                        <span class="text-style style-mobi">lyricists :</span>
                                        @if($val[0][12] != null)
                                            @foreach ($val[0][12] as $key2 => $val2)
                                                @if($val2[2] == "Lyricist")
                                                    <?php echo substr($val[0][12][$key2][0], 0, 1) . '.' . $val[0][12][$key2][1]; ?>
                                                @endif
                                            @endforeach
                                        @else
                                            {{'-'}}
                                        @endif
                                    </div>
                                    <div class="data-songs-last-save data-text">
                                        <span class="text-style style-mobi">last save :</span>
                                        {{date('d-m-Y')}}
                                    </div>
                                </div>
                                <div class="data-songs-play">
                                    <?php if( !empty($val[0][9]) ){ ?>
                                    <div class="play_music home-play_music"><i class="fa fa-play"></i>
                                        <audio controls="" class="play-publish-song1 play-publish-song2">
                                            <source src="source/music/<?php
                                            echo $val[0][9];
                                            ?>" type="audio/mpeg">
                                        </audio>
                                    </div>
                                    <?php } else {?>
                                    <div class="play_music home-play_music play_hiden"><i class="fa fa-play"></i></div>
                                    <?php } ?>
                                </div>
                                <div class="data-claim-submit data-submit">
                                    <a class="bth-data-submit <?php if ($val[0][8] != null) {
                                        echo "btn-publish-song-color";
                                    } else {
                                        echo "bth-data-disable";
                                    } ?>">Publish</a>
                                </div>
                            </div>
                            <div class="data-claim-edit">
                                <a href="saved-song/<?php echo $key . '/' . $place_saved; ?>"><i
                                        class="fa fa-pencil"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        @if(!empty($claim_publish))
            @if(count($claim_publish) >  0)
                <div class="concept-claim-container">
                    <div class="concept-claim-row ">
                        <div class="concept-claim-title title-row">
                            concept claims
                        </div>
                        <div class="mobile-show hide-jq">
                            <div class="concept-claim-medium title-row">
                                Medium
                            </div>
                            <div class="concept-claim-station_channel title-row">
                                Station/channel
                            </div>
                            <div class="concept-claim-program title-row">
                                program
                            </div>
                            <div class="concept-claim-air-date title-row">
                                air date
                            </div>
                            <div class="concept-claim-county title-row">
                                country
                            </div>
                        </div>
                    </div>
                    <div class="row-background">
                        @foreach($claim_publish as $key => $claims)
                            <div class="concept-claim-data @if($key == 0) first-child-row @endif">
                                <div class="align-content-center">
                                    <div class="data-claim-title data-text song-title-click">
                                        {{$claims->song_title}}
                                    </div>
                                    <div class="mobile-show hide-jq">
                                        <div class="data-claim-medium data-text">
                                            <span class="text-style style-mobi">Medium :</span>
                                            @if($claims->radio_tv == 1)
                                                Radio
                                            @else
                                                TV
                                            @endif
                                        </div>
                                        <div class="data-claim-station_channel data-text">
                                            <span class="text-style style-mobi">Station/channel :</span>
                                            @if($claims->station_channel)
                                                {{$claims->station_channel}}
                                            @else
                                                -
                                            @endif
                                        </div>
                                        <div class="data-claim-program data-text">
                                            <span class="text-style style-mobi">program :</span>
                                            @if($claims->program)
                                                {{$claims->program}}
                                            @else
                                                -
                                            @endif
                                        </div>
                                        <div class="data-claim-air-date data-text">
                                            <span class="text-style style-mobi">air date :</span>
                                            {{date('d-m-Y', strtotime($claims->air_date))}}
                                        </div>
                                        <div class="data-claim-county data-text">
                                            <span class="text-style style-mobi">country :</span>
                                            @if($claims->country)
                                                {{$claims->country}}
                                            @else
                                                -
                                            @endif
                                        </div>
                                    </div>
                                    <div class="data-claim-submit data-submit">
                                        @if($claims->station_channel == '' || $claims->program == '' || $claims->country == '' )
                                            <a class="bth-data-submit bth-data-claim-submit bth-data-disable">Submit</a>
                                        @else
                                            <a class="bth-data-submit bth-data-claim-submit" data-id="{{$claims->id}}">Submit</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="data-claim-edit">
                                    <a href="edit-claim/{{$claims->id}}"><i class="fa fa-pencil"></i></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

        @endif
    @endif

    <!-- The Modal -->
        <div id="id01" class="modal">
            <!-- Modal Content -->
            <div class="modal-content">
                <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
                    ORIGINAL MATERIAL
                </p>
                <p class="text-home-warning">Indieplant can only publish original material. Please confirm that this is
                    your own work, or work of composer(s) and/or lyricist(s) you are entitled to publish work from.</p>
                <p class="btn-warning1">
                    <button onclick="document.getElementById('id01').style.display='none'" class="btn btn-cancel">
                        CANCEL
                    </button>
                    <button onclick="agreeFunction()" class="btn">CONFIRM</button>
                </p>
                <div class="checked1 checked2">
        <span class="squaredTwo">
          <input type="checkbox" value="None" id="squaredTen" name="check" class="home-orinal-material">
          <label for="squaredTen" style=""></label>
        </span>
                    <div class="home-confirm">
                        <span>Confirm for this, and all future songs</span>
                        <br/><span>I will publish via Indieplant.</span>
                    </div>
                </div>
                <script type='text/javascript'>
                    function agreeFunction() {
                        if ($('input#squaredTen').is(':checked')) {
                            location.href = "{{route('agreement')}}";
                        }
                    }
                </script>

            </div>
        </div>
        <div id="idSubmitClaim" class="modal">
            <!-- Modal Content -->
            <div class="modal-content modal-content-publish-song modal-saved-claims modal-submit-claim">
                <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
                    SUBMIT CLAIM?
                </p>
                <p class="text-home-warning">Are you sure you want to submit this broadcast<br>claim? You can't edit
                    your claim later.</p>
                <p class="btn-warning1">
                    <button onclick="document.getElementById('idSubmitClaim').style.display='none'"
                            class="btn btn-cancel">CANCEL
                    </button>
                    <button onclick="agreeFunction()" class="btn btn-confirm">CONFIRM</button>
                </p>
            </div>
        </div>
    </div>

    </div>
    <script type='text/javascript'>
        $('.btn-publish').on('click', function () {
            location.href = "{{route('add_claim_empty')}}";
        });

        function list_songsFunction() {
            location.href = "{{route('listsongs')}}";
        }

        $('.data-claim-title').click(function () {
            $(this).next($('.mobile-show')).slideToggle();
        });
        $('.data-songs-title').click(function () {
            $(this).next($('.mobile-show')).slideToggle();
        });
    </script>
@endsection
