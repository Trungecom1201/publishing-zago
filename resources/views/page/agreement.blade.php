@extends('master')
@section('content')
<div class="content content-agreement">
<p class="title-content title-content-agreement">INDIEPLANT PUBLISHING AGREEMENT</p>
<p class="note-before-read">Please read the following agreement and click Accept to confirm you've read, understand and accept it.</p>
<div class="content-agreement-detail">
  <p>
    This Pubishing Agreement has been compiled to better serve those who are concerned with how their 'Personally Identifiable Infomati-on' (Pll) is being used online. Pll, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Indentifiable Information in accordance with our website.             
  </p> 
  <p>
    What person information do we collect from the people that visit our blog, website or app?
  </p>
  <p>
    When ordering or registering on our website, as appropreate, you may be asked to enter your name, email address, phone number, Banking Details or other details to help you with your experience.  
  </p>
  <p>
    When do we collect information?
  </p>
</div>
<p class="check-agreement"><span class="squaredTwo">
  <input type="checkbox" value="None" id="squaredTwo" name="check" />
  <label for="squaredTwo"></label>
</span><span> I have read and agree with the agreement. </span></p>

<p class="btn-warning1 btn-agreement1">
  <button onclick="warningnotcheckFunction()" class="btn btn-continue">CONTINUE</button>
  <a href="{{route('home')}}">Cancel</a>
</p>
<!-- The Modal -->
<div id="id01" class="modal" >
  

  <!-- Modal Content -->
  <div class="modal-content modal-content-warning-agreement">
    <p class="btn-ok-warning-agreement">Please confirm that you've read and agree with our Publishing Agreement.</p>
    <p class="btn-warning1"><button onclick="document.getElementById('id01').style.display='none'" class="btn">OK</button></p>
     
	
	<script type='text/javascript'>
    function agreeFunction() {
        location.href = "{{route('home')}}";
    }
    function warningnotcheckFunction(){
      if ($('input#squaredTwo').is(':checked')) {
      location.href = "{{route('addsong')}}";
    }
    else{
      document.getElementById('id01').style.display='block';  
    }
  }
	</script>
    
    </div>
  </div>
</div>

</div>
@endsection