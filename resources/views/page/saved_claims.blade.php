@extends('master')
@section('content')
<div class="content content-songs">
<p class="title-content">PUBLISHING</p>
<div class="content-option content-option-1 content-option-published1">
	<p class="title-option">PUBLISHED</p>

	<p><button class="btn btn-published-song btn-saved-claims" onclick="list_songsFunction()">35</button></p>
	<p class="note1 info1">LAST SONG</p>
  <p class="note1 info2">BEYOND RAGING WILD WAVES - LP MIX</p>
  <p class="note1 info3">2 DAYS AGO</p>
</div>
<div class="content-option content-option-published2">
	<p class="title-option title-option2">BROADCAST CLAIMS</p>
	<img src="source/image/content/movie1.jpg">
	<p><button class="btn btn1 btn-publish" onclick="saved_claimsFunction()">+ ADD CLAIM</button></p>
	<p class="note1 note2">YOU HAVEN'T SUBMITTED ANY CLAIMS YET</p>
</div>
<div class="content-option content-option-published11" hidden="">
  <p class="title-option">PUBLISHED</p>

  <p><button class="btn btn-published-song btn-saved-claims" onclick="broadcast_claimsFunction()">6</button></p>
  <p class="note1 info1">LAST CLAIMS</p>
  <p class="note1 info2">SKY AND SAND</p>
  <p class="note1 info3">3 WEEKS AGO</p>
</div>
<div class="clear"></div>
<div class="concept-songs">
<table width="99%" height="50px">
    <tbody><tr>
        <th class="color-text20" width="24.75%">CONCEPT</th>
        <th class="small-text2" width="23.75%">COMPOSERS</th>
                <th class="small-text2" width="23.75%">LYRICISTS</th>
        <th class="small-text2" width="10.75%">LAST SAVE</th>
        <th class="small-text2" width="3%"></th>
                <th width="12%"></th>
        <th width="1%"></th>
        
    </tr>
    
    <tr class="background-row2">
        <td class="padding-text">Lullaby</td>
        <td class="padding-zero small-text">-</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">-</td>
        <td class="padding-zero small-text">23-09-2018</td>
        <td class="padding-zero"><i class="fa fa-play fa-play-color"></i></td>
<td><button class="btn btn1 btn17 btn27" onclick="published_songFunction()">PUBLISH</button></td>
        <td class="padding-zero"><i class="fa fa-pencil fa-pencil2 color-pencil" style=""></i></td>        
    </tr>
    <tr class="background-row2 border-row1 row_submit" hidden="">
        <td class="padding-text">Breezeblocks</td>
        <td class="padding-zero small-text">S. Wit</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">C.val Sliedregt, S.Wit</td>
        <td class="padding-zero small-text">-</td>
        <td class="padding-zero"><i class="fa fa-play fa-play-color"></i></td>
<td><button class="btn btn17 btn27" onclick="published_songFunction()">PUBLISH</button></td>
        <td class="padding-zero"><i class="fa fa-pencil fa-pencil2 color-pencil" style=""></i></td>        
    </tr>
    <tr class="background-row2 border-row1 row_submit" hidden="">
        <td class="padding-text">Firestarter</td>
        <td class="padding-zero small-text">J.Kleerebezem</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">-</td>
        <td class="padding-zero small-text">11-7-2018</td>
        <td class="padding-zero"><i class="fa fa-play fa-play-color"></i></td>
<td><button class="btn btn1 btn17 btn27" onclick="published_songFunction()">PUBLISH</button></td>
        <td class="padding-zero"><i class="fa fa-pencil fa-pencil2 color-pencil" style=""></i></td>        
    </tr>    
</tbody></table>
</div>
<div class="concept-songs">
<table width="99%" height="50px">
    <tbody><tr>
        <th class="color-text20" width="29%">CONCEPT CLAIMS</th>
        <th class="small-text2" width="7%">MEDIUM</th>
                <th class="small-text2" width="15%">STATION/CHANNEL</th>
                <th class="small-text2" width="16%">PROGRAM</th>
        <th class="small-text2" width="10%">AIR DATE</th>
        <th class="small-text2" width="9%">COUNTRY</th>
                <th width="12%"></th>
        <th width="1%"></th>
        
    </tr>
    
    <tr class="background-row2">
        <td class="padding-text">In The Middle Of The Night</td>
        <td class="padding-zero small-text">Radio</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">BBC Radio 4</td>
        <td class="padding-zero small-text">BBC Breakfast</td>
        <td class="padding-zero small-text">23-09-2018</td>
        <td class="padding-zero small-text">UK</td>
<td><button class="btn btn17 btn27 btn29" onclick="document.getElementById('id01').style.display='block'">SUBMIT</button></td>
        <td class="padding-zero"><i class="fa fa-pencil fa-pencil2 color-pencil" style=""></i></td>        
    </tr>
    <tr class="background-row2 border-row1">
        <td class="padding-text">Lullaby</td>
        <td class="padding-zero small-text">TV</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">-</td>
        <td class="padding-zero small-text">-</td>
        <td class="padding-zero small-text">20-09-2018</td>
        <td class="padding-zero small-text">-</td>
<td><button class="btn btn1 btn17 btn27" onclick="published_songFunction()">SUBMIT</button></td>
        <td class="padding-zero"><i class="fa fa-pencil fa-pencil2 color-pencil" style=""></i></td>        
    </tr>    
</tbody></table>
</div>

<!-- The Modal -->
<div id="id01" class="modal" >
  <!-- Modal Content -->
  <div class="modal-content modal-content-publish-song modal-saved-claims">
    <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
      ARE YOU SURE?
    </p>
    <p class="text-home-warning">Please make sure everything is correct.<br>Your song will now be published.</p>
    <p class="btn-warning1">
      <button onclick="document.getElementById('id01').style.display='none'" class="btn btn-cancel">CANCEL</button>
      <button onclick="agreeFunction()" class="btn">CONFIRM</button>
    </p> 
  

	
	<script type='text/javascript'>
    function agreeFunction() {
      $(".content-option-published2").hide();
      $(".content-option-published11").show();
      $(".row_submit").show();
      $("#id01").hide();
        //location.href = "{{route('agreement')}}";
      // alert("Agree");
      //location.href = "{{route('agreement')}}";
      
    }
    function saved_claimsFunction() {
        location.href = "{{route('saved_claims')}}";
    }
    function list_songsFunction() {
        location.href = "{{route('list_songs')}}";
    }
    function broadcast_claimsFunction() {
        location.href = "{{route('broadcast_claims')}}"; 
    }
	</script>
    
    </div>
  </div>
</div>

</div>
@endsection