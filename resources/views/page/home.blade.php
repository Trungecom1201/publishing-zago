@extends('master')
@section('content')
<div class="content">
<p class="title-content">PUBLISHING</p>
<div class="content-option content-option-1">
	<p class="title-option">PUBLISHED</p>
	<img src="source/image/content/music1.jpg">

	<p>
    @if(Auth::check())  
        <button onclick="document.getElementById('id01').style.display='block'" class="btn">+ ADD SONG</button>
        @else
        <button onclick="window.location.href = 'login'" class="btn">+ ADD SONG</button>
        
    @endif
    </p>
	<p class="note1">YOU HAVEN'T PUBLISHED ANY SONGS YET</p>
</div>
<div class="content-option">
	<p class="title-option">BROADCAST CLAIMS</p>
	<img src="source/image/content/movie1.jpg">
	<p><button class="btn btn1">+ ADD CLAIM</button></p>
	<p class="note1">PLEASE FIRST PUBLISH A SONG</p>
</div>

<!-- The Modal -->
<div id="id01" class="modal" >
  <!-- Modal Content -->
  <div class="modal-content">
    <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
    	ORIGINAL MATERIAL
    </p>
    <p class="text-home-warning">Indieplant can only publish original material. Please confirm that this is your own work, or work of composer(s) and/or lyricist(s) you are entitled to publish work from.</p>
    <p class="btn-warning1">
      <button onclick="document.getElementById('id01').style.display='none'" class="btn btn-cancel">CANCEL</button>
      <button onclick="agreeFunction()" class="btn">CONFIRM</button>
    </p>
    <p class="checked1 checked2"><span class="squaredTwo">
  <input type="checkbox" value="None" id="squaredTen" name="check" style="
    /* margin-top: 13px; */
    
">
  <label for="squaredTen" style=""></label>
</span><span class="text-checked-line1">Confirm for this, and all future songs</span> <br/><span class="text-checked-line2">I will publish via Indieplant.</span></p> 
	
	<script type='text/javascript'>
    function agreeFunction() {
      if ($('input#squaredTen').is(':checked')) {
        location.href = "{{route('agreement')}}";
      }
      // alert("Agree");
      //location.href = "{{route('agreement')}}";
      
    }
	</script>
    
    </div>
  </div>
</div>

</div>
@endsection
