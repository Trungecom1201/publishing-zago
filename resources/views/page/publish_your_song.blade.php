@extends('master')
@section('content')
<?php

$key1 = max(array_keys(Session()->get('info')));
foreach (Session()->get('info') as $key => $val) {

    if($key == $key1){
        $val1 = $val[0];
        $val2 = $val[1];
        $val3 = $val[2];
        $val4 = $val[3];
        $val5 = $val[4];
        $val6 = $val[5];
        $val7 = $val[6];
        $val8 = $val[7];
        $val9 = $val[8];
        $val10 = $val[9];
        $val11 = $val[10];
        $val12 = $val[11];
        $val13 = $val[12];
        $val14 = $val[13];
    }

}

?>
<div class="content content-agreement content-publish-song publish-song-pages">
<p class="title-content title-content-agreement">PUBLISH YOUR SONG</p>
<p class="note-before-read publish-song-title">Please check your song info and contributors and click Publish if everything is correct.</p>
    <div class="publish-song-info-container">
<div class="parent-publish-song-info">
  <p class="title-publish-song title-song-info2">SONG INFO</p>
<div class="publish-song-info">
  <div class="publish-song-info1 padding-for-title-publish-song">
  <p class="title-option-publish-song">Title</p>
  <p class="content-option-publish-song"><?php echo $val1; ?></p>
</div>
<div class="publish-song-info2 padding-for-title-publish-song border-publish-song">
  <p class="title-option-publish-song">Duration</p>
  <p class="content-option-publish-song"><?php echo $val2; ?></p>
</div>
<div class="publish-song-info3 padding-for-title-publish-song">
  <p class="option-publish-song1"><?php if( $val3 == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Contains Sample(s)</p>
  <p class="option-publish-song1"><?php if( $val4 == "1" ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Do you have the right to use the sample(s)?</p>
</div>
<div class="publish-song-info4 padding-for-title-publish-song border-publish-song padding-special">
  <p class="option-publish-song1"><?php if( $val5 == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Remix</p>
</div>
<div class="publish-song-info5 padding-for-title-publish-song">
  <p class="option-publish-song1 margin-bottom-table"><?php if( $val6 == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Previously registered with a PRO</p>
  <?php if( $val6 == 1 ) { ?>
  <table border="0px" width="350px" height="50px">
    <tr class="change-color-row">
        <td class="padding-zero">which PRO</td>
        <td class="padding-zero">PRS TurnCode</td>

    </tr>
    <tr>
        <td class="padding-zero"><?php echo $val7; ?></td>
        <td class="padding-zero"><?php echo $val8; ?></td>

    </tr>

</table>
<?php } ?>
</div>
<div class="publish-song-info6 padding-for-title-publish-song border-publish-song publish-song1">
  <p class="title-option-publish-song">Audio file (original)</p>
 <?php if( $val9 != null ){?>
    <p class="content-option-publish-song">
        <img src="source/image/content/music2.jpg"> <?php echo $val9; ?><span class="size-song">7.511 KB</span>
    </p>
    <?php }else{ ?>There are no songs<?php } ?>

<?php if( $val10 != null ){?>
    <div class="dowload_music publish-song-dowload_music"><a href="source/music/{{$val10}}" download><i class="fa fa-download"></i></a></div>
    <div class="play_music publish-song-play_music">
        <i class="fa fa-play"></i>
    <audio style="display:none" controls="" class="play-publish-song1"><source src="source/music/<?php echo $val10; ?>" type="audio/mpeg"></audio>

    </div>
    <?php } ?>
</div>
<div class="publish-song-info7 padding-for-title-publish-song publish-song1">
  <p class="title-option-publish-song">Audio file (instrumental)</p>
  <?php if( $val11 != null ){?><p class="content-option-publish-song"><img src="source/image/content/music2.jpg"> <?php echo $val11; ?><span class="size-song">7.290 KB</span></p><?php }else{ ?>There are no songs<?php } ?>
  <!--<img class="listen_music2" src="source/image/content/pause.jpg" style="
    float: right;
    margin: -28px 0 0 0;
    cursor: pointer;
">-->
<?php if( $val12 != null ){?>
    <div class="dowload_music publish-song-dowload_music"><a href="source/music/{{$val12}}" download><i class="fa fa-download"></i></a></div>
    <div class="play_music publish-song-play_music ">
        <i class="fa fa-play"></i>
            <audio style="display:none" controls="" class="play-publish-song1"><source src="source/music/<?php echo $val12; ?>" type="audio/mpeg"></audio>

    </div>
        <?php } ?>
</div>
</div>
</div>

<div class="parent-publish-contributor">
  <p class="title-publish-song title-publish-contributor">CONTRIBUTORS</p>
<div class="publish-contributor"><?php

if($val13 != null) {
        foreach ($val13 as $key => $val) {

    if( !empty($val[0]) ){


        $val20 = $val[0];
    }else{
        $val20 = "";
    }
    if( !empty($val[1]) ){


        $val21 = $val[1];
    }else{
        $val21 = "";
    }
    if( !empty($val[2]) ){


        $val22 = $val[2];
    }else{
        $val22 = "";
    }
    if( !empty($val[3]) ){


        $val23 = $val[3];
    }else{
        $val23 = "";
    }
    if( !empty($val[4]) ){


        $val24 = $val[4];
    }else{
        $val24 = "";
    }
    if( !empty($val[5]) ){


        $val25 = $val[5];
    }else{
        $val25 = "";
    }
    if( !empty($val[6]) ){


        $val26 = $val[6];
    }else{
        $val26 = "";
    }
    if( !empty($val[7]) ){


        $val27 = $val[7];
    }else{
        $val27 = "";
    }
    if( !empty($val[8]) ){


        $val28 = $val[8];
    }else{
        $val28 = "";
    }
    if( !empty($val[9]) ){


        $val29 = $val[9];
    }else{
        $val29 = "";
    }

                ?>
    <div class="publish-contributor1 padding-for-title-publish-song">
        <div class="publish-contributor-sub1 publish-contributor-sub">
            <p class="title-option-publish-song">Name</p>
            <p class="content-option-publish-song contributors-details"><?php echo $val20.' '.$val21 ; ?></p>
            <p class="title-option-publish-song">Role</p>
            <p class="content-option-publish-song contributors-details"><?php echo $val22; ?></p>
            <p class="title-option-publish-song">Share</p>
            <p class="content-option-publish-song"><?php echo $val29 ; ?>%</p>
        </div>
        <div class="publish-contributor-sub2 publish-contributor-sub">
            <p class="option-publish-song1 margin-bottom-table"><?php if( $val23 == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Member of a PRO</p>
            <?php if( $val23 == 1 ) { ?>
            <table border="0px" width="250px" height="50px">
                <tbody><tr class="change-color-row">
                    <td class="padding-zero">which PRO</td>
                    <td class="padding-zero">CAE Number</td>

                </tr>
                <tr>
                    <td class="padding-zero"><?php echo $val24; ?></td>
                    <td class="padding-zero"><?php echo $val25; ?></td>

                </tr>

                </tbody></table>
            <?php } ?>
            <p class="option-publish-song1 margin-bottom-table contributors-details3"><?php if( $val26 == 1 ){?><img src="source/image/content/ticker1.jpg"><?php }else {?><img src="source/image/content/unticker.jpg"><?php } ?>Member of a mechanical society</p>
            <?php if( $val26 == 1 ) { ?>
            <table border="0px" width="250px" height="50px">
                <tbody><tr class="change-color-row">
                    <td class="padding-zero">which PRO</td>
                    <td class="padding-zero">CAE Number</td>

                </tr>
                <tr>
                    <td class="padding-zero"><?php echo $val27; ?></td>
                    <td class="padding-zero"><?php echo $val28; ?></td>

                </tr>

                </tbody></table>
            <?php } ?>
        </div>
    </div>
            <?php
        }?>
        <div class="publish-contributor3 padding-for-title-publish-song padding-special">
<p class="option-publish-song1"><?php if( $val14 == 0 ){?><img src="source/image/content/unticker.jpg"><?php }else {?><img src="source/image/content/ticker1.jpg"><?php } ?>Equal shares</p>
</div>
    <?php
        }else{?>
    <div class="publish-contributor3 padding-for-title-publish-song padding-special">
<p class="option-publish-song1 option-publish-song2">No contributor</p>
</div>
  <?php } ?>

</div>
</div>
    </div>
<p class="btn-warning1 btn-agreement1 btn-publish-song">
  <button onclick="backFunction()" class="btn btn-continue btn-back1 btn-publish-song1">BACK</button>
  <button onclick="document.getElementById('id01').style.display='block'" class="btn btn-continue btn-publish1 btn-publish-song1">PUBLISH</button>
</p>
<!-- The Modal -->
<div id="id01" class="modal" >
  <!-- Modal Content -->
  <div class="modal-content modal-content-publish-song">
    <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
      ARE YOU SURE?
    </p>
    <p class="text-home-warning">Please make sure everything is correct.<br>Your song will now be published.</p>
    <p class="btn-warning1">
      <button onclick="document.getElementById('id01').style.display='none'" class="btn btn-cancel">CANCEL</button>
      <button onclick="agreeFunction()" class="btn btn-comfirm">CONFIRM</button>
    </p>
    <p class="check-agreement check-agreement5"><span class="squaredTwo">
  <input type="checkbox" value="None" id="squaredSixtys" name="check">
  <label for="squaredSixtys"></label>
</span><span class="text-publish-song"> Don't show this message again. </span></p>


	<script type='text/javascript'>
    function agreeFunction() {
        location.href = "{{route('savedsong')}}";
    }
    function warningnotcheckFunction(){
      if ($('input#squaredTwo').is(':checked')) {
      location.href = "{{route('addsong')}}";
    }
    else{
      document.getElementById('id01').style.display='block';
    }
  }
  var play_music1 = true;
  $('.publish-song-info6').on('click','img',function(){

    if (play_music1 === true){
     //alert($('img.testtest').attr('src'));
     $('img.listen_music1').attr("src","source/image/content/pause.jpg");
     play_music1 = false;
   }
     else {
      $('img.listen_music1').attr("src","source/image/content/play1.jpg");
      play_music1 = true;
     }
  })
  var play_music2 = true;
  $('.publish-song-info7').on('click','img',function(){

    if (play_music2 === true){
     //alert($('img.testtest').attr('src'));
     $('img.listen_music2').attr("src","source/image/content/play1.jpg");
     play_music2 = false;
   }
     else {
      $('img.listen_music2').attr("src","source/image/content/pause.jpg");
      play_music2 = true;
     }
  })
  function backFunction() {
        location.href = "{{route('addsong')}}";
    }
	</script>

    </div>
  </div>
</div>

</div>
@endsection