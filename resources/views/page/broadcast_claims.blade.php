@extends('master')
@section('content')
<div class="content content-songs">
<p class="title-content">BROADCAST CLAIMS</p>
<p class="btn-warning1 btn-agreement1 btn-publish-song btn-add-song3">
  <button onclick="document.getElementById('id01').style.display='block'" class="btn btn-continue btn-back1 btn-publish-song1">+ ADD CLAIM</button>
</p>
<div class="concept-songs concept-songs2">
<table width="99%" height="50px">
    <tbody><tr>
        <th class="color-text20" width="29%">CONCEPT CLAIMS</th>
        <th class="small-text2" width="7%">MEDIUM</th>
                <th class="small-text2" width="15%">STATION/CHANNEL</th>
                <th class="small-text2" width="16%">PROGRAM</th>
        <th class="small-text2" width="10%">AIR DATE</th>
        <th class="small-text2" width="9%">COUNTRY</th>
                <th width="12%"></th>
        <th width="1%"></th>
        
    </tr>
    
    <tr class="background-row2">
        <td class="padding-text">In The Middle Of The Night</td>
        <td class="padding-zero small-text">Radio</td>
        <td class="padding-zero small-text" style="
    /* background-color: white; */
">BBC Radio 4</td>
        <td class="padding-zero small-text">BBC Breakfast</td>
        <td class="padding-zero small-text">23-09-2018</td>
        <td class="padding-zero small-text">UK</td>
<td><button class="btn btn17 btn27 btn29" onclick="document.getElementById('idSubmitClaim').style.display='block'">SUBMIT</button></td>
        <td class="padding-zero"><i class="fa fa-pencil fa-pencil2 color-pencil" style="" onclick="edit_claimFunction()"></i></td>        
    </tr>
    <tr class="background-row2 border-row1">
        <td class="padding-text">Lullaby</td>
        <td class="padding-zero small-text">Radio</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">-</td>
        <td class="padding-zero small-text">-</td>
        <td class="padding-zero small-text">-</td>
        <td class="padding-zero small-text">-</td>
<td><button class="btn btn1 btn17 btn27" onclick="published_songFunction()">SUBMIT</button></td>
        <td class="padding-zero"><i class="fa fa-pencil fa-pencil2 color-pencil" style=""></i></td>        
    </tr>
    <tr class="background-row2 border-row1">
        <td class="padding-text">In The Middle Of The Night</td>
        <td class="padding-zero small-text">TV</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">-</td>
        <td class="padding-zero small-text">DJ Mike's Power Hour</td>
        <td class="padding-zero small-text">20-09-2018</td>
        <td class="padding-zero small-text">NL</td>
<td><button class="btn btn1 btn17 btn27" onclick="published_songFunction()">SUBMIT</button></td>
        <td class="padding-zero"><i class="fa fa-pencil fa-pencil2 color-pencil" style=""></i></td>        
    </tr>    
</tbody></table>
</div>
<div class="concept-songs">
<table width="99%" height="50px">
    <tbody><tr>
        <th class="color-text20" width="29%">CONCEPT CLAIMS</th>
        <th class="small-text2" width="7%">MEDIUM</th>
                <th class="small-text2" width="15%">STATION/CHANNEL</th>
                <th class="small-text2" width="16%">PROGRAM</th>
        <th class="small-text2" width="10%">AIR DATE</th>
        <th class="small-text2" width="9%">COUNTRY</th>
                <th width="12%" class="small-text2">SUBMIT DATE</th>
        <th width="1%"></th>
        
    </tr>
    
    <tr class="background-row2">
        <td class="padding-text">In The Middle Of The Night</td>
        <td class="padding-zero small-text">Radio</td>
        <td class="padding-zero small-text" style="
    /* background-color: white; */
">BBC Radio 4</td>
        <td class="padding-zero small-text">BBC Breakfast</td>
        <td class="padding-zero small-text">20-09-2018</td>
        <td class="padding-zero small-text">UK</td>
<td class="padding-zero small-text">23-09-2018</td>
        <td class="padding-zero"><i class="fa fa-eye fa-pencil2 color-pencil color-pencil2 color-pencil3" onclick="document.getElementById('idShowclaims').style.display='block';"></i></td>        
    </tr>
    <tr class="background-row2 border-row1">
        <td class="padding-text">Lullaby</td>
        <td class="padding-zero small-text">Radio</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">-</td>
        <td class="padding-zero small-text">BBC Breakfast</td>
        <td class="padding-zero small-text">20-09-2018</td>
        <td class="padding-zero small-text">UK</td>
<td class="padding-zero small-text">23-09-2018</td>
        <td class="padding-zero"><i class="fa fa-eye fa-pencil2 color-pencil color-pencil2 color-pencil3" style="" onclick="document.getElementById('idShowclaims').style.display='block';"></i></td>        
    </tr>
    <tr class="background-row2 border-row1">
        <td class="padding-text">In The Middle Of The Night</td>
        <td class="padding-zero small-text">Radio</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">-</td>
        <td class="padding-zero small-text">DJ Mike's Power Hour</td>
        <td class="padding-zero small-text">16-08-2018</td>
        <td class="padding-zero small-text">NL</td>
<td class="padding-zero small-text">02-09-2018</td>
        <td class="padding-zero"><i class="fa fa-eye fa-pencil2 color-pencil color-pencil2 color-pencil3" style="" onclick="document.getElementById('idShowclaims').style.display='block';"></i></td>        
    </tr>
    <tr class="background-row2 border-row1">
        <td class="padding-text">In The Middle Of The Night</td>
        <td class="padding-zero small-text">Radio</td>
        <td class="padding-zero" style="
    /* background-color: white; */
">-</td>
        <td class="padding-zero small-text">Eigen Huis en Turin</td>
        <td class="padding-zero small-text">11-8-2018</td>
        <td class="padding-zero small-text">NL</td>
<td class="padding-zero small-text">02-09-2018</td>
        <td class="padding-zero"><i class="fa fa-eye fa-pencil2 color-pencil color-pencil2 color-pencil3" style="" onclick="document.getElementById('idShowclaims').style.display='block';"></i></td>        
    </tr>    
</tbody></table>
</div>

<div id="idSubmitClaim" class="modal" >
  <!-- Modal Content -->
  <div class="modal-content modal-content-publish-song modal-saved-claims modal-submit-claim">
    <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
      SUBMIT CLAIM?
    </p>
    <p class="text-home-warning">Are you sure you want to submit this broadcast<br>claim? You can't edit your claim later.</p>
    <p class="btn-warning1">
      <button onclick="document.getElementById('idSubmitClaim').style.display='none'" class="btn btn-cancel">CANCEL</button>
      <button onclick="agreeFunction()" class="btn">CONFIRM</button>
    </p>
  </div>
</div>
<div id="idShowclaims" class="modal" hidden="">
  

  <!-- Modal Content -->
  <div class="modal-content modal-add-contributor modal-info-claim">
    <p class="title-home-warning title-upload-song title-claims">
      BROADCAST CLAIM
    </p>
    <div class="concept-songs concept-songs2">
<table width="160%" style="font-size: 14px;">
    <tbody>
    <tr class="background-row2 height-info-claim" style="height: 50px;">
        <td class="padding-zero text-info-claim color-info-claim" width="40%">Submit date:</td>
        <td class="padding-zero text-info-claim" width="120%">23-09-2018</td>                
    </tr>
    
    <tr class="background-row2 height-info-claim">
        <td class="padding-zero text-info-claim color-info-claim">Song:</td>
        <td class="padding-zero text-info-claim">In The Middle Of The Night</td>                
    </tr>
    <tr class="background-row2 height-info-claim">
        <td class="padding-zero text-info-claim color-info-claim">Medium:</td>
        <td class="padding-zero text-info-claim">Radio</td>        
    </tr>
    <tr class="background-row2 height-info-claim">
        <td class="padding-zero text-info-claim color-info-claim">Station:</td>
        <td class="padding-zero text-info-claim">BBC Radio 4</td>        
    </tr>
    <tr class="background-row2 height-info-claim">
        <td class="padding-zero text-info-claim color-info-claim">Program:</td>
        <td class="padding-zero text-info-claim">BBC Breakfast</td>        
    </tr>    
    <tr class="background-row2 height-info-claim">
        <td class="padding-zero text-info-claim color-info-claim">Air date:</td>
        <td class="padding-zero text-info-claim">20-09-2018</td>                
    </tr>
    <tr class="background-row2 height-info-claim">
        <td class="padding-zero text-info-claim color-info-claim">Country:</td>
        <td class="padding-zero text-info-claim">UK</td>        
    </tr>
    <tr class="background-row2 height-info-claim">
        <td class="padding-zero text-info-claim color-info-claim evidence">Evidence:</td>
        <td class="padding-zero text-info-claim">https://www.rtl.nl/video/a0ab2a1febd9-3b8e-9994-68f99fffe363/ence - at 12 min 29 sec</td>        
    </tr>
    <tr class="background-row2 height-info-claim">
        <td class="padding-zero text-info-claim color-info-claim">File:</td>
        <td class="padding-zero text-info-claim">BBC Breakfast Sunday June 7 ...mp3</td>        
    </tr>    
</tbody></table>
</div>
    

    
 
  







    <p class="btn-choose-file btn-add-contributor5 color-a">      
      <button onclick="document.getElementById('idShowclaims').style.display='none'" id="btnchange-color" class="btn btn-upload-audio btn-upload-audio2 btn-add-contributor2 btn-claims">CLOSE</button>      
    </p>
     
  
  
</div>
</div>

<!-- The Modal -->
<div id="id01" class="modal" >
  <!-- Modal Content -->
  <div class="modal-content modal-content-publish-song modal-saved-claims modal-add-song1">
    <p class="title-home-warning">
      COPY EXISTING SONG?
    </p>
    <p class="text-home-warning">You can select a song to copy its contributors<br>and song info for your new song.</p>
    <select class="form-control form-control-pro form-control-pro1" id="sel1">
    <option>In The Middle Of The Night</option>
  </select>
  <p class="btn-choose-file contributor-radio1 contributor-radio2">
                            
  <input id="composer25" name="option28" type="radio" value="Composer" checked="">
  <label for="composer25">Contributors</label>
  <input id="lyricist26" name="option28" type="radio" value="Lyricist">
  <label for="lyricist26">Contributors + Song info</label>
  
                        
  </p>
    <p class="btn-warning1">
      <button onclick="blankFunction()" class="btn btn-cancel btn-blank">START BLANK</button>
      <button onclick="copy_songFunction()" class="btn">COPY SONG</button>
    </p>     
	
	<script type='text/javascript'>
    function agreeFunction() {
        location.href = "{{route('agreement')}}";
      // alert("Agree");
      //location.href = "{{route('agreement')}}";
      
    }
    function blankFunction() {
        location.href = "{{route('add_claim_empty')}}";
    }
    /*
    var play_music4 = true;
    var play_music5 = true;
    var play_music6 = true;
    var play_music7 = true;
  $('.concept-songs5').on('click','img',function(){
    
    if (play_music4 === true){
     //alert($('img.testtest').attr('src'));
     $('img.test765').attr("src","source/image/content/pause.jpg");
     play_music4 = false;
   }
     else if (play_music4 === true){
      $('img.test766').attr("src","source/image/content/play1.jpg");
      play_music4 = true;
     }   
  })
  */
  var play_music4 = true;
  var play_music5 = true;
  var play_music6 = true;
  var play_music7 = true;
  function play_pause_music1() {
    if (play_music4 === true){
     //alert($('img.testtest').attr('src'));
     $('img.test765').attr("src","source/image/content/pause.jpg");
     play_music4 = false;
     //alert($(this).parent().attr("src"));
     //alert($(this).attr("src"));
   }
     else if (play_music4 === false){
      $('img.test765').attr("src","source/image/content/play1.jpg");
      play_music4 = true;
     }
      
    }
    function play_pause_music2() {
    if (play_music5 === true){
     //alert($('img.testtest').attr('src'));
     $('img.test766').attr("src","source/image/content/pause.jpg");
     play_music5 = false;
   }
     else if (play_music5 === false){
      $('img.test766').attr("src","source/image/content/play1.jpg");
      play_music5 = true;
     }
      
    }
    function play_pause_music3() {
    if (play_music6 === true){
     //alert($('img.testtest').attr('src'));
     $('img.test767').attr("src","source/image/content/pause.jpg");
     play_music6 = false;
   }
     else if (play_music6 === false){
      $('img.test767').attr("src","source/image/content/play1.jpg");
      play_music6 = true;
     }
      
    }
    function play_pause_music4() {
    if (play_music7 === true){
     //alert($('img.testtest').attr('src'));
     $('img.test768').attr("src","source/image/content/play1.jpg");
     play_music7 = false;
   }
     else if (play_music7 === false){
      $('img.test768').attr("src","source/image/content/pause.jpg");
      play_music7 = true;
     }
      
    }
    function finished_publish_songsFuction() {
      location.href = "{{route('finished_publish_songs')}}";
    } 
    function add_songFunction() {
      location.href = "{{route('addsong')}}";
    }
    function edit_songFunction() {
      location.href = "{{route('edit_song')}}"; 
    }
    function copy_songFunction() {
      location.href = "{{route('copy_song')}}"; 
    }
    function show_info_claimFunction() {

    }
	function edit_claimFunction() {
      location.href = "{{route('edit_claim')}}";	
    }
	</script>
    
    </div>
  </div>
</div>

@endsection