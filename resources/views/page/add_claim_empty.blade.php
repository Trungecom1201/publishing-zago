@extends('master')
@section('content')
<div class="content content-claim-empty page-claim">
  <form action="add-claim" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{csrf_token()}}" />
<p class="title-content title-content-agreement">ADD BROADCAST CLAIM</p>
<div class="parent-claim-empty">
<div class="modal-claim-empty modal-add-contributor">
<div class="form-group form-group4 claim-played-song form-margin-bottom">
  <label class="label-title label-title-2">Played song <img src="source/image/content/question.jpg"></label>
    <input type="hidden" name="name_song" class="claim-name-song"/>
    <input type="hidden" name="url_song" class="claim-url" />
    <input type="hidden" name="id_song" class="claim-id-song" />
    <input type="hidden" name="check_publish" class="claim_check_publish" />
  <select class="form-control form-control-pro claim-select-song" required id="sel1" name="song">
    <option value="">Please select</option>
    @foreach($songs as $key => $value)
    <option value="{{$value->song_title}}" data-id="{{$value->id}}" data-name="{{$value->audio_original}}" data-url="{{$value->file_url}}" >{{$value->song_title}}</option>
    @endforeach
  </select>
</div>
<p class="btn-choose-file contributor-radio1 choose-tv-radio form-margin-bottom">
      
  <input id="new14" name="radio_tv" type="radio" value="Radio" onclick="radioFunction()" checked="" />
  <label for="new14">Radio</label>
  <input id="new8" name="radio_tv" type="radio" value="TV" onclick="tvFunction()" />
  <label for="new8">TV</label>
  
    </p>
    
    	<div class="form-group form-group5 form-radio form-margin-bottom">
            <label for="station-channel" class="label-title">Station<img src="source/image/content/question.jpg"></label>
            <input type="text" id="station-channel" required name="station_channel" class="form-control form-title form-station form-radio2" placeholder="Station">
                        
  </div>

<div class="form-group form-group3 form-margin-bottom">
    <label class="label-title">Program<img src="source/image/content/question.jpg"></label>
    <input type="text" class="form-control form-title form-program" required placeholder="Program" name="program">
  </div>
  <div class="form-group form-group3 form-margin-bottom  form-air-date">
        <label class="label-title">Air date<img src="source/image/content/question.jpg"></label>
        <input type="text" class="form-control form-title form-date" required placeholder="Date" name="air_date">
        <img src="source/image/content/date.png" style="
    width: 20px;
    height: 20px;
    float: right;
    margin: -10% 1%;
">
                        
  </div>

<div class="form-group form-group4 form-margin-bottom">
  
  <label class="label-title label-title-2">Country <img src="source/image/content/question.jpg"></label>
<select class="form-control form-control-pro form-country" id="sel1" required name="country">
    <option value="" selected>Please select</option>
    <option value="Afghanistan">Afghanistan</option>
    <option value="Albania">Albania</option>
    <option value="Algeria">Algeria</option>
    <option value="American Samoa">American Samoa</option>
    <option value="Andorra">Andorra</option>
    <option value="Angola">Angola</option>
    <option value="Anguilla">Anguilla</option>
    <option value="Antartica">Antarctica</option>
    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
    <option value="Argentina">Argentina</option>
    <option value="Armenia">Armenia</option>
    <option value="Aruba">Aruba</option>
    <option value="Australia">Australia</option>
    <option value="Austria">Austria</option>
    <option value="Azerbaijan">Azerbaijan</option>
    <option value="Bahamas">Bahamas</option>
    <option value="Bahrain">Bahrain</option>
    <option value="Bangladesh">Bangladesh</option>
    <option value="Barbados">Barbados</option>
    <option value="Belarus">Belarus</option>
    <option value="Belgium">Belgium</option>
    <option value="Belize">Belize</option>
    <option value="Benin">Benin</option>
    <option value="Bermuda">Bermuda</option>
    <option value="Bhutan">Bhutan</option>
    <option value="Bolivia">Bolivia</option>
    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
    <option value="Botswana">Botswana</option>
    <option value="Bouvet Island">Bouvet Island</option>
    <option value="Brazil">Brazil</option>
    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
    <option value="Brunei Darussalam">Brunei Darussalam</option>
    <option value="Bulgaria">Bulgaria</option>
    <option value="Burkina Faso">Burkina Faso</option>
    <option value="Burundi">Burundi</option>
    <option value="Cambodia">Cambodia</option>
    <option value="Cameroon">Cameroon</option>
    <option value="Canada">Canada</option>
    <option value="Cape Verde">Cape Verde</option>
    <option value="Cayman Islands">Cayman Islands</option>
    <option value="Central African Republic">Central African Republic</option>
    <option value="Chad">Chad</option>
    <option value="Chile">Chile</option>
    <option value="China">China</option>
    <option value="Christmas Island">Christmas Island</option>
    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
    <option value="Colombia">Colombia</option>
    <option value="Comoros">Comoros</option>
    <option value="Congo">Congo</option>
    <option value="Congo">Congo, the Democratic Republic of the</option>
    <option value="Cook Islands">Cook Islands</option>
    <option value="Costa Rica">Costa Rica</option>
    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
    <option value="Croatia">Croatia (Hrvatska)</option>
    <option value="Cuba">Cuba</option>
    <option value="Cyprus">Cyprus</option>
    <option value="Czech Republic">Czech Republic</option>
    <option value="Denmark">Denmark</option>
    <option value="Djibouti">Djibouti</option>
    <option value="Dominica">Dominica</option>
    <option value="Dominican Republic">Dominican Republic</option>
    <option value="East Timor">East Timor</option>
    <option value="Ecuador">Ecuador</option>
    <option value="Egypt">Egypt</option>
    <option value="El Salvador">El Salvador</option>
    <option value="Equatorial Guinea">Equatorial Guinea</option>
    <option value="Eritrea">Eritrea</option>
    <option value="Estonia">Estonia</option>
    <option value="Ethiopia">Ethiopia</option>
    <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
    <option value="Faroe Islands">Faroe Islands</option>
    <option value="Fiji">Fiji</option>
    <option value="Finland">Finland</option>
    <option value="France">France</option>
    <option value="France Metropolitan">France, Metropolitan</option>
    <option value="French Guiana">French Guiana</option>
    <option value="French Polynesia">French Polynesia</option>
    <option value="French Southern Territories">French Southern Territories</option>
    <option value="Gabon">Gabon</option>
    <option value="Gambia">Gambia</option>
    <option value="Georgia">Georgia</option>
    <option value="Germany">Germany</option>
    <option value="Ghana">Ghana</option>
    <option value="Gibraltar">Gibraltar</option>
    <option value="Greece">Greece</option>
    <option value="Greenland">Greenland</option>
    <option value="Grenada">Grenada</option>
    <option value="Guadeloupe">Guadeloupe</option>
    <option value="Guam">Guam</option>
    <option value="Guatemala">Guatemala</option>
    <option value="Guinea">Guinea</option>
    <option value="Guinea-Bissau">Guinea-Bissau</option>
    <option value="Guyana">Guyana</option>
    <option value="Haiti">Haiti</option>
    <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
    <option value="Holy See">Holy See (Vatican City State)</option>
    <option value="Honduras">Honduras</option>
    <option value="Hong Kong">Hong Kong</option>
    <option value="Hungary">Hungary</option>
    <option value="Iceland">Iceland</option>
    <option value="India">India</option>
    <option value="Indonesia">Indonesia</option>
    <option value="Iran">Iran (Islamic Republic of)</option>
    <option value="Iraq">Iraq</option>
    <option value="Ireland">Ireland</option>
    <option value="Israel">Israel</option>
    <option value="Italy">Italy</option>
    <option value="Jamaica">Jamaica</option>
    <option value="Japan">Japan</option>
    <option value="Jordan">Jordan</option>
    <option value="Kazakhstan">Kazakhstan</option>
    <option value="Kenya">Kenya</option>
    <option value="Kiribati">Kiribati</option>
    <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
    <option value="Korea">Korea, Republic of</option>
    <option value="Kuwait">Kuwait</option>
    <option value="Kyrgyzstan">Kyrgyzstan</option>
    <option value="Lao">Lao People's Democratic Republic</option>
    <option value="Latvia">Latvia</option>
    <option value="Lebanon">Lebanon</option>
    <option value="Lesotho">Lesotho</option>
    <option value="Liberia">Liberia</option>
    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
    <option value="Liechtenstein">Liechtenstein</option>
    <option value="Lithuania">Lithuania</option>
    <option value="Luxembourg">Luxembourg</option>
    <option value="Macau">Macau</option>
    <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
    <option value="Madagascar">Madagascar</option>
    <option value="Malawi">Malawi</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Maldives">Maldives</option>
    <option value="Mali">Mali</option>
    <option value="Malta">Malta</option>
    <option value="Marshall Islands">Marshall Islands</option>
    <option value="Martinique">Martinique</option>
    <option value="Mauritania">Mauritania</option>
    <option value="Mauritius">Mauritius</option>
    <option value="Mayotte">Mayotte</option>
    <option value="Mexico">Mexico</option>
    <option value="Micronesia">Micronesia, Federated States of</option>
    <option value="Moldova">Moldova, Republic of</option>
    <option value="Monaco">Monaco</option>
    <option value="Mongolia">Mongolia</option>
    <option value="Montserrat">Montserrat</option>
    <option value="Morocco">Morocco</option>
    <option value="Mozambique">Mozambique</option>
    <option value="Myanmar">Myanmar</option>
    <option value="Namibia">Namibia</option>
    <option value="Nauru">Nauru</option>
    <option value="Nepal">Nepal</option>
    <option value="Netherlands">Netherlands</option>
    <option value="Netherlands Antilles">Netherlands Antilles</option>
    <option value="New Caledonia">New Caledonia</option>
    <option value="New Zealand">New Zealand</option>
    <option value="Nicaragua">Nicaragua</option>
    <option value="Niger">Niger</option>
    <option value="Nigeria">Nigeria</option>
    <option value="Niue">Niue</option>
    <option value="Norfolk Island">Norfolk Island</option>
    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
    <option value="Norway">Norway</option>
    <option value="Oman">Oman</option>
    <option value="Pakistan">Pakistan</option>
    <option value="Palau">Palau</option>
    <option value="Panama">Panama</option>
    <option value="Papua New Guinea">Papua New Guinea</option>
    <option value="Paraguay">Paraguay</option>
    <option value="Peru">Peru</option>
    <option value="Philippines">Philippines</option>
    <option value="Pitcairn">Pitcairn</option>
    <option value="Poland">Poland</option>
    <option value="Portugal">Portugal</option>
    <option value="Puerto Rico">Puerto Rico</option>
    <option value="Qatar">Qatar</option>
    <option value="Reunion">Reunion</option>
    <option value="Romania">Romania</option>
    <option value="Russia">Russian Federation</option>
    <option value="Rwanda">Rwanda</option>
    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
    <option value="Saint LUCIA">Saint LUCIA</option>
    <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
    <option value="Samoa">Samoa</option>
    <option value="San Marino">San Marino</option>
    <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
    <option value="Saudi Arabia">Saudi Arabia</option>
    <option value="Senegal">Senegal</option>
    <option value="Seychelles">Seychelles</option>
    <option value="Sierra">Sierra Leone</option>
    <option value="Singapore">Singapore</option>
    <option value="Slovakia">Slovakia (Slovak Republic)</option>
    <option value="Slovenia">Slovenia</option>
    <option value="Solomon Islands">Solomon Islands</option>
    <option value="Somalia">Somalia</option>
    <option value="South Africa">South Africa</option>
    <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
    <option value="Span">Spain</option>
    <option value="SriLanka">Sri Lanka</option>
    <option value="St. Helena">St. Helena</option>
    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
    <option value="Sudan">Sudan</option>
    <option value="Suriname">Suriname</option>
    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
    <option value="Swaziland">Swaziland</option>
    <option value="Sweden">Sweden</option>
    <option value="Switzerland">Switzerland</option>
    <option value="Syria">Syrian Arab Republic</option>
    <option value="Taiwan">Taiwan, Province of China</option>
    <option value="Tajikistan">Tajikistan</option>
    <option value="Tanzania">Tanzania, United Republic of</option>
    <option value="Thailand">Thailand</option>
    <option value="Togo">Togo</option>
    <option value="Tokelau">Tokelau</option>
    <option value="Tonga">Tonga</option>
    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
    <option value="Tunisia">Tunisia</option>
    <option value="Turkey">Turkey</option>
    <option value="Turkmenistan">Turkmenistan</option>
    <option value="Turks and Caicos">Turks and Caicos Islands</option>
    <option value="Tuvalu">Tuvalu</option>
    <option value="Uganda">Uganda</option>
    <option value="Ukraine">Ukraine</option>
    <option value="United Arab Emirates">United Arab Emirates</option>
    <option value="United Kingdom">United Kingdom</option>
    <option value="United States">United States</option>
    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
    <option value="Uruguay">Uruguay</option>
    <option value="Uzbekistan">Uzbekistan</option>
    <option value="Vanuatu">Vanuatu</option>
    <option value="Venezuela">Venezuela</option>
    <option value="Vietnam">Viet Nam</option>
    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
    <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
    <option value="Western Sahara">Western Sahara</option>
    <option value="Yemen">Yemen</option>
    <option value="Yugoslavia">Yugoslavia</option>
    <option value="Zambia">Zambia</option>
    <option value="Zimbabwe">Zimbabwe</option>
</select>

</div>
<div class="form-group form-group4">
    <label class="label-title">Evidence<img src="source/image/content/question.jpg"></label>
    <textarea class="form-control form-title input-evidence" required placeholder="Evidence" name="evidence"></textarea>
  </div>
    <p class="btn-choose-file btn-add-contributor5 color-a form-upload-claim">
      <button id="btnchange-color" class="btn btn-upload-audio btn-upload-audio2 btn-add-contributor2 btn-upload-file">UPLOAD FILE</button>
      <a class="claim-optional" onclick="document.getElementById('idAddContributor').style.display='none'">Optional</a>
    </p>
    <div class="form-file-claim">
        <div class="file-icon">
            <img src="source/image/content/file_icon.png">
        </div>
        <div class="file-text"></div>
        <div class="file-remove"><i class="fa fa-remove"></i></div>

    </div>
    <img class="img_song1" src="source/image/content/img_song.jpg" style="margin: 10px 132px -27px 11px;">
    <label id="fileLabel17" class="inputfile inputfile-1 add-margin-top" data-multiple-caption="{count} files selected" multiple="">Anh Nho Em - Tuan Hung.mp3</label><i class="fa fa-remove song333 song1 remove-song-claim"></i>
</div>
<p class="btn-warning1 btn-agreement1 btn-publish-song btn-claim-empty">
   <a   class="btn btn-continue btn-back1 btn-general claim-submit" style="position: static;">Submit</a>
    <input type="submit" class="claim-publish-hidden" >
  <button class="btn btn-continue btn-publish1 btn-general claim-publish claim-save">Save</button>
   <a href="./" class="claim-cancel">Cancel</a>
</p>
</div>

<div id="idSubmitClaim" class="modal" >
  <!-- Modal Content -->
  <div class="modal-content modal-content-publish-song modal-saved-claims modal-submit-claim">
    <p class="title-home-warning"><img src="source/image/content/warning1.jpg">
      SUBMIT CLAIM?
    </p>
    <p class="text-home-warning">Are you sure you want to submit this broadcast <br> claim? You can't edit your claim later.</p>
    <p class="btn-warning1">
      <button class="btn btn-cancel btn-claim-cancel">CANCEL</button>
      <button onclick="agreeFunction()" class="btn btn-confirm">CONFIRM</button>
    </p>
  </div>
</div>

<div id="claim-upload-file" class="modal">
  <!-- Modal Content -->
  <div class="modal-content modal-upload-song">
    <p class="title-home-warning title-upload-song">
      UPLOAD EVIDENCE
    </p>
    
    <div class="box box2">
          <input type="file" name="claim-upload[]" id="file18" title="Choose a video please" onchange="pressed()">
            <label id="fileLabel23" class="inputfile inputfile-1 add-margin-top inputfile-7 label-box" data-multiple-caption="{count} files selected" multiple="" onclick="upload_audioFunction(e)">No file chosen</label>
            <label for="file18" > <span>CHOOSE FILE</span></label>
        </div>

    <p class="text-upload-audio">You can upload an image, audio or video file with
        a max file size of 3mb. The file name can containt
        letters, numbers, spaces, underscores and dashes.</p>
    <p class="btn-choose-file">      
      <button  class="btn btn-upload-audio btn-upload-audio2 claim-upload-file">UPLOAD</button>
      <a onclick="document.getElementById('claim-upload-file').style.display='none'">Cancel</a>
    </p>
     
  
  
</div>
</div>
<script type='text/javascript'>
$('.btn-upload-file').click(function(e){
    e.preventDefault();
    document.getElementById('claim-upload-file').style.display='block';
});
$('.claim-upload-file').click(function(e){
    e.preventDefault();
});

  window.pressed = function(){

      var a = document.getElementById('file18').value;
      var theSplit = a.split('\\');
      var string = theSplit[theSplit.length-1];
      var position = string.lastIndexOf('.');
      var exp = string.slice(position+1);
      var name = string.slice(0,position);
      if ( $('#file18')[0].files[0].name.match(/\.(jpg|jpeg|png|gif|mp3|mp4)$/) ) {
          if($('#file18')[0].files[0].size >= 4000000){
              alert('Please select a file smaller than 3mb to upload')
          }else{
              if(a == "")
              {
                  fileLabel23.innerHTML = "Choose file";
              }
              else
              {

                  if(name.length >= 23){
                      fileLabel23.innerHTML = name.slice(0,20)+'... .'+exp.toUpperCase();
                      $(".btn-upload-audio2").css("background-color","#F35A5C");
                      $('.file-text').text(name.slice(0,20)+'... .'+exp.toUpperCase());
                  }else{
                      fileLabel23.innerHTML =  name+'.'+exp.toUpperCase();
                      $(".btn-upload-audio2").css("background-color","#F35A5C");
                      $('.file-text').text(name+'.'+exp.toUpperCase());
                  }

              }
          }
      }else{
          alert('Please select a image, audio or video to upload');
      }
};
function save_songFunction() {
         var b = document.getElementById('file18').value;
    if(b == "")
    {
        fileLabel22.innerHTML = "Choose file";
    }
    else
    {
        var theSplit = b.split('\\');
        fileLabel17.innerHTML = theSplit[theSplit.length-1];
        //$(".btn-upload-audio2").css("background-color","#F35A5C");
        $("#claim-upload-file").hide();
        $("p.color-a").css("display","none");
        $(".modal-claim-empty").css("height","100%");
        $(".img_song1").css("display","inline");
        $("#fileLabel17").css("display","inline");
        $(".remove-song-claim").css("display","inline");
        
    }  
    }


$( ".form-country" ).change(function() {
  if($( ".form-country" ).val() != "" && $('.form-control-pro').val() != "" && $('.form-station').val() != "" && $('.form-program').val() != "" && $('.form-date').val() != ""){
$(".btn-general").css("background-color","#F35667");
  }
});



    $('.form-title7').keyup(function(){ 
if($('.form-title5').val() != "" && $('.form-title6').val() != "" && $('.form-title7').val() != ""){
$(".btn-add-song2").css("background-color","#F35667");
}
else {
  $(".btn-add-song2").css("background-color","#AAAAAA");
}
});
$('.form-title6').keyup(function(){ 
if($('.form-title5').val() != "" && $('.form-title6').val() != "" && $('.form-title7').val() != ""){
$(".btn-add-song2").css("background-color","#F35667");
}
else {
  $(".btn-add-song2").css("background-color","#AAAAAA");
}
});
$('.form-title5').keyup(function(){ 
if($('.form-title5').val() != "" && $('.form-title6').val() != "" && $('.form-title7').val() != ""){
$(".btn-add-song2").css("background-color","#F35667");
}
else {
  $(".btn-add-song2").css("background-color","#AAAAAA");
}
});
</script>
<script type="text/javascript">
  
 function radioFunction(){
$('#station-channel').attr("placeholder","Station");
 $("label[for='station-channel']").html("Station<img src="+'source/image/content/question.jpg'+">");
 }
 function tvFunction(){
$('#station-channel').attr("placeholder","Channel");
 $("label[for='station-channel']").html("Channel<img src="+'source/image/content/question.jpg'+">");
 } 
  
</script>
</form>
</div>
@endsection