<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">



    <link rel="icon" href="Favicon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <title>Register</title>
    <style type="text/css">
        
        .errors-text{
                margin: 10px 0 0;
                font-size: 13px;
                color: #ff4e4e;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
    <div class="container">
    <a class="navbar-brand" href="#"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="login">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="register">Register</a>
            </li>
        </ul>

    </div>
    </div>
</nav>

<main class="my-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Register</div>
                        <div class="card-body">
                             
                            <form name="my-form"   action=""  method="POST">
                                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                <div class="form-group row">
                                    <label for="full_name" class="col-md-4 col-form-label text-md-right">First Name</label>
                                    
                                    <div class="col-md-6">
                                        <input type="text" id="full_name" class="form-control" name="txtFirstName">
                                        @if ($errors->has('txtFirstName'))
                                        <div class="errors-text"> 
                                          {{$errors->first('txtFirstName')}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="full_name" class="col-md-4 col-form-label text-md-right">Last Name</label>
                                    <div class="col-md-6">
                                        <input type="text" id="full_name" class="form-control" name="txtLastName">
                                        @if ($errors->has('txtLastName'))
                                        <div class="errors-text"> 
                                          {{$errors->first('txtLastName')}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                    <div class="col-md-6">
                                        <input type="text" id="email_address" class="form-control" name="txtEmail">
                                        @if ($errors->has('txtEmail'))
                                        <div class="errors-text"> 
                                          {{$errors->first('txtEmail')}}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="user_name" class="col-md-4 col-form-label text-md-right">Password</label>
                                    <div class="col-md-6">
                                        <input type="password" id="user_name" class="form-control" name="txtPass">
                                        @if ($errors->has('txtPass'))
                                        <div class="errors-text"> 
                                          {{$errors->first('txtPass')}}</div>
                                        @endif
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label for="user_name" class="col-md-4 col-form-label text-md-right">Repassword</label>
                                    <div class="col-md-6">
                                        <input type="password" id="user_name" class="form-control" name="txtRePass">
                                         @if ($errors->has('txtRePass'))
                                        <div class="errors-text"> 
                                          {{$errors->first('txtRePass')}}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone_number" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                                    <div class="col-md-6">
                                        <input type="text" id="phone_number" name="txtPhone" class="form-control">
                                        @if ($errors->has('txtPhone'))
                                        <div class="errors-text"> 
                                          {{$errors->first('txtPhone')}}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="present_address" class="col-md-4 col-form-label text-md-right">Address</label>
                                    <div class="col-md-6">
                                        <input type="text" id="present_address" name="txtAddress" class="form-control">
                                        @if ($errors->has('txtAddress'))
                                        <div class="errors-text"> 
                                          {{$errors->first('txtAddress')}}</div>
                                        @endif
                                    </div>
                                </div>
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                        Register
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>

</main>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>