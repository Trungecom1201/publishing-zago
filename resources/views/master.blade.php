<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Laravel </title>
	<base href="{{asset('')}}">
	<link href='source/assets/dest/css/fonts-googleapis-com1.css' rel='stylesheet'>
	<link href='source/assets/dest/css/fonts-googleapis-com2.css' rel='stylesheet'>
	<link rel="stylesheet" href="source/assets/dest/css/bootstrap.min.css">
	<link rel="stylesheet" href="source/assets/dest/css/font-awesome.min.css">
	<link rel="stylesheet" href="source/assets/dest/rs-plugin/css/responsive.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">		
	<link rel="stylesheet" title="style" href="source/assets/dest/css/style2.css">
    <link rel="stylesheet" title="style" href="source/assets/dest/css/huong-style.css">
	<link rel="stylesheet" title="style" href="source/assets/dest/js/bootstrap-datepicker.js">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
    @include('header')
	<div class="rev-slider">
	@yield('content')
	</div> <!-- .container -->
     @include('footer')
	<!-- include js files -->
	
	<script src="source/assets/dest/js/myjquery.js"></script>
	<script src="source/assets/dest/vendors/jqueryui/jquery-ui-1.10.4.custom.min.js"></script>	
	<script src="source/assets/dest/js/bootstrap.min.js"></script>
	<script src="source/assets/dest/js/custom.js"></script>
	<script>
	$(document).ready(function(){
		var href =  window.location.href;
		var step ='{{asset('')}}publish-your-song';
		var step2 ='{{asset('')}}add-song';
		if(href != step && href != step2){
			$.get('{{asset('')}}clear-session',function(data){});
		}else if(href == step2){
			$.get('{{asset('')}}clear-contributor',function(data){});
		}
	});
	</script>
</body>
</html>
