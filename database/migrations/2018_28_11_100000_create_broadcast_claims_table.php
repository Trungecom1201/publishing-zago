<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broadcast_claims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_first_name')->nullable();
            $table->string('user_last_name')->nullable();
            $table->string('user_email')->nullable();
            $table->string('user_phone')->nullable();
            $table->string('user_member_type')->nullable();
            $table->string('ip_terms_conditions_signed')->nullable();
            $table->string('ip_publishing_agreementsigned')->nullable();
            $table->string('song_title')->nullable();
            $table->string('claim_status')->nullable();
            $table->string('submit_date')->nullable();
            $table->string('medium')->nullable();
            $table->string('station/channel')->nullable();
            $table->string('program')->nullable();
            $table->string('air_date')->nullable();
            $table->string('country')->nullable();
            $table->string('evidence')->nullable();
            $table->string('file')->nullable();
            $table->string('file_size')->nullable();
            $table->string('file_url')->nullable();
            $table->timestamps();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broadcast_claims');
    }
}
