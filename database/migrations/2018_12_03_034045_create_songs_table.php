<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id');
			$table->string('user_first_name')->nullable();
            $table->string('user_last_name')->nullable();
            $table->string('user_email')->nullable();
            $table->string('user_phone')->nullable();
            $table->string('user_member_type')->nullable();
            $table->string('song_title')->nullable();
            $table->string('status')->nullable();
            $table->string('duration')->nullable();
            $table->string('contains_samples')->nullable();
            $table->string('rights_to_use_samples')->nullable();
            $table->string('samples_info')->nullable();
            $table->string('remix')->nullable();
            $table->string('prev_registered_with_pro')->nullable();
            $table->string('which_pro')->nullable();
            $table->string('prs_tunecode')->nullable();
            $table->string('audio_original')->nullable();
            $table->string('file_size')->nullable();
            $table->string('file_url')->nullable();
            $table->string('audio_instrumental')->nullable();
            $table->string('file_size_instrumental')->nullable();
            $table->string('file_url_instrumental')->nullable();
            $table->string('equal_shares')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
