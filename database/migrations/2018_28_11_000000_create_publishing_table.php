<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublishingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publishing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_first_name')->nullable();
            $table->string('user_last_name')->nullable();
            $table->string('user_email');
            $table->string('user_phone')->nullable();
            $table->string('user_member_type')->nullable();
            $table->string('ip_terms_conditions_signed')->nullable();
            $table->string('ip_publishing_agreement_signed')->nullable();
            $table->string('song_title')->nullable();
            $table->string('status')->nullable();
            $table->string('publish_date')->nullable();
            $table->string('duration')->nullable();
            $table->string('contains_samples')->nullable();
            $table->string('rights_to_use_samples')->nullable();
            $table->string('samples_info')->nullable();
            $table->string('remix')->nullable();
            $table->string('prev_registered_with_pro')->nullable();
            $table->string('which_pro')->nullable();
            $table->string('prs_tunecode')->nullable();
            $table->string('audio_original')->nullable();
            $table->string('file_size')->nullable();
            $table->string('file_url')->nullable();
            $table->string('audio_instrumental')->nullable();
            $table->string('equal_shares')->nullable();
            $table->string('first_name(cont1)')->nullable();
            $table->string('last_name(cont1)')->nullable();
            $table->string('role(cont1)')->nullable();
            $table->string('share(cont1)')->nullable();
            $table->string('pro_member(cont1)')->nullable();
            $table->string('cae_number(cont1)')->nullable();
            $table->string('mech_society_member(cont1)')->nullable();
            $table->string('first_name(cont2)')->nullable();
            $table->string('last_name(cont2)')->nullable();
            $table->string('role(cont2)')->nullable();
            $table->string('share(cont2)')->nullable();
            $table->string('pro_member(cont2)')->nullable();
            $table->string('cae_number(cont2)')->nullable();
            $table->string('mech_society_member(cont2)')->nullable();
            $table->string('first_name(cont3)')->nullable();
            $table->string('last_name(cont3)')->nullable();
            $table->string('role_(cont3)')->nullable();
            $table->string('share_(cont3)')->nullable();
            $table->string('pro_member(cont3)')->nullable();
            $table->string('cae_number(cont3)')->nullable();
            $table->string('mech_society_member(cont3)')->nullable();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publishing');
    }
}
